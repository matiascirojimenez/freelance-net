﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RocaAPI.AdminEcomm.Web.Models.HomeModels
{
    public class NotificacionesModel
    {
        public NotificacionesModel()
        {
            this.Notificaciones = new List<Notificaciones>();
        }

        public List<Notificaciones> Notificaciones { get; set; }
    }

    public class Notificaciones
    {
    }
}