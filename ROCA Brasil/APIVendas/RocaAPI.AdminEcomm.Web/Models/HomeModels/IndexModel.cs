﻿using RocaAPI.AdminEcomm.Common.Entities.CORE.Seguridad;

namespace RocaAPI.AdminEcomm.Web.Models.HomeModels
{
    public class IndexModel
    {
        public string TestCulture { get; set; }
        public double TestCultureDouble { get; set; }
    }
}