﻿namespace RocaAPI.AdminEcomm.Web.Models
{
    public class ImportModel
    {
        public ImportModel(string action)
        {
            this.FormAction = action;
        }

        public string FormAction { get; set; }
    }
}