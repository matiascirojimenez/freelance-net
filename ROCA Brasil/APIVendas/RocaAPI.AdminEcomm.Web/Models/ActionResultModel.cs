﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RocaAPI.AdminEcomm.Web.Models
{
    public class ActionResultModel
    {
        public const int MAXREINTENTOS = 3;

        public bool Invalid { get; set; }
        public string InvalidMessage { get; set; }
        public int InvalidIntents { get; set; }

        public void SetInvalidIntents(HttpContextBase cotext)
        {
            if (cotext.Session["InvalidIntents"] == null || string.IsNullOrEmpty(cotext.Session["InvalidIntents"].ToString()))
                cotext.Session.Add("InvalidIntents", 0);
            cotext.Session["InvalidIntents"] = this.InvalidIntents;
        }

        public void ReadInvalidIntents(HttpContextBase cotext)
        {
            if (cotext.Session["InvalidIntents"] == null || string.IsNullOrEmpty(cotext.Session["InvalidIntents"].ToString()))
                cotext.Session.Add("InvalidIntents", 0);
            this.InvalidIntents = int.Parse(cotext.Session["InvalidIntents"].ToString());
        }
    }
}