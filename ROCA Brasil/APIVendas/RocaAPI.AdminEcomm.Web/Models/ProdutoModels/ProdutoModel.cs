﻿using RocaAPI.AdminEcomm.Common.Entities;
using System.Collections.Generic;
using System.Web;

namespace RocaAPI.AdminEcomm.Web.Models.ProdutoModels
{
    public class ProdutoModel : Code.Models.BaseFormEntityModel<Produto>
    {
        public ProdutoModel()
        {
        }

        public ProdutoModel(string entityName)
            : base(entityName)
        {
        }

        public ProdutoModel(string entityName, Produto entity)
            : base(entityName, entity)
        {
        }

        public HttpPostedFileBase ProfileImage { get; set; }

        public string UploadBaseEntityID { get; set; }

        public List<Produto> Produtos { get; set; }
    }
}