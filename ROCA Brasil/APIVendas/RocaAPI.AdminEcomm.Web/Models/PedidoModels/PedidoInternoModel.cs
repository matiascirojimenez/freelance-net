﻿using RocaAPI.AdminEcomm.Common.Entities;
using System.Collections.Generic;
using System.Web;

namespace RocaAPI.AdminEcomm.Web.Models.PedidoModels
{
    public class PedidoInternoModel : Code.Models.BaseFormEntityModel<PedidoInterno>
    {
        public PedidoInternoModel()
        {
        }

        public PedidoInternoModel(string entityName)
            : base(entityName)
        {
        }

        public PedidoInternoModel(string entityName, PedidoInterno entity)
            : base(entityName, entity)
        {
        }

        public string UploadBaseEntityID { get; set; }

        public List<Pedido> Pedidos { get; set; }
    }
}