﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RocaAPI.AdminEcomm.Web.Models
{
    public class TreeViewChildsChildsModel
    {
        public TreeViewChildsChildsModel(string id, List<TreeItem> childs, string title)
        {
            this.ParentBaseEntityID = id;
            this.TreeData = childs;
            this.Title = title;
        }

        public List<TreeItem> TreeData { get; set; }
        public string ParentBaseEntityID { get; set; }
        public string Title { get; set; }

    }

    public class TreeItem
    {
        public bool Checked { get; set; }

        public string Id { get; set; }

        public string SortValue { get; set; }

        public string Text { get; set; }

        public List<TreeItem> Childs { get; set; }

        public TreeItem(string id, string text, bool che, string sortValue)
        {
            this.Id = id;
            this.Text = text;
            this.Checked = che;
            this.SortValue = sortValue;
        }

        public static string GetJson(List<TreeItem> treeData)
        {
            var jsonData = "";
            foreach (var item in treeData)
            {
                var childrenJson = "";
                if (item.Childs != null && item.Childs.Count > 0)
                {
                    childrenJson += ", 'children': [" + Environment.NewLine;
                    childrenJson += GetJson(item.Childs);
                    childrenJson += "]" + Environment.NewLine;
                }
                jsonData += "{ 'id': '" + item.Id + "', 'text': '" + item.Text + "', 'state': { 'selected': " + item.Checked.ToString().ToLower() + " } " + childrenJson + " }," + Environment.NewLine;
            }
            return jsonData;
        }
    }
}