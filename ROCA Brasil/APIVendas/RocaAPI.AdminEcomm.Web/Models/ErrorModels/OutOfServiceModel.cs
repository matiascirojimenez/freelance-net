﻿using System;
using System.Linq;

namespace RocaAPI.AdminEcomm.Web.Models.ErrorModels
{
    public class OutOfServiceModel : ErrorModel
    {
        public OutOfServiceModel(DateTime to)
            : base()
        {
            this.To = to;
        }

        public DateTime To { get; private set; }

        public string ToDateJQuery()
        {
            return "var toDate = new Date(" + this.To.Year + ", " + (this.To.Month - 1) + ", " + this.To.Day + ", " + this.To.Hour + ", " + this.To.Minute + ", " + this.To.Second + ");";
        }
    }
}