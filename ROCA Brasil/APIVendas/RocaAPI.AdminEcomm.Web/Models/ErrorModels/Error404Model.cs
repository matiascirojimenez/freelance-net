﻿using System;
using System.Linq;

namespace RocaAPI.AdminEcomm.Web.Models.ErrorModels
{
    public class Error404Model : ErrorModel
    {
        public Error404Model()
            : base()
        {
        }
    }
}