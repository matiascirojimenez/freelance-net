﻿using System;
using System.Linq;

namespace RocaAPI.AdminEcomm.Web.Models.ErrorModels
{
    public class ErrorModel
    {
        public ErrorModel()
        {
        }

        public string Message { get; set; }

    }
}