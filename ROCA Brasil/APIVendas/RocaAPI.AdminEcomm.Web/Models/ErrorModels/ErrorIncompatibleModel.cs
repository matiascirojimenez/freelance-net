﻿using System;
using System.Linq;

namespace RocaAPI.AdminEcomm.Web.Models.ErrorModels
{
    public class ErrorIncompatibleModel : ErrorModel
    {
        public ErrorIncompatibleModel()
            : base()
        {
        }
    }
}