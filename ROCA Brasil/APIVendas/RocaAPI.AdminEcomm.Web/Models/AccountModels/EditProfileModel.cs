﻿using RocaAPI.AdminEcomm.Common.Entities.CORE.Seguridad;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace RocaAPI.AdminEcomm.Web.Models.AccountModels
{
    public class EditProfileModel
    {
        public EditProfileModel()
        { 
        }

        public EditProfileModel(Usuario user)
        {
            this.Entity = user;
        }

        public Usuario Entity { get; set; }

        [Required]
        public HttpPostedFileBase ProfileImage { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña actual:")]
        public string OldPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Nueva contraseña:")]
        public string NewPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirmar nueva contraseña:")]
        public string ConfirmPassword { get; set; }
    }
}
