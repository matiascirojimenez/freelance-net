﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace RocaAPI.AdminEcomm.Web.Models.AccountModels
{
    public class ChangePasswordModel : ActionResultModel
    {
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña actual:")]
        public string OldPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Nueva contraseña:")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar nueva contraseña:")]
        public string ConfirmPassword { get; set; }

        public Int32 UsuarioId { get; set; }
        public Int32 TenantId { get; set; }
        public String TenantCode { get; set; }
    }
}