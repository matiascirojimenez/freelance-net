﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace RocaAPI.AdminEcomm.Web.Models.AccountModels
{
    public class LoginModel : ActionResultModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool Remember { get; set; }
        public string Email { get; set; }

        public bool CambiarClave { get; set; }
        public string ReturnUrl { get; set; }
        public string Reset { get; set; }
    }
}
