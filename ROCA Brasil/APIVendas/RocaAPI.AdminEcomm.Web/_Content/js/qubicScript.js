﻿var $pageFooter;
var $pageContent;
var $pageTitle;
var qlibs_mDataTableIconUpdate = "kt-nav__link-icon flaticon2-contract";
var qlibs_mDataTableIconDelete = "kt-nav__link-icon flaticon2-trash";
var qlibs_mDataTableIconReacti = "kt-nav__link-icon flaticon2-calendar-5";
var qlibs_mDataTableIconDetail = "kt-nav__link-icon flaticon2-search-1";
var qlibs_mDataTable;
var urlDetailAction = '';
var urlCreateAction = '';
var urlUpdateAction = '';
var urlDeleteAction = '';
var urlReactiAction = '';
var urlSearchAction = '';
var urlExportAction = '';
var urlImportAction = '';

$(document).ready(function () {
    $pageFooter = $("#kt_footer");
    $pageContent = $('#kt_page');
    $pageTitle = $('#kt_page_title');
    $.qLibs_configureValidator();
    if (Cookies.get('qubicCurrentView')) {
        if (Cookies.get('qubicCurrentView') != "" && Cookies.get('qubicCurrentViewModule') == qubicCurrentModule) {
            loadContent(Cookies.get('qubicCurrentView'), Cookies.get('qubicCurrentViewTitle'));
        }
        Cookies.set('qubicCurrentView', "");
        Cookies.set('qubicCurrentViewTitle', "");
        Cookies.set('qubicCurrentViewModule', "");
    }
    $(document).on("input", ".solonumeros", function () {
        this.value = this.value.replace(/\D/g, '');
    });
    $(document).on("input", ".condecimales", function () {
        if (this.value.length == 1 && this.value == '-') {
            //do nothing
        }
        else {
            var retValu = testInput(this.value);

            if (retValu != '1') {
                try {
                    this.value = this.value.substring(0, this.value.length - 1);
                } catch (e) {
                }
            }
        }
    });
});

function reloadView(href) {
    Cookies.set('qubicCurrentView', "");
    Cookies.set('qubicCurrentViewTitle', "");
    Cookies.set('qubicCurrentViewModule', "");
    window.location.href = href;
}
function setView(module, partialURL, title) {
    Cookies.set('qubicCurrentView', partialURL);
    Cookies.set('qubicCurrentViewTitle', title);
    Cookies.set('qubicCurrentViewModule', module);
    loadContent(partialURL, title);
    var body = $("body");
    if (KTUtil.isInResponsiveRange('desktop')) {
        //body.removeClass('kt-aside--minimize-hover');
        //body.addClass('kt-aside--minimize');
        // Hover class
        KTLayout.getAsideMenu().scrollUpdate();
        KTLayout.getAsideMenu().scrollTop();
    } else {
        if (KTUtil.hasClass(body, 'kt-aside--minimize-hover') && KTUtil.isInResponsiveRange('desktop')) {
            //KTUtil.removeClass(body, 'kt-aside--minimize-hover');
            //KTUtil.addClass(body, 'kt-aside--minimize');
            //// Minimizing class
            //KTUtil.addClass(body, 'kt-aside--minimizing');
            //KTUtil.transitionEnd(body, function () {
            //    KTUtil.removeClass(body, 'kt-aside--minimizing');
            //});
            // Hover class
            asideMenu.scrollUpdate();
            asideMenu.scrollTop();
        }
    }
    return true;
}
var loadContent = function (href, title) {
    $pageFooter.hide();
    $pageTitle.html('');
    $pageContent.hide();
    KTApp.blockPage();
    $.ajax({
        url: href,
        type: 'GET',
        error: function () {
            KTApp.unblock();
            alert('ERROR');
        },
        success: function (dataHtml) {
            KTApp.unblock();
            $pageTitle.html(title);
            $pageContent.html(dataHtml);
            KTQuickPanel.init();
            $pageContent.show(300, function () {
                $pageFooter.show();
            });
        }
    });
};

function testInput(inputValue) {
    if (inputValue.match(/^-?[+]?[0-9]+([,][0-9]{1,2})?$/)) { return 1; }
    else if (inputValue.match(/^-?[+]?[0-9]+([,])?$/)) { return 1; }
    else { return 0; }
}

function refillParentCombo(comboId, urlCombo, idParent, placeHolderCombo) {
    var urlComboFinal = urlCombo.replace('ID-PARENT', idParent);
    $.qLibs_fillCombo(comboId, urlComboFinal, null, function () {
        $(comboId).select2({ placeholder: placeHolderCombo, allowClear: true, width: 'resolve' });
    });
}

//---------------DataTableVariables--------------------------------------
var qlibs_mDataTable_GridParams_language = {
    "lengthMenu": "&nbsp;&nbsp;Mostrar _MENU_",
    "zeroRecords": "<div class='alert alert-secondary  fade show' role='alert'><div class='alert-text'>SIN DATOS</div></div></div>",
    "info": "Pagína _PAGE_ de _PAGES_",
    "infoEmpty": "",
    "infoFiltered": ""
};
var qlibs_mDataTable_GridParams_dom = `<'row'<'col-sm-12'tr>>
                                                    <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`;
//---------------DataTableVariables--------------------------------------
function qlibs_mDataTable_GridParams_lengthMenu() {
    if ('getGridPageNumber' in window) {
        return window['getGridPageNumber']();
    }
    return [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "Todos"]];
}
function qlibs_mDataTable_GridAutoFill() {
    if ('getGridAutoFill' in window) {
        return window['getGridAutoFill']();
    }
    return true;
}
function onAjaxFailGrid(e, xhr) {
    $.qLibs_showAlertError(xhr.statusText + '<br>' + xhr.responseText, 'body', false, 60000, true);
}
function gridActionTemplate(actions) {
    var result = '';
    if (actions != '') {
        result += '<div class="dropdown"><a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md" aria-expanded="false"><i class="flaticon-more-1"></i></a>';
        result += '<div class="dropdown-menu dropdown-menu-right" x-placement="top-end" style="display: none; position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(1589px, 27px, 0px);">';
        result += '<ul class="kt-nav">';
        result += actions;
        result += '</ul>';
        result += '</div>';
        result += '</div>';
    }
    return result;
}
function getGridActionColumn(icon, renderFunction) {
    if (!icon || icon == "")
        icon = "flaticon-more-1";
    if (!renderFunction)
        renderFunction = function (e, a, i) { return getGridActionColumnTemplate(icon, e, a, i); };
    return { data: "Actions", class: 'action_column', title: "", orderable: false, overflow: "visible", render: renderFunction };
}
function getGridActionColumnActivo(activo, textSi, textNo) {
    if (!textSi) textSi = 'ACTIVO';
    if (!textNo) textNo = 'BAJA';
    if (activo)
        return '<span><span class="kt-badge kt-badge--success kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-success">' + textSi+'</span></span>';
    else
        return '<span><span class="kt-badge kt-badge--danger kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-danger">' + textNo +'</span></span>';
    //if (activo)
    //    return '<span class="btn btn-label-success btn-bold" style="width: 100%; height: 24px; padding: 2px !important;">activo</span>';
    //else
    //    return '<span class="btn btn-label-danger btn-bold" style="width: 100%; height: 24px; padding: 2px !important;">baja</span>';
}
function getGridActionColumnTemplate(icon, x, a, e) {
    var items = getGridActionsItems(x, a, e);
    return getGridActionColumnTemplateHtml(icon, items);
}
function getGridActionColumnTemplateHtml(icon, items) {
    var itemsHtml = '';
    $.each(items, function (key, obj) {
        itemsHtml += '<li class="kt-nav__item"><a href="javascript::void(0)" onclick="' + obj.onclick + '" class="kt-nav__link"><i class="' + obj.icon + '"></i><span class="kt-nav__link-text">' + obj.text + '</span></a></li>';
    });
    return '\<div class="dropdown">\
                                                                    <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown">\
                                                                        <i class="' + icon + '"></i>\
                                                                    </a>\
                                                                    <div class="dropdown-menu dropdown-menu-left">\
                                                                        <ul class="kt-nav">' + itemsHtml +
        '</ul>\
                                                                    </div>\
                                                                </div>';
}
var defaultPageL = 200;
function getPageLenght() {
    return defaultPageL;
}
function fillGrid() {
    if (qlibs_mDataTable) {
        qlibs_mDataTable.destroy();
        $(".data_table").empty();
    }
    $(".data_table").hide();
    var dataParams = getGridQueryParams();

    if (typeof validateFilters == 'function') {
        if (validateFilters(dataParams) == false) {
            return;
        }
    }

    qlibs_mDataTable = null;
    qlibs_mDataTable = $(".data_table").DataTable({
        dom: qlibs_mDataTable_GridParams_dom,
        language: qlibs_mDataTable_GridParams_language,
        lengthMenu: qlibs_mDataTable_GridParams_lengthMenu(),
        pageLength: 50,
        responsive: false,
        processing: true,
        autoWidth: false,
        serverSide: true,
        searching: false,
        orderMulti: true,
        order: [],
        ajax: {
            type: "POST",
            url: getGridURL(),
            data: dataParams
        },
        columns: getGridColumns(),
        initComplete: function (settings, json) {
            if ('initComplete' in window) {
                window['initComplete']();
            }
        },
        createdRow: function (row, data, dataIndex) {
            if ('createdRow' in window) {
                window['createdRow'](row, data, dataIndex);
            }
        },
        footerCallback: function (t, e, n, a, r) {
            if ('footerCallback' in window) {
                window['footerCallback']();
            }
        },
    });
    qlibs_mDataTable.on('draw', function (e, settings, json, xhr) {
        e.stopImmediatePropagation();
        if ('onDrawDoneGrid' in window) {
            window['onDrawDoneGrid']();
        }
    });
    qlibs_mDataTable.on('xhr.dt', function (e, settings, json, xhr) {
        e.stopImmediatePropagation();
        $(".data_table").show();
        if (xhr.statusText == "OK" || xhr.statusText == "success" || xhr.statusText == "HTTP/2.0 200") {
            if ('onAjaxDoneGrid' in window) {
                window['onAjaxDoneGrid'](e, json, this);
            }
        } else {
            if ('onAjaxFailCustomGrid' in window) {
                window['onAjaxFailCustomGrid'](this);
            }
            else {
                onAjaxFailGrid(e, xhr);
            }
        }
    });
}
function showGrid(refill) {
    if (refill)
        fillGrid();
    $("#detailActions").hide(300, function () {
        $('#detailActions').html('');
        $("#gridActions").show();
        $("#kt_subheader_search").show();
    });
    $("#detailPanel").hide(300, function () {
        $('#detailPanel').html('');
        $("#gridPanel").show();
        $("#kt_contact_aside").show();
    });
}
function hideGrid(onComplete) {
    $("#gridActions").hide(300, function () {
        $("#detailActions").show();
    });
    $("#gridPanel").hide(300, function () {
        $("#kt_subheader_search").hide();
        $("#detailPanel").show();
        onComplete();
    });
}
function showDetailForm(href) {
    $.qLibs_blockContainer("#gridPanel");

    $.ajax({
        url: href,
        type: 'GET',
        error: function () {
            KTApp.unblock();
            alert('ERROR');
        },
        success: function (dataHtml) {
            KTApp.unblock();
            hideGrid(function () {
                $('#detailPanel').html(dataHtml);
                initializeDetailForm($('#detailActions'));
            });
        }
    });
    return false;
}
function setCreateButton(button, urlCreate) {
    if ($(button)) {
        $(button).click(function (event) {
            debugger;
            event.preventDefault();
            showDetailForm(urlCreate);
            return false;
        });
    }
    return false;
}
function setCancelDetailClick(button) {
    button.on("click", function (a) {
        a.preventDefault();
        showGrid();
        return false;
    });
}

var qlibs_selctedRow;
function editRow(actionButton, url) {
    qlibs_selctedRow = $(actionButton).closest('tr');
    showDetailForm(url);
    return false;
}
function detailRow(actionButton, url) {
    qlibs_selctedRow = $(actionButton).closest('tr');
    showDetailForm(url);
    return false;
}
function deleteRow(actionButton, url, confirmation) {
    qlibs_selctedRow = $(actionButton).closest('tr');
    if (!confirmation || confirmation == "")
        confirmation = "¿Está seguro que desea eliminar el registro?";
    $.qLibs_showAlertConfirmMessage(confirmation, function () {
        $.qLibs_doAjaxSubmit('body', url, 'POST');
    }, function () {
        return false;
    });
    return false;
}
function reactivateRow(actionButton, url, confirmation) {
    qlibs_selctedRow = $(actionButton).closest('tr');
    if (!confirmation || confirmation == "")
        confirmation = "¿Está seguro que desea reactivar el registro?";
    $.qLibs_showAlertConfirmMessage(confirmation, function () {
        $.qLibs_doAjaxSubmit('body', url, 'POST');
    }, function () {
        return false;
    });
    return false;
}
function getRefreshButton() {
    return '<a id="refreshButton" class="btn btn-label-info btn-bold"><i class="flaticon-refresh" style="padding-right: 0px;"></i></a>';
}
function getCancelButton() {
    return '<a id="cancelButton" class="btn btn-default btn-bold">Cancelar</a>';
}
function getSaveButton() {
    return '<button type="button" id="submitButton" class="btn btn-brand btn-bold">Grabar</button>';
}
function qlibs_crud_submitFormOK() {
    if (getGridMustDoSubmitFormOK()) {
        setTimeout(function () { showGrid(true); fillGrid(); }, 2000);
    }
}
function qlibs_crud_deleteOK() {
    if (qlibs_selctedRow) {
        qlibs_selctedRow.hide();
    }
}
function submitFormEditFormOKDefault() {
    setTimeout(function () { showGrid(true); fillGrid(); }, 2000);
}

function dataTableForDetailGenerate(tableElement, dataArray, columnsArray) {
    var result = tableElement.DataTable({
        dom: qlibs_mDataTable_GridParams_dom,
        language: qlibs_mDataTable_GridParams_language,
        lengthMenu: qlibs_mDataTable_GridParams_lengthMenu,
        data: dataArray,
        columns: columnsArray,
        paging: false,
        ordering: false,
        responsive: false,
        processing: false,
        serverSide: false,
        searching: false,
        orderMulti: false,
    });
    return result
}

function setCancelRefreshDetailClick(buttonC, buttonR) {
    if (buttonC)
        setCancelDetailClick(buttonC);
    if (buttonR)
        setRefreshDetailClick(buttonR);
}
var lastHrefDetail = '';
function setRefreshDetailClick(button) {
    button.on("click", function (a) {
        debugger
        a.preventDefault();
        if (lastHrefDetail != '') {
            showDetailForm(lastHrefDetail);
        }
        return false;
    });
}

$fillCrudListFilter = function (listOptions) {
    var dropDownListId = listOptions.listId;
    var actionURL = listOptions.url;
    var defaultIcon = listOptions.defaultIcon;
    $.ajax({
        url: actionURL,
        dataType: "json",
        type: "POST",
        error: function () {
            console.log("An error occurred.");
        },
        success: function (data) {
            // states is your JSON array
            $(dropDownListId).html('');
            $.each(data.Options, function (i, obj) {
                //var itemHtml = '<option></option>';
                //if (obj.Selected)
                //    itemHtml = '<option selected="1"></option>';
                if (listOptions.addItemValidation)
                    if (listOptions.addItemValidation(obj.Value) == false)  //Saltea el elemento.
                        return true;
                var icon = defaultIcon;
                if (obj.Icon) {
                    icon = obj.Icon;
                }
                var itemhtml = "<a href=\"javascript:$fillCrudListFilterSelectedItem('" + dropDownListId + "','" + obj.Value + "')\" class=\"kt-notification__item\"><div class=\"kt-notification__item-icon\"><i class=\"" + icon + " kt-font-brand\"></i></div><div class=\"kt-notification__item-details\"><div class=\"kt-notification__item-title\">" + obj.DisplayText + "</div></div></a>";
                $(dropDownListId).append(itemhtml);
            });
            if (listOptions.populateComplete)
                listOptions.populateComplete();
        }
    });
}
$fillCrudListFilterSelectedItem = function (dropDownListId, selectedValue) {
    debugger;
    $(dropDownListId).data('selected', selectedValue);
    if ('crudListFilterSelected' in window) {
        crudListFilterSelected(dropDownListId, selectedValue);
    }
}
