﻿qlibs_mDataTable_GridParams_language = {
    "lengthMenu": "&nbsp;&nbsp;_MENU_&nbsp;rows&nbsp;per&nbsp;page",
    "zeroRecords": "-- NO DATA --",
    "info": "Page _PAGE_ of _PAGES_",
    "infoEmpty": "",
    "infoFiltered": ""
};
qLibs_waitMessage = 'Wait please...';
qLibs_deleteRowConfirmationMessage = "Are you sure you want to delete the record?";
qLibs_reactivateRowConfirmationMessage = "Are you sure you want to reactivate the record?";
qLibs_CancelButtonText = "Cancel";
qLibs_SaveButtonText = "Save";