﻿var qlibs_mDataTable_GridParams_language = {
    "lengthMenu": "&nbsp;&nbsp;_MENU_&nbsp;reg.&nbsp;por&nbsp;pagína",
    "zeroRecords": "-- Sin datos --",
    "info": "Pagína _PAGE_ de _PAGES_",
    "infoEmpty": "",
    "infoFiltered": ""
};
var qLibs_waitMessage = 'Aguarde por favor...';
var qLibs_deleteRowConfirmationMessage = "¿Está seguro que desea eliminar el registro?";
var qLibs_reactivateRowConfirmationMessage = "¿Está seguro que desea reactivar el registro?";
var qLibs_CancelButtonText = "Cancelar";
var qLibs_SaveButtonText = "Grabar";

$.qLibs_waitSleep = function (callback, seconds) {
    return window.setTimeout(callback, seconds * 1000);
}

$.qLibs_goBackToReturnURL = function () {
    window.location.href = $("#ReturnURL").val();
}

$.qLibs_createTreeView = function (tree, data, defaultIcon, fileIcon) {
    if (!defaultIcon || defaultIcon == "")
        defaultIcon = "fa fa-folder icon-state-warning icon-lg";
    if (!fileIcon || fileIcon == "")
        fileIcon = "fa fa-file icon-state-warning icon-lg";
    tree.jstree({
        'plugins': ["wholerow", "checkbox", "types"],
        checkbox: {
            three_state: false,
            cascade: 'up'
        },
        'core': { "themes": { "responsive": false, "icons": false }, 'data': data, expand_selected_onload: false },
        "types": { "default": { "icon": defaultIcon }, "file": { "icon": fileIcon } }
    });
}

$.qLibs_getUndeterminedFromTreeView = function (tree) {
    var checked_ids = [];
    tree.find(".jstree-undetermined").each(function (i, element) {
        checked_ids.push($(element).closest('.jstree-node').attr("id"));
    });
    return checked_ids;
}

$.qLibs_getSelectedFromTreeView = function (tree) {
    return tree.jstree('get_selected');
}

$.qLibs_setFormValidations = function (formId, formRules, ignoreHiddens) {
    if (ignoreHiddens) {
        $(formId).validate({
            rules: formRules,
            ignore: "",
            errorClass: "help-block text-danger form-validator-span",
            errorElement: "span",
            errorMessagePosition: 'top',
            scrollToTopOnError: true,
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.form-control').addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.form-control').removeClass('is-invalid');
            }
        });
    }
    else {
        $(formId).validate({
            rules: formRules,
            errorClass: "help-block text-danger form-validator-span",
            errorElement: "span",
            errorMessagePosition: 'top',
            scrollToTopOnError: true,
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.form-control').addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.form-control').removeClass('is-invalid');
            }
        });
    }
}

$.qLibs_fillCombo = function (dropDownListId, actionURL, validateItemToAddInComboMethod, postComplete) {
    $.ajax({
        url: actionURL,
        dataType: "json",
        type: "POST",
        error: function () {
            console.log("An error occurred.");
        },
        success: function (data) {
            // states is your JSON array
            $(dropDownListId).empty();
            var arr = new Array();

            $.each(data.Options, function (i, obj) {
                var itemHtml = '<option></option>';
                if (obj.Selected)
                    itemHtml = '<option selected="1"></option>';

                if (validateItemToAddInComboMethod)
                    if (validateItemToAddInComboMethod(obj.Value) == false)  //Saltea el elemento.
                        return true;

                $(dropDownListId).append($(itemHtml).val(obj.Value).html(obj.DisplayText));
                $(dropDownListId).attr('code', obj.Code);

            });
            if (postComplete)
                postComplete();
        }
    });
}

$.qLibs_fillComboAutocomplete = function (dropDownListId, actionURL, cache) {
    $(dropDownListId).selectpicker('destroy');
    $(dropDownListId).selectpicker("refresh");
    $(dropDownListId + ' option').remove();
    $(dropDownListId).selectpicker("refresh");
    $.ajax({
        url: actionURL,
        dataType: "json",
        type: "POST",
        error: function () {
            console.log("An error occurred.");
        },
        success: function (data) {
            var id = null;
            $.each(data.Options, function (i, obj) {
                var itemHtml = '<option></option>';

                if (obj.Code != null && obj.Code != '')
                    itemHtml = itemHtml.replace('><', ' data-tokens="' + obj.Code + '"><');

                $(dropDownListId).append($(itemHtml).val(obj.Value).html(obj.DisplayText));

                if (obj.Selected) {
                    id = obj.Value;
                }
            });

            $(dropDownListId).selectpicker('val', id);
            $(dropDownListId).selectpicker('windowPadding', 'top');
            $(dropDownListId).selectpicker('virtualScroll', 300);
            $(dropDownListId).selectpicker("refresh");
        }
    });
}

$.qLibs_doAjaxSubmit = function (targetContainerToBlock, ajaxUrl, ajaxMethod, formData, successTrigger, onErrorTrigger) {

    $.qLibs_blockOnContainer(targetContainerToBlock);

    $.ajax({
        url: ajaxUrl,
        data: formData,
        method: ajaxMethod,
        success: function (data, textStatus, jqXHR) {
            if (successTrigger)
                successTrigger();
            $.qLibs_manageAjaxSubmitOK(targetContainerToBlock, data, textStatus, jqXHR);
            KTApp.unblock(targetContainerToBlock);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (onErrorTrigger)
                onErrorTrigger();
            $.qLibs_manageAjaxSubmitError(targetContainerToBlock, jqXHR, textStatus, errorThrown);
            KTApp.unblock(targetContainerToBlock);
        }
    });
}

$.qLibs_setFormAjaxSubmit = function (button, targetContainerToBlock, formId, fileUpload) {
    $(button).click(function () {
        $(formId).submit();
        return false;
    });

    $(formId).attr('fileUpload', fileUpload);

    $(formId).submit(function (e) {
        var validator = $(formId).validate();
        if (validator.valid()) {
            $.qLibs_blockContainer(targetContainerToBlock);
        }
        else {
            e.preventDefault(); //STOP default action
            return false;
        }

        var formData = $(this).serializeArray();
        var formprocessData = true;
        var formcontentType = 'application/x-www-form-urlencoded; charset=UTF-8';
        if ($(this).attr('fileUpload')) {
            formData = new FormData(this);
            formprocessData = false;
            formcontentType = false;
        }

        $.ajax({
            url: $(this).attr("action"),
            type: $(this).attr('method'),
            data: formData,
            processData: formprocessData,
            contentType: formcontentType,
            success: function (data, textStatus, jqXHR) {
                $.qLibs_manageAjaxSubmitOK(targetContainerToBlock, data, textStatus, jqXHR);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.qLibs_manageAjaxSubmitError(targetContainerToBlock, jqXHR, textStatus, errorThrown);
            }
        });
        e.preventDefault(); //STOP default action
    });
}

$.qLibs_setFormAjaxSubmitCustomAjax = function (button, targetContainerToBlock, formId, ajaxMethod) {
    $(button).click(function () {
        $(formId).submit();
        return false;
    });

    $(formId).submit(function (e) {
        
        var validator = $(formId).validate();
        if (validator.valid()) {
            $.qLibs_blockContainer(targetContainerToBlock);
        }
        else {
            e.preventDefault(); //STOP default action
            return false;
        }

        ajaxMethod();
        e.preventDefault(); //STOP default action
    });
}

$.qLibs_manageAjaxSubmitOK = function (targetContainerToBlock, data, textStatus, jqXHR) {
    if (data.Result == "OK") {
        if (data.DoAction == "REDIRECTTOPAGE") {
            KTApp.unblock();
            if (data.ShowAlert) {
                $.qLibs_showAlertOK(data.Message, targetContainerToBlock);
            }
            window.location.href = data.DoActionParams;
        }
        else if (data.DoAction == "EXECUTEMETHOD") {
            if (data.ShowAlert) {
                $.qLibs_showAlertOK(data.Message, targetContainerToBlock);
            }
            if (data.DoActionParams in window) {
                window[data.DoActionParams](data.Record);
            }
            else {
                alert('Metodo no encontrado: ' + data.DoActionParams);
            }
            KTApp.unblock();
        }
        else {
            $.qLibs_showAlertOK(data.Message, targetContainerToBlock);
            KTApp.unblock();
        }
    }
    else if (data.Result == "INFO") {
        $.qLibs_showAlertInfo(data.Message);
        KTApp.unblock();
    }
    else {
        $.qLibs_showAlertError(data.Message, 60000, targetContainerToBlock, true);
        KTApp.unblock();
    }
};

$.qLibs_manageAjaxSubmitError = function (targetContainerToBlock, jqXHR, textStatus, errorThrown) {
    $.qLibs_showAlertError('Se ha producido un error inesperado.', 60000, targetContainerToBlock, true);
    KTApp.unblock();
};

$.qLibs_showEntityModalForm = function ($modal, url, initializePopupFunctionName) {
    if (!initializePopupFunctionName)
        initializePopupFunctionName = 'initializePopup';

    setTimeout(function () {
        $modal.load(url, '', function (response, status, xhr) {
            if (status != 'success')
                return false;
            $modal.modal();
            if (window[initializePopupFunctionName]) {
                window[initializePopupFunctionName]($modal);
            }
            return false;
        });
    });
};

$.qLibs_showEntityModalFormUnblockContainer = function ($modal, url, initializePopupFunctionName, container) {
    if (!initializePopupFunctionName)
        initializePopupFunctionName = 'initializePopup';

    setTimeout(function () {
        $modal.load(url, '', function (response, status, xhr) {
            if (status != 'success')
                return false;
            $modal.modal();
            KTApp.unblock(container);
            if (window[initializePopupFunctionName]) {
                window[initializePopupFunctionName]($modal);
            }
            return false;
        });
    });
};

$.qLibs_autocomplete = function ($textbox, urlAutoComplete, $getDataMethod, $onExecuteMethod, $onSelectMethod) {
    $textbox.autocomplete({
        source: function (request, response) {
            if ($onExecuteMethod)
                $onExecuteMethod();
            $textbox.addClass('ui-autocomplete-loading');
            $.ajax({
                dataType: "json",
                type: 'GET',
                url: urlAutoComplete,
                data: $getDataMethod(request, response),
                success: function (data) {
                    response($.map(data, function (obj) {
                        return obj;
                    }));
                },
                error: function (data) {
                    $textbox.removeClass('ui-autocomplete-loading');
                }
            });
        },
        select: function (event, ui) {
            $textbox.removeClass('ui-autocomplete-loading');
            $onSelectMethod(event, ui);
        }
    });
};

$.qLibs_confirmation = function ($butoon, $labelTitle, $labelConfirm, $labelCancel, $onConfirm, $onCancel, $placement) {
    if (!$placement)
        $placement = 'bottom';
    $butoon.confirmation({
        title: $labelTitle,
        placement: $placement,
        popout: true,
        btnOkLabel: $labelConfirm,
        btnCancelLabel: $labelCancel,
        onConfirm: function () { return $onConfirm(); },
        onCancel: function () { return $onCancel(); },
    });
};


$.qLibs_blockOnContainer = function (targetContainerToBlock) {
    KTApp.block(targetContainerToBlock, {
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: qLibs_waitMessage
    });
};

$.qLibs_blockContainer = function (targetContainerToBlock) {
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: qLibs_waitMessage
    });
};

// ------------- ALERTS/MESSAGES------------------------------
$.qLibs_showAlertOK = function (message, target, resetErrors, $postShowAlert) {
    swal.fire({
        title: "Success operation!",
        text: message,
        type: "success",
        buttonsStyling: false,
        confirmButtonText: "Close",
        confirmButtonClass: "btn btn-brand"
    }).then(function (result) {
        if (result.value && $postShowAlert != undefined) {
            $postShowAlert();
        }
    });
};
$.qLibs_showAlertWarning = function (message, target, resetErrors, $postShowAlert) {
    swal.fire({
        title: "Atention!",
        text: message,
        type: "warning",
        buttonsStyling: false,
        confirmButtonText: "Close",
        confirmButtonClass: "btn btn-brand"
    }).then(function (result) {
        if (result.value && $postShowAlert != undefined) {
            $postShowAlert();
        }
    });
};
$.qLibs_showAlertError = function (message, target, resetErrors, $closeInMilliseconds, $postShowAlert) {
    swal.fire({
        title: "Error!",
        text: message,
        type: "error",
        buttonsStyling: false,
        confirmButtonText: "Close",
        confirmButtonClass: "btn btn-brand"
    }).then(function (result) {
        if (result.value && $postShowAlert != undefined) {
            $postShowAlert();
        }
    });
};
$.qLibs_showAlertInfo = function (message, target, resetErrors) {
    swal.fire({
        title: "Info!",
        text: message,
        type: "info",
        buttonsStyling: false,
        confirmButtonText: "Close",
        confirmButtonClass: "btn btn-brand"
    });
};
$.qLibs_showAlertWarning = function (message, target, resetErrors, $postShowAlert) {
    swal.fire({
        title: "Atención!",
        text: message,
        type: "warning",
        buttonsStyling: false,
        confirmButtonText: "Close",
        confirmButtonClass: "btn btn-brand"
    }).then(function (result) {
        if (result.value && $postShowAlert != undefined) {
            $postShowAlert();
        }
    });
};
$.qLibs_showAlertConfirm = function (methodName, html) {
    swal.fire({
        title: "¿Está seguro?",
        html: html,
        text: "",
        type: "warning",

        buttonsStyling: false,

        confirmButtonText: "<i class='la la-check'></i> Yes, confirm!",
        confirmButtonClass: "btn btn-success",

        showCancelButton: true,
        cancelButtonText: "<i class='la la-thumbs-down'></i> No, cancel",
        cancelButtonClass: "btn btn-default"
    }).then(function (result) {
        if (result.value && methodName in window) {
            return window[methodName]();
        }
    });
};
$.qLibs_showAlertConfirmMessage = function ($labelConfirm, $onConfirm, $onCancel, html) {
    swal.fire({
        title: $labelConfirm,
        html: html,
        text: "",
        type: "warning",

        buttonsStyling: false,

        confirmButtonText: "<i class='la la-check'></i> Yes, confirm!",
        confirmButtonClass: "btn btn-success",

        showCancelButton: true,
        cancelButtonText: "<i class='la la-thumbs-down'></i> No, cancel",
        cancelButtonClass: "btn btn-default"
    }).then(function (result) {
        if (result.value) {
            return $onConfirm();
        }
        else {
            return $onCancel();
        }
    });
};

// ------------- Masks------------------------------
$.qLibs_setDecimalMask = function (inputId, decimals, integers) {
    if (!decimals)
        decimals = 4;
    if (!integers)
        integers = 9;
    if ($(inputId).length) {
        $(inputId).inputmask({ 'mask': "-{0,1}9{0," + integers + "},9{0," + decimals + "}", greedy: false });
        if ($(inputId).val().endsWith(',')) {
            $(inputId).val($(inputId).val() + "0");
        }
        $(inputId).unbind(".qlibsEvents");
        $(inputId).bind("blur.qlibsEvents", function () {
            if ($(this).val().endsWith(',')) {
                $(this).val($(this).val() + "0");
            }
            if ($(this).val().startsWith(',')) {
                $(this).val("0" + $(this).val());
            }
        });
        $(inputId).bind("leave.qlibsEvents", function () {
            if ($(this).val().endsWith(',')) {
                $(this).val($(this).val() + "0");
            }
            if ($(this).val().startsWith(',')) {
                $(this).val("0" + $(this).val());
            }
        });
        $(inputId).bind("click.qlibsEvents", function () {
            $(this).select();
        });
    }
}
$.qLibs_setIntegerMask = function (inputId, digits) {
    if (!digits)
        digits = 9;
    if ($(inputId).length) {
        $(inputId).unbind(".qlibsEvents");
        $(inputId).inputmask({ "mask": "9", "repeat": digits, "greedy": false });
    }
}
// ------------- TouchSpin------------------------------
$.qLibs_setDecimalTouchSpin = function (inputId, decimals, minim, maxim, step, postf) {
    if (!maxim)
        maxim = 999999999;
    if (!step)
        step = 1;
    if (!decimals)
        decimals = 4;
    if (!postf)
        postf = "";
    if ($(inputId).length)
        $(inputId).TouchSpin({
            min: minim,
            max: maxim,
            step: step,
            decimals: decimals,
            boostat: 5,
            maxboostedstep: 10,
            postfix: postf
        });
}

// ------------- CONFIGURE VALIDATIONS ------------------------------
$.qLibs_configureValidator = function () {
    $(".input-date").datepicker({ dateFormat: 'dd/mm/yy' });
    $(".input-date").on('change', function () {
        $('.datepicker').hide();
    });

    $.validator.methods.range = function (value, element, param) {
        var globalizedValue = value.replace(".", "").replace(",", ".");
        return this.optional(element) || (globalizedValue >= param[0] && globalizedValue <= param[1]);
    }

    $.validator.methods.max = function (value, element, param) {
        var globalizedValue = value.replace(".", "").replace(",", ".");
        return this.optional(element) || (globalizedValue <= param);
    }

    $.validator.methods.min = function (value, element, param) {
        var globalizedValue = value.replace(".", "").replace(",", ".");
        return this.optional(element) || (globalizedValue >= param);
    }

    $.validator.methods.number = function (value, element) {
        return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:[\s\.,]\d{3})+)(?:[\,]\d+)?$/.test(value);
    }

    $.validator.addMethod("cuit", validaCuit, 'CUIT/CUIT Inválido');

    $.validator.addMethod("customValidation", validaCustomFunction, '*');

    $.validator.addMethod('reCaptchaMethod', function (value, element, param) {
        if (grecaptcha.getResponse() == '') {
            return false;
        } else {
            // I would like also to check server side if the recaptcha response is good
            return true;
        }
    }, 'Es necesario completar el CAPTCHA');
}
function validaFecha(value, element) {
    if (value && value != '')
        return value.match(/^\d\d?\/\d\d?\/\d\d\d\d$/);
    return true;
}
function validaFechaHora(value, element) {
    if (value && value != '')
        return value.match(/^\d\d?\/\d\d?\/\d\d\d\d \d\d:\d\d:\d\d$/);
    return true;
}
function validaCustomFunction(value, element) {
    try {
        var controlId = $(element).attr('id');
        var methodName = controlId + '_validation';
        if (methodName in window) {
            return window[methodName](value, element);
        }
        alert("Validacion no establecida para " + controlId);
        return false;
    }
    catch (err) {
        if (!err)
            alert("Error inesperado en metodo de validacion custom. " + err);
        else
            alert("Error inesperado en metodo de validacion custom.");
        return false;
    }
}
function validaCuit(cuit) {
    if (typeof (cuit) == 'undefined')
        return true;
    cuit = cuit.toString().replace(/[-_]/g, "");
    if (cuit == '')
        return true; //No estamos validando si el campo esta vacio, eso queda para el "required"
    if (cuit.length != 11)
        return false;
    else {
        var mult = [5, 4, 3, 2, 7, 6, 5, 4, 3, 2];
        var total = 0;
        for (var i = 0; i < mult.length; i++) {
            total += parseInt(cuit[i]) * mult[i];
        }
        var mod = total % 11;
        var digito = mod == 0 ? 0 : mod == 1 ? 9 : 11 - mod;
    }
    return digito == parseInt(cuit[10]);
}


// Starts with
String.prototype.startsWith = function (suffix) {
    return this.indexOf(suffix, 0) === 0;
};

// End width
String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

// ------------- CONFIGURE VALIDATIONS ------------------------------
$.qLibs_loadingWidget = function (widgetContainer, widgetClass) {
    $(widgetContainer).html('');
    $(widgetContainer).append('<div id="widget-loading-container" class="' + widgetClass + '"><div class="widget-loading portlet light bordered widget-container"></div></div>');
    $.qLibs_animatedBlockContainer(".widget-loading");
}
$.qLibs_addWidget = function (widgetContainer, data, button, reloadFunction) {
    $(widgetContainer).find('#widget-loading-container').first().remove();
    if (data != '')
        $(widgetContainer).append(data);
    var reloadButton = $(widgetContainer).find(button);
    if (reloadButton) {
        $(reloadButton).click(reloadFunction);
    }
}
$.qLibs_addEventClickButtonWidget = function (widgetContainer, button, reloadFunction) {
    var btn = $(widgetContainer).find(button);
    if (btn) {
        $(btn).click(reloadFunction);
    }
}

var qLibs_accentMap = {
    "á": "a",
    "é": "e",
    "í": "i",
    "ó": "o",
    "ú": "u"
};
$.qLibs_normalizeAccent = function (term) {
    var ret = "";
    for (var i = 0; i < term.length; i++) {
        ret += qLibs_accentMap[term.charAt(i)] || term.charAt(i);
    }
    return ret;
};

$.qLibs_addNew = function (widgetId, value, targetContainerToBlock, urlNewItem) {
    var widget = $("#" + widgetId).getKendoDropDownList();
    var dataSource = widget.dataSource;
    $.qLibs_blockContainer('#' + targetContainerToBlock);
    $.ajax({
        url: urlNewItem,
        type: 'POST',
        data: {
            sort: null,
            group: null,
            filter: null,
            ID: 0,
            TenantId: 0,
            Detalle: value,
            BaseEntityID: '0|0',
            HasID: true,
            Habilitado: true,
        },
        success: function (data, textStatus, jqXHR) {
            KTApp.unblock();
            $('.k-list-filter > input').val('');
            $("#" + widgetId).data("kendoDropDownList").dataSource.read();
            setTimeout(function () {
                $("#" + widgetId).data("kendoDropDownList").value(data.Data[0].ID);
                $("#" + widgetId).data("kendoDropDownList").close();
            }, 500);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('ERROR');
        }
    });
};

$.qLibs_getDate = function (input) {
    var date = $(input).datepicker('getDate');
    if (date != null)
        return ('0' + date.getDate()).slice(-2) + "/" + ('0' + (1 + date.getMonth())).slice(-2) + "/" + date.getFullYear();
    return null;
};

$.qLibs_substringMatcher = function (strs) {
    return function findMatches(q, cb) {
        var matches, substringRegex;
        // an array that will be populated with substring matches
        matches = [];
        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');
        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function (i, str) {
            if (substrRegex.test(str)) {
                matches.push(str);
            }
        });
        cb(matches);
    };
};

$.qLibs_convertDatetime = function (dateStr) {
    if (dateStr == null || dateStr == '')
        return '';
    var d = dateStr.toString().includes('/Date') ? new Date(parseInt(dateStr.slice(6, -2))) : new Date(dateStr);
    return '' + ('0' + d.getDate()).slice(-2) + '/' + ('0' + (1 + d.getMonth())).slice(-2) + '/' + d.getFullYear().toString().slice(-4) + ' ' + ('0' + d.getHours().toString()).slice(-2) + ':' + ('0' + d.getMinutes().toString()).slice(-2);
};

$.qLibs_convertDate = function (dateStr) {
    if (dateStr == null || dateStr == '')
        return '';
    var d = dateStr.toString().includes('/Date') ? new Date(parseInt(dateStr.slice(6, -2))) : new Date(dateStr);
    return '' + ('0' + d.getDate()).slice(-2) + '/' + ('0' + (1 + d.getMonth())).slice(-2) + '/' + d.getFullYear().toString().slice(-4);
};

$.qLibs_getDate = function (input) {
    var date = $(input).datepicker('getDate');
    if (date != null)
        return ('0' + date.getDate()).slice(-2) + "/" + ('0' + (1 + date.getMonth())).slice(-2) + "/" + date.getFullYear();
    return null;
};

$.qLibs_getTime = function (input) {
    var ds = moment($(input).val(), "MM/DD/YYYY", true).toDate();
    return "/Date(" + ds.getTime() + ")/";
};

$.qLibs_getPeriod = function (input) {
    var format = input.substring(0, 2);
    return format + "/" + input.substring(2, 6);
}
