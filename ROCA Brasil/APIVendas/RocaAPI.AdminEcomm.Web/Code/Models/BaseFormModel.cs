﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RocaAPI.AdminEcomm.Web.Code.Models
{
    public class BaseFormModel
    {
        public BaseFormModel()
        {
        }

        public BaseFormModel(string messageError)
        {
            this.ErrorMessage = messageError;
            this.HasError = true;
        }

        public string FormAction { get; set; }

        public string ErrorMessage { get; }

        public bool HasError { get; }
    }
}