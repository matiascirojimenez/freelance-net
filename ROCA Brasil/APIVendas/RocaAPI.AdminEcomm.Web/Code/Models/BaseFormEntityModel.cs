﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RocaAPI.AdminEcomm.Web.Code.Models
{
    public class BaseFormEntityModel<T> : BaseFormModel
        where T : new()
    {
        public BaseFormEntityModel()
            : base()
        {
        }

        public BaseFormEntityModel(string entityName)
            : base()
        {
            this.Entity = new T();
            this.FormAction = entityName + QLibs.Web.MVC.Menues.SiteMapHelper.CREATE_ACTION;
        }

        public BaseFormEntityModel(string entityName, T entity)
        {
            this.Entity = entity;
            this.FormAction = entityName + QLibs.Web.MVC.Menues.SiteMapHelper.UPDATE_ACTION;
        }

        public T Entity { get; set; }
    }
}