﻿using Microsoft.AspNet.Identity;
using QLibs.Web;
using QLibs.Web.MVC;
using QLibs.Web.MVC.Extensions;
using QLibs.Web.MVC.Menues;
using RocaAPI.AdminEcomm.Common.Entities.CORE.Seguridad;
using RocaAPI.AdminEcomm.Domain.Managers.CORE.Seguridad;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Caching;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;

namespace RocaAPI.AdminEcomm.Web.Code
{
    public static class SecurityManager
    {
        #region -- HasPermission --

        public static bool HasPermissionForProgram(string codigo)
        {
            return true;
            //var usuario = HttpContext.Current.User.GetUser();
            //if (usuario == null)
            //    return false;

            //if (usuario.TipoUsuario == TipoUsuarioEnum.Cliente)
            //{
            //    if (codigo.StartsWith("CLIENT"))
            //        return true;
            //    else
            //        return false;
            //}

            //if (ConfigurationManager.AppSettings["developMode"] == "true")
            //    return true;

            //var tenantCode = RocaAPI.AdminEcomm.Common.Constants.GetTenantCode();
            //var cache = new InMemoryCache();
            //var prgramasUSuario = cache.Get<List<string>>("SecurityManager.HasPermission." + usuario.BaseEntityID, () => new UsuarioManager().GetProgramasId(usuario), new CacheItemPolicy() { SlidingExpiration = new TimeSpan(0, 5, 0) });
            //if (prgramasUSuario.Contains(codigo))
            //    return true;
            //return false;
        }

        public static bool HasPermissionForMenu(QLibs.Web.MVC.Menues.Menu menu)
        {
            var area = menu.SiteMapKey;
            if (string.IsNullOrEmpty(menu.SiteMapKey) && menu.Childs != null)
                foreach (var child in menu.Childs)
                    if (HasPermissionForAction(child.SiteMapKey))
                        return true;
            if (HasPermissionForAction(menu.SiteMapKey))
                return true;

            if (menu.Childs != null && menu.Childs.Count > 0)
                foreach (var submenu in menu.Childs)
                    if (HasPermissionForMenu(submenu))
                        return true;
            return false;
        }

        public static bool HasPermissionForCRUDAction(ViewContext context, string entityName, JTableExtensions.jTableAction.jTableActionTypeEnum crudActionType, string customName = "")
        {
            var area = QLibsController.GetAreaName(context);
            var controller = QLibsController.GetControllerName(context);
            switch (crudActionType)
            {
                case JTableExtensions.jTableAction.jTableActionTypeEnum.Create:
                case JTableExtensions.jTableAction.jTableActionTypeEnum.Update:
                case JTableExtensions.jTableAction.jTableActionTypeEnum.Delete:
                    return HasPermissionForAction(area, controller, entityName + crudActionType.ToString());
                case JTableExtensions.jTableAction.jTableActionTypeEnum.Custom:
                default:
                    return HasPermissionForAction(area, controller, customName);
            }
        }

        public static bool HasPermissionForAction(string area, string controller, string action)
        {
            var key = QLibs.Web.MVC.Menues.SiteMap.GetKey(area, controller, action);
            return HasPermissionForAction(key);
        }

        public static bool HasPermissionForAction(string key)
        {
            //var program = SecurityManager.GetProgram(key);
            //if (program == null)
            //    return false;
            //return HasPermissionForProgram(program);
            return true;
        }

        #endregion

        #region -- User Methods --

        public static Usuario GetUser(this IPrincipal principal)
        {
            if (principal.Identity.IsAuthenticated)
            {
                var identity = (System.Security.Claims.ClaimsIdentity)principal.Identity;
                if (identity != null)
                    return new Usuario()
                    {
                        NombreUsuario =
                        principal.GetUserName(),
                        UsuarioId = principal.GetUserID(),
                    };
            }
            return null;
        }

        public static PasswordVerificationResult ValidateLogin(string userName, string password, bool isWebLogin = false)
        {
            using (var manager = new UsuarioManager())
            {
                var user = manager.GetBy(userName);
                if (user != null && user.Bloqueado == false && user.Habilitado == true)
                {
                    if (VerifyHashedPassword(user.Clave, password) == PasswordVerificationResult.Success)
                        return PasswordVerificationResult.Success;
                    else if (user.Clave == password)
                    {
                        /// Si no esta encriptada, maraca para cambio
                        //manager.SeteaCambioPassword(user.UsuarioId);
                        return PasswordVerificationResult.Success;
                    }
                }
            }
            return PasswordVerificationResult.Failed;
        }

        //public static bool ChangePassword(int userId, string oldpassword, string newpassword, bool ignoreOldPassword = false)
        //{
        //    using (UsuarioManager manager = new UsuarioManager())
        //    {
        //        var userDB = manager.GetById(new Usuario() { UsuarioId = userId });
        //        if (userDB != null)
        //        {
        //            if (VerifyHashedPassword(userDB.Clave, oldpassword) == PasswordVerificationResult.Success || userDB.Clave == oldpassword || ignoreOldPassword)
        //            //if(true)
        //            {
        //                manager.ChangePassword(userDB.UsuarioId, Guid.NewGuid(), HashPassword(newpassword));
        //                return true;
        //            }
        //        }
        //    }
        //    return false;
        //}

        internal static string HashPassword(string password)
        {
            return new Microsoft.AspNet.Identity.PasswordHasher().HashPassword(password);
        }

        internal static PasswordVerificationResult VerifyHashedPassword(string hashed, string password)
        {
            try
            {
                return new Microsoft.AspNet.Identity.PasswordHasher().VerifyHashedPassword(hashed, password);
            }
            catch (Exception ex)
            {
                QLibs.Logging.LogHelper.Instance.LogException(ex);
                return PasswordVerificationResult.Failed;
            }
        }

        #endregion
    }
}
