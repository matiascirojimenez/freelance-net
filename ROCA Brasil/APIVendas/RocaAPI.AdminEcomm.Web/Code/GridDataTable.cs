﻿using QLibs.Common.DTO.Queries;
using QLibs.Common.Entities;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace RocaAPI.AdminEcomm.Web.Code
{
    public class GridDataTableData
    {
        public static object JsonResult<TEntity>(QueryResults<TEntity> queryResult, object dto = null)
            where TEntity : BaseEntity
        {
            return new
            {
                recordsFiltered = queryResult.TotalVirtualRows,
                recordsTotal = queryResult.TotalVirtualRows,
                data = queryResult.CurrentPageResult,
                draw = HttpContext.Current.Request["draw"],
                totales = dto
            };
        }
        public static object JsonResult<TEntity>(List<TEntity> queryResult)
            where TEntity : BaseEntity
        {
            return new
            {
                recordsFiltered = queryResult.Count,
                recordsTotal = queryResult.Count,
                data = queryResult,
                draw = HttpContext.Current.Request["draw"]
            };
        }
    }

    public class GridDataTable<TQF>
            where TQF : BaseQueryFilters
    {
        public GridDataTable()
        {
            Filters = System.Activator.CreateInstance<TQF>();
        }

        public GridDataTable(System.Web.HttpRequestBase request, params OrderByColumn[] defaultSort)
        {
            PaginationData = new DataTablePagination(request);
            SortData = new DataTableSort(request);

            Filters = System.Activator.CreateInstance<TQF>();
            Filters.CurrentPage = PaginationData.startIndex / PaginationData.perpage;
            Filters.PageSize = PaginationData.perpage;
            var sortText = SortData.GetSort();
            if (!string.IsNullOrEmpty(sortText))
            {
                Filters.OrderByColumns = new System.Collections.Generic.List<OrderByColumn>();
                string[] sorts = sortText.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string sort in sorts)
                {
                    var columName = sort.Replace(" ASC", "").Replace(" DESC", "").Replace(" asc", "").Replace(" desc", "");
                    var direction = OrderBySortDirection.Ascending;
                    if (sort.ToUpper().Contains(" DESC"))
                        direction = OrderBySortDirection.Descending;
                    Filters.OrderByColumns.Add(new OrderByColumn(columName, direction));
                }
            }
            else
            {
                if (defaultSort != null)
                    foreach (var ds in defaultSort)
                        Filters.OrderByColumns.Add(ds);
            }

            Filters.FindValue = request["Filters[FindValue]"];
            if (request["Filters[DynamicCols]"] != null)
                Filters.ColumnasDinamicas = request["Filters[DynamicCols]"].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        public TQF Filters { get; set; }

        public DataTablePagination PaginationData { get; set; }

        public DataTableSort SortData { get; set; }
    }

    [Serializable]
    public class DataTablePagination
    {
        public int startIndex { get; set; }
        public int perpage { get; set; }

        public DataTablePagination()
        {
        }
        public DataTablePagination(HttpRequestBase request)
        {
            perpage = int.Parse(request["length"]);
            startIndex = int.Parse(request["start"]);
        }
    }

    [Serializable]
    public class DataTableSort
    {
        private string sortData = "";

        public string GetSort()
        {
            return sortData;
        }

        public DataTableSort()
        {
        }

        public DataTableSort(HttpRequestBase request)
        {
            ///order[0][column]: 1
            ///order[0][dir]: asc
            ///columns[0][data]: IdCuentaCorriente
            ///columns[0][name]: 
            ///columns[0][searchable]: true
            ///columns[0][orderable]: true
            ///columns[0][search][value]: 
            ///columns[0][search][regex]: false
            foreach (var key in request.Params.AllKeys)
            {
                if (key.StartsWith("order[") && key.EndsWith("][column]"))
                {
                    int colIndex = 0;
                    if (int.TryParse(request[key], out colIndex))
                    {
                        int sortIndex = int.Parse(key.Replace("order[", "").Replace("][column]", ""));
                        var sentido = request["order[" + sortIndex + "][dir]"];

                        var nombre = request["columns[" + colIndex + "][data]"];
                        sortData += nombre + " " + sentido + ",";
                    }
                }
            }
        }
    }
}
