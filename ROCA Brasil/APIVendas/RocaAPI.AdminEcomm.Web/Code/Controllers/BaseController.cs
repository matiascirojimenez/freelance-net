﻿using HiQPdf;
using Infragistics.Documents.Excel;
using QLibs.Common.DTO;
using QLibs.Common.Entities;
using QLibs.Common.Exceptions;
using QLibs.Common.Utils;
using QLibs.Excel;
using QLibs.Logging;
using QLibs.Web;
using QLibs.Web.MVC;
using RocaAPI.AdminEcomm.Common.DTO.Sincronizacion;
using RocaAPI.AdminEcomm.Domain.Managers;
using RocaAPI.AdminEcomm.Web.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace RocaAPI.AdminEcomm.Web.Code.Controllers
{
    public class BaseController : QLibsController
    {
        internal static string DownloadFileAndResize(string url, string path, string codigoProduto, string tipoImagen, int width)
        {
            try
            {
                var ext = url.Substring(url.LastIndexOf('.') + 1);
                var filename = width + "_" + codigoProduto + "_" + tipoImagen + "." + ext;
                var downloadTo = System.IO.Path.Combine(path, filename);
                if (System.IO.File.Exists(downloadTo))
                {
                    if (new FileInfo(downloadTo).CreationTime.Date == DateTime.Now.Date)
                        return downloadTo;
                    else
                        System.IO.File.Delete(downloadTo);
                }

                using (WebClient webClient = new WebClient())
                {
                    webClient.DownloadFile(url, downloadTo);
                    ResizeJpg(downloadTo, width, width);
                    return downloadTo;
                }
            }
            catch (Exception ex)
            {
                QLibs.Logging.LogHelper.Instance.LogException(ex);
                return "";
            }
        }

        internal static void ResizeJpg(string path, int nWidth, int nHeight)
        {
            int newHeight = 0;
            int newWidth = 0;
            System.Drawing.Image thumbimage = null;
            using (var originalImage = new System.Drawing.Bitmap(path))
            {
                var originalWidth = originalImage.Width;
                var originalHeight = originalImage.Height;
                double ratioX = (double)nWidth / (double)originalWidth;
                double ratioY = (double)nHeight / (double)originalHeight;
                // use whichever multiplier is smaller
                double ratio = ratioX < ratioY ? ratioX : ratioY;
                newHeight = Convert.ToInt32(originalHeight * ratio);
                newWidth = Convert.ToInt32(originalWidth * ratio);
                thumbimage = originalImage.GetThumbnailImage(newWidth, newHeight, delegate () { return false; }, IntPtr.Zero);
            }
            using (var result = new System.Drawing.Bitmap(nWidth, nHeight, PixelFormat.Format32bppArgb))
            {
                using (var g = System.Drawing.Graphics.FromImage((System.Drawing.Image)result))
                {
                    g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighSpeed;
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighSpeed;
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

                    g.FillRectangle(new System.Drawing.SolidBrush(System.Drawing.Color.White), 0, 0, nWidth, nHeight);
                    g.DrawImage(thumbimage, (nWidth - newWidth) / 2, (nHeight - newHeight) / 2, newWidth, newHeight);
                    var ici = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders().FirstOrDefault(ie => ie.MimeType == "image/jpeg");
                    var eps = new System.Drawing.Imaging.EncoderParameters(1);
                    eps.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
                    result.Save(path, ici, eps);
                }
            }
        }


        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            // Attempt to read the culture cookie from Request
            HttpCookie cultureCookie = Request.Cookies["sav_culture"];
            if (cultureCookie != null)
            {
                ChangeCulture(cultureCookie.Value);
            }

            return base.BeginExecuteCore(callback, state);
        }

        protected void ChangeCulture(string cultureName)
        {
            if (Thread.CurrentThread.CurrentCulture.Name != cultureName)
            {
                if (cultureName == "en-US" || cultureName == "es-MX")
                {
                    HttpCookie langCookie = new HttpCookie("sav_culture", cultureName);
                    langCookie.Expires = DateTime.Now.AddYears(1);
                    System.Web.HttpContext.Current.Response.Cookies.Add(langCookie);

                    Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
                    Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
                }
            }
        }

        [Authorize]
        [IgnoreAudit]
        [AuthorizeIgnoreValidation]
        public ActionResult Import(string a)
        {
            return PartialView("~/Views/Shared/Partials/Import.cshtml", new ImportModel(a));
        }

        public static MVCInfo GetMVCInfoValited(ViewContext context, UrlHelper Url, bool detail = true, bool create = true, bool update = true, bool delete = true, bool reactivate = true, bool import = true, bool export = true)
        {
            var info = QLibsController.GetMVCInfo(context, Url);
            if (detail)
                if (!RocaAPI.AdminEcomm.Web.Code.SecurityManager.HasPermissionForAction(info.DetailActionKey)) { info.DetailAction = ""; }
            if (create)
                if (!RocaAPI.AdminEcomm.Web.Code.SecurityManager.HasPermissionForAction(info.CreateActionKey)) { info.CreateAction = ""; }
            if (update)
                if (!RocaAPI.AdminEcomm.Web.Code.SecurityManager.HasPermissionForAction(info.UpdateActionKey)) { info.UpdateAction = ""; }
            if (delete)
                if (!RocaAPI.AdminEcomm.Web.Code.SecurityManager.HasPermissionForAction(info.DeleteActionKey)) { info.DeleteAction = ""; }
            if (reactivate)
                if (!RocaAPI.AdminEcomm.Web.Code.SecurityManager.HasPermissionForAction(info.ReactivateAction)) { info.ReactivateAction = ""; }
            if (import)
                if (!RocaAPI.AdminEcomm.Web.Code.SecurityManager.HasPermissionForAction(info.ImportActionKey)) { info.ImportAction = ""; }
            if (export)
                if (!RocaAPI.AdminEcomm.Web.Code.SecurityManager.HasPermissionForAction(info.ExportActionKey)) { info.ExportAction = ""; }
            return info;
        }

        internal JsonResult JsonError(string donde, Exception ex = null)
        {
            if (ex != null)
                LogHelper.Instance.LogException(ex, donde);
            return Json(new { actionResult = false, message = "Error inesperado. (COD999)", redirectURL = "" });
        }

        protected string GetHomeURL(string returnUrl = "")
        {
            if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
                returnUrl = Url.Action("Index", "Home");
            return returnUrl;
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controller = GetControllerName(filterContext);

            try
            {
                if (controller != "Errors")
                    if (BaseController.ValidateOutOfService(HttpContext.Server.MapPath("~/"), HttpContext.Session))
                    {
                        filterContext.Result = new RedirectResult("~/Errors/OutOfService");
                        return;
                    }
            }
            catch (Exception)
            {
            }


            base.OnActionExecuting(filterContext);
        }

        protected bool? SoloHabilitado(string onlyactive)
        {
            if (!string.IsNullOrEmpty(onlyactive) && onlyactive == "true")
                return true;
            return null;
        }

        public static bool ValidateOutOfService(string rootPath, HttpSessionStateBase session = null)
        {
            var filePath = Path.Combine(rootPath, "NO-SERVICE.XML");
            if (System.IO.File.Exists(filePath))
            {
                var xml = System.IO.File.ReadAllText(filePath);
                var outOf = new QLibs.Common.Utils.GenericXmlSerializer<OutOfServiceDTO>().Deserialize(xml);
                if (outOf != null)
                {
                    if (outOf.From < DateTime.UtcNow && outOf.To > DateTime.UtcNow)
                    {
                        if (session != null)
                        {
                            if (session["OutOfServiceDateTo"] == null)
                                session.Add("OutOfServiceDateTo", outOf.To);
                            if (session["OutOfServiceMessage"] == null)
                                session.Add("OutOfServiceMessage", outOf.Message);
                        }
                        return true;
                    }
                    else
                    {
                        System.IO.File.Delete(filePath);
                    }
                }
            }
            return false;
        }
        protected JsonResult ResultImport(ResultadoSincronizacionERP result)
        {
            if (result.Resultado == ResultadoCargaLoteEnum.Ok)
                return base.GetJsonResultOk(result, "SuccessOperation", DoActionEnum.ExecuteMethod, "submitFormEditFormOKDefault");
            else if (result.Resultado == ResultadoCargaLoteEnum.OkConErrores)
                return base.GetJsonResultOk(result, "SuccessOperationWithErrors", DoActionEnum.ExecuteMethod, "submitFormEditFormOKDefault");
            else
                return base.GetJsonResultErrorMessage(new string[] { result.Mensaje });
        }

        protected ActionResult ExportToExcel<TEntity>(List<TEntity> data, List<string> ignoreCols = null, bool reorder = false, List<string> colOrder = null, string Name = null)
        {
            //[2] Armando el Excel
            Workbook wbk = ExcelHelper.Export<TEntity>(data, ignoreCols, reorder, colOrder);
            //[3] Guardando en carpeta temporal
            string path = GetTempFolderPath(User.GetTenantID());
            string filePath = Path.Combine(path, Guid.NewGuid().ToString());
            wbk.Save(filePath);
            return File(filePath, "application/vnd.ms-excel", typeof(TEntity).Name.Replace("DTO", "") + "_" + (!string.IsNullOrEmpty(Name) ? Name + "_" : "") + DateTime.UtcNow.ToString("yyyy_MM_dd") + ".xls");
        }

        protected TEntity[] ImportFromFile<TEntity>(HttpRequestBase Request)
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                if (file == null || file.ContentLength == 0)
                    throw new QException("No se encontraron archivos para importar!");
                if (file != null && file.ContentLength > 0)
                {
                    string fileExtension = Path.GetExtension(file.FileName).Trim().ToLower().TrimStart('.');
                    if (fileExtension == "xls" || fileExtension == "xlsx")
                    {
                        var data = ExcelHelper.GetDataFromExcel<TEntity>(Workbook.Load(file.InputStream));
                        if (data == null || data.Count == 0)
                            throw new QException("El archivo a importar esta vacio!");
                        if (data != null && data.Count > 0)
                            return (TEntity[])data.ToArray(typeof(TEntity));
                        return null;
                    }
                }
            }
            return null;
        }

        protected string GetTempFolderPath(int tenant)
        {
            string path = Server.MapPath(string.Format("~/_tempfiles/{0}/{1}", DateTime.Now.ToString("yyyyMMdd"), tenant));
            if (Directory.Exists(path) == false)
                Directory.CreateDirectory(path);
            return path;
        }

        protected List<string> GetOldTempFolderPaths()
        {
            List<string> result = new List<string>();
            string path = Server.MapPath("~/_tempfiles/");
            if (Directory.Exists(path))
            {
                foreach (var dir in Directory.GetDirectories(path))
                {
                    if (int.Parse(new DirectoryInfo(dir).Name) < int.Parse(DateTime.Now.ToString("yyyyMMdd")))
                        result.Add(dir);
                }
            }
            return result;
        }

        protected byte[] GenerarPDF(object model, string view)
        {
            try
            {
                string htmlToConvert = RenderView(this, view, model);

                HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
                htmlToPdfConverter.Document.PageOrientation = PdfPageOrientation.Portrait;
                byte[] pdfBuffer = htmlToPdfConverter.ConvertHtmlToMemory(htmlToConvert, ControllerContext.HttpContext.Request.Url.AbsoluteUri);

                return pdfBuffer;
            }
            catch (Exception ex)
            {
                LogHelper.Instance.LogException(ex);
                throw ex;
            }
        }

        protected string RenderView(Controller controller, string viewName, object model)
        {
            controller.ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        [IgnoreAuditAttribute]
        [AuthorizeIgnoreValidation]
        public ActionResult GetPageLogo(bool loginLogo = false, int? tenantId = null)
        {
            WebImage img;
            try
            {
                if (tenantId.HasValue == false)
                    tenantId = User.GetTenantID();
                var logoPath = "~/_files/" + tenantId + "/html/logoHeader.png";
                if (System.IO.File.Exists(Server.MapPath(logoPath)))
                    img = new WebImage(logoPath);
                else
                    img = new WebImage("~/_Content/images/logo_default_dark.png");
            }
            catch (Exception)
            {
                img = new WebImage("~/_Content/images/logo_default_dark.png");
            }
            return File(img.GetBytes(), img.GetType().ToString());
        }

        #region -- Validacion seguridad --

        protected override bool ValidateAction(ActionExecutingContext filterContext)
        {
            //var program = SecurityManager.GetCurrentProgram(filterContext);
            //if (program == null)
            //    return false;
            //bool autorized = SecurityManager.HasPermissionForProgram(program);
            //if (!autorized)
            //{
            //    filterContext.Result = new RedirectResult("~/Errors/ER403");
            //    return false;
            //}
            //else
            return true;
        }

        #endregion

        #region -- Auditoria --

        protected override void AuditAction(ActionExecutingContext filterContext)
        {
        }

        #endregion

        #region -- Combos --

        [HttpPost]
        [Authorize]
        [IgnoreAuditAttribute]
        [AuthorizeIgnoreValidationAttribute]
        public JsonResult GetJSONCombo(GetJSONComboEntityType e, string selectedValue = null, bool emptyValue = false, string parentId = null, string emptyValueText = "", string extradata = "", bool usecache = true, string text = null)
        {
            try
            {
                IEnumerable<IJSONComboOptionEntity> options = new List<IJSONComboOptionEntity>();
                var cache = new InMemoryCache();
                switch (e)
                {
                    case GetJSONComboEntityType.Brand:
                        foreach (var item in new ProdutoManager().GetBrand())
                            ((List<IJSONComboOptionEntity>)options).Add(new JSONComboOptionEntity() { Value = item, Description = item });
                        break;
                    case GetJSONComboEntityType.Line:
                        foreach (var item in new ProdutoManager().GetLines())
                            ((List<IJSONComboOptionEntity>)options).Add(new JSONComboOptionEntity() { Value = item, Description = item });
                        break;
                    case GetJSONComboEntityType.Category:
                        foreach (var item in new ProdutoManager().GetCategories())
                            ((List<IJSONComboOptionEntity>)options).Add(new JSONComboOptionEntity() { Value = item, Description = item });
                        break;
                    case GetJSONComboEntityType.Type:
                        foreach (var item in new ProdutoManager().GetTypes())
                            ((List<IJSONComboOptionEntity>)options).Add(new JSONComboOptionEntity() { Value = item, Description = item });
                        break;
                    case GetJSONComboEntityType.SubType:
                        foreach (var item in new ProdutoManager().GetSubTypes())
                            ((List<IJSONComboOptionEntity>)options).Add(new JSONComboOptionEntity() { Value = item, Description = item });
                        break;
                    case GetJSONComboEntityType.Client:
                        options = new ClienteManager().Query(new Common.Queries.ClienteQueryFilters());
                        break;
                    default:
                        break;
                }

                var optionList = options.Where(d => d.Habilitado || selectedValue == d.GetValue().Trim().ToUpper()).ToList();
                if (emptyValue)
                    optionList.Insert(0, new JSONComboOptionEntity() { Value = "", Description = emptyValueText });
                if (!string.IsNullOrEmpty(text))
                    optionList = optionList.Where(w => w.GetDescription().ToLower().Contains(text.ToLower())).ToList();

                options = optionList.ToList();

                return GetJsonResultComboOptions(options, selectedValue);
            }
            catch (Exception ex)
            {
                return GetJsonResultErrorMessage(ex);
            }
        }

        #endregion

        #region -- RedirectError --

        [IgnoreAuditAttribute]
        protected ActionResult RedirectError(Exception ex)
        {
            var error = Guid.NewGuid().ToString();
            Session.Add(error, ex.Message);
            return Redirect(Url.Action("error", "Errors", new { Area = "", m = error }));
        }

        [IgnoreAuditAttribute]
        protected ActionResult RedirectError(string messageCode, params object[] parameters)
        {
            var error = Guid.NewGuid().ToString();
            Session.Add(error, ResourceManagerHelper.GetString(messageCode, parameters));
            return Redirect(Url.Action("error", "Errors", new { Area = "", m = error }));
        }

        #endregion

        #region -- SiteMapPage --

        public static string GetWebTitle(ViewContext context)
        {
            var usuario = context.HttpContext.User.GetUser();
            if (usuario != null)
            {
                var env = context.HttpContext.User.GetTenantCode();
                return env + " - Ecommerce Manager";
            }
            return "ROCA - Ecommerce Manager";
        }

        public static MvcHtmlString SiteMapPageTitle(ViewContext context)
        {
            string area = QLibs.Web.MVC.QLibsController.GetAreaName(context);
            string controller = QLibs.Web.MVC.QLibsController.GetControllerName(context);
            string action = QLibs.Web.MVC.QLibsController.GetActionName(context);
            return SiteMapPageTitle(area, controller, action);
        }
        public static MvcHtmlString SiteMapPageTitle(ViewContext context, string action)
        {
            string area = QLibs.Web.MVC.QLibsController.GetAreaName(context);
            string controller = QLibs.Web.MVC.QLibsController.GetControllerName(context);
            return SiteMapPageTitle(area, controller, action);
        }

        public static MvcHtmlString SiteMapPageTitle(string area, string controller, string action)
        {
            return new MvcHtmlString(QLibs.Web.MVC.Menues.SiteMapHelper.SiteMapPageTitle(area, controller, action));
        }
        public static MvcHtmlString SiteMapIcon(ViewContext context)
        {
            string area = QLibs.Web.MVC.QLibsController.GetAreaName(context);
            string controller = QLibs.Web.MVC.QLibsController.GetControllerName(context);
            string action = QLibs.Web.MVC.QLibsController.GetActionName(context);
            return SiteMapIcon(area, controller, action);
        }

        public static MvcHtmlString SiteMapIcon(string area, string controller, string action)
        {
            return new MvcHtmlString(QLibs.Web.MVC.Menues.MenuHelper.GetIcon(area, controller, action));
        }

        #endregion

        #region -- Static --

        public static byte[] ResizeImage(HttpPostedFileBase file, int width, int height, bool paddingImage)
        {
            using (Bitmap postedImage = new Bitmap(file.InputStream))
            {
                return BaseController.scaleImage(postedImage, width, height, paddingImage);
            }
        }


        #region -- scaleImage --

        public static byte[] scaleImage(Image image, int maxWidth, int maxHeight, bool padImage)
        {
            try
            {
                int newWidth;
                int newHeight;
                byte[] returnArray;

                //check if the image needs rotating (eg phone held vertical when taking a picture for example)
                foreach (var prop in image.PropertyItems)
                {
                    if (prop.Id == 0x0112)
                    {
                        int rotateValue = image.GetPropertyItem(prop.Id).Value[0];
                        RotateFlipType flipType = getRotateFlipType(rotateValue);
                        image.RotateFlip(flipType);
                        break;
                    }
                }

                //apply padding if needed
                if (padImage == true)
                {
                    image = applyPaddingToImage(image);
                }

                //check if the with or height of the image exceeds the maximum specified, if so calculate the new dimensions
                if (image.Width > maxWidth || image.Height > maxHeight)
                {
                    var ratioX = (double)maxWidth / image.Width;
                    var ratioY = (double)maxHeight / image.Height;
                    var ratio = Math.Min(ratioX, ratioY);

                    newWidth = (int)(image.Width * ratio);
                    newHeight = (int)(image.Height * ratio);
                }
                else
                {
                    newWidth = image.Width;
                    newHeight = image.Height;
                }

                //start with a new image
                var newImage = new Bitmap(newWidth, newHeight);

                //set the new resolution, 72 is usually good enough for displaying images on monitors
                newImage.SetResolution(72, 72);
                //or use the original resolution
                //newImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

                //resize the image
                using (var graphics = Graphics.FromImage(newImage))
                {
                    graphics.CompositingMode = CompositingMode.SourceCopy;
                    graphics.CompositingQuality = CompositingQuality.HighQuality;
                    graphics.SmoothingMode = SmoothingMode.HighQuality;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                    graphics.DrawImage(image, 0, 0, newWidth, newHeight);
                }
                image = newImage;

                //save the image to a memorystream to apply the compression level, higher compression = better quality = bigger images
                using (MemoryStream ms = new MemoryStream())
                {
                    EncoderParameters encoderParameters = new EncoderParameters(1);
                    encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, 80L);
                    image.Save(ms, getEncoderInfo("image/jpeg"), encoderParameters);

                    //save the stream as byte array
                    returnArray = ms.ToArray();
                }

                //cleanup
                image.Dispose();
                newImage.Dispose();

                return returnArray;
            }
            catch (Exception)
            {
                //there was an error: ex.Message
                return null;
            }
        }

        private static ImageCodecInfo getEncoderInfo(string mimeType)
        {
            ImageCodecInfo[] encoders = ImageCodecInfo.GetImageEncoders();
            for (int j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType.ToLower() == mimeType.ToLower())
                    return encoders[j];
            }
            return null;
        }

        private static Image applyPaddingToImage(Image image)
        {
            //get the maximum size of the image dimensions
            int maxSize = Math.Max(image.Height, image.Width);
            Size squareSize = new Size(maxSize, maxSize);

            //create a new square image
            Bitmap squareImage = new Bitmap(squareSize.Width, squareSize.Height);

            using (Graphics graphics = Graphics.FromImage(squareImage))
            {
                //fill the new square with a color
                graphics.FillRectangle(Brushes.White, 0, 0, squareSize.Width, squareSize.Height);

                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                //put the original image on top of the new square
                graphics.DrawImage(image, (squareSize.Width / 2) - (image.Width / 2), (squareSize.Height / 2) - (image.Height / 2), image.Width, image.Height);
            }

            return squareImage;
        }

        private static RotateFlipType getRotateFlipType(int rotateValue)
        {
            RotateFlipType flipType = RotateFlipType.RotateNoneFlipNone;

            switch (rotateValue)
            {
                case 1:
                    flipType = RotateFlipType.RotateNoneFlipNone;
                    break;
                case 2:
                    flipType = RotateFlipType.RotateNoneFlipX;
                    break;
                case 3:
                    flipType = RotateFlipType.Rotate180FlipNone;
                    break;
                case 4:
                    flipType = RotateFlipType.Rotate180FlipX;
                    break;
                case 5:
                    flipType = RotateFlipType.Rotate90FlipX;
                    break;
                case 6:
                    flipType = RotateFlipType.Rotate90FlipNone;
                    break;
                case 7:
                    flipType = RotateFlipType.Rotate270FlipX;
                    break;
                case 8:
                    flipType = RotateFlipType.Rotate270FlipNone;
                    break;
                default:
                    flipType = RotateFlipType.RotateNoneFlipNone;
                    break;
            }

            return flipType;
        }

        #endregion

        public static byte[] ResizeAvatar(byte[] data, int width = 41, int height = 41, bool preservAspectRatio = true, bool squareSize = true)
        {
            var img = new WebImage(data);
            if (squareSize)
            {
                if (img.Width > img.Height)
                {
                    int cropLeft = (img.Width - img.Height) / 2;
                    img.Crop(0, cropLeft, 0, cropLeft);
                }
                else if (img.Width < img.Height)
                {
                    int cropLeft = (img.Height - img.Width) / 2;
                    img.Crop(cropLeft, 0, cropLeft, 0);
                }
            }
            img.Resize(width, height, preservAspectRatio);
            img.Write();
            return img.GetBytes();
        }

        #endregion

        #region -- GetNotifications --

        [IgnoreAuditAttribute]
        public PartialViewResult GetNotifications()
        {
            var model = new NotificationsModel();
            return PartialView("~/Views/Shared/Partials/Header/Notifications.cshtml", model);
        }

        #endregion

        #region -- Grid Export --

        [HttpPost]
        public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);
            return File(fileContents, contentType, fileName);
        }

        [HttpPost]
        public ActionResult Pdf_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);
            return File(fileContents, contentType, fileName);
        }

        #endregion

        #region -- Version Validation Methods --

        public static bool ValidateCompatibleVersion()
        {
            if (System.Web.HttpContext.Current.Application["ValidateCompatibleVersion"] == null || (bool)System.Web.HttpContext.Current.Application["ValidateCompatibleVersion"] == false)
            {
                var version = GetVersionNumber().ToString();
                System.Web.HttpContext.Current.Application.Remove("ValidateCompatibleVersion");
            }
            return (bool)System.Web.HttpContext.Current.Application["ValidateCompatibleVersion"];
        }

        public static long GetVersionNumber()
        {
            if (System.Web.HttpContext.Current.Application["AssemblyVersion"] == null)
            {
                var assem = System.Reflection.Assembly.GetCallingAssembly();
                var assemName = assem.GetName();
                //      Major Version
                //      Minor Version
                //      Build Number
                //      Revision
                var version = assemName.Version.Major.ToString("0000") + assemName.Version.Minor.ToString("00") + assemName.Version.Build.ToString("00") + assemName.Version.Revision.ToString("00000");
                System.Web.HttpContext.Current.Application.Add("AssemblyVersion", long.Parse(version));
            }
            return (long)System.Web.HttpContext.Current.Application["AssemblyVersion"];
        }

        public static string GetVersionNumberFormatted()
        {
            var version = GetVersionNumber().ToString();
            return version.Substring(0, 4) + "." + version.Substring(4, 2) + "." + version.Substring(6, 2) + "." + version.Substring(8, 5);
        }

        #endregion
    }
}