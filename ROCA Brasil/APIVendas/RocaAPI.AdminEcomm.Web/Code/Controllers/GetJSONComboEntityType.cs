﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RocaAPI.AdminEcomm.Web.Code.Controllers
{
    public enum GetJSONComboEntityType
    {
        Brand = 1,
        Line,
        Category,
        Type,
        SubType,
        Client
    }
}