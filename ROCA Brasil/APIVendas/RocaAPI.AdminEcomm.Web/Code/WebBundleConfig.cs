﻿using QLibs.Web;
using QLibs.Web.MVC;
using System.Web.Optimization;

namespace RocaAPI.AdminEcomm.Web.Code
{
    public class WebBundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            var css = new StyleImagePathBundle("~/metronic/css");

            ///< !--begin:: Global Mandatory Vendors -->
            css.Include("~/_Content/metronic_v603/assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css");
            ///< !--end:: Global Mandatory Vendors -->
            ///< !--begin::Global Mandatory Vendors -->
            css.Include("~/_Content/metronic_v603/assets/vendors/general/tether/dist/css/tether.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/general/bootstrap-daterangepicker/daterangepicker.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/general/select2/dist/css/select2.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/general/ion-rangeslider/css/ion.rangeSlider.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/general/nouislider/distribute/nouislider.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/general/owl.carousel/dist/assets/owl.carousel.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/general/owl.carousel/dist/assets/owl.theme.default.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/general/dropzone/dist/dropzone.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/general/summernote/dist/summernote.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/general/animate.css/animate.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/general/toastr/build/toastr.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/general/morris.js/morris.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/general/sweetalert2/dist/sweetalert2.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/general/socicon/css/socicon.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/custom/vendors/line-awesome/css/line-awesome.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/custom/vendors/flaticon/flaticon.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/custom/vendors/flaticon2/flaticon.css");
            css.Include("~/_Content/metronic_v603/assets/vendors/general/@fortawesome/fontawesome-free/css/all.min.css");
            ///< !--end::Global Mandatory Vendors -->

            var js = new ScriptBundle("~/metronic/js");
            /// <!--begin:: Global Mandatory Vendors -->
            js.Include("~/_Content/metronic_v603/assets/vendors/general/jquery/dist/jquery.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/popper.js/dist/umd/popper.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/bootstrap/dist/js/bootstrap.min.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/js-cookie/src/js.cookie.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/moment/min/moment.min.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/sticky-js/dist/sticky.min.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/wnumb/wNumb.js");

            /// <!--end:: Global Mandatory Vendors -->
            /// <!--begin:: Global Optional Vendors -->
            js.Include("~/_Content/metronic_v603/assets/vendors/general/jquery-form/dist/jquery.form.min.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/block-ui/jquery.blockUI.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/custom/js/vendors/bootstrap-datepicker.init.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/custom/js/vendors/bootstrap-timepicker.init.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/bootstrap-daterangepicker/daterangepicker.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/bootstrap-maxlength/src/bootstrap-maxlength.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/custom/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/bootstrap-select/dist/js/bootstrap-select.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/bootstrap-switch/dist/js/bootstrap-switch.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/custom/js/vendors/bootstrap-switch.init.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/select2/dist/js/select2.full.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/ion-rangeslider/js/ion.rangeSlider.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/typeahead.js/dist/typeahead.bundle.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/handlebars/dist/handlebars.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/inputmask/dist/jquery.inputmask.bundle.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/inputmask/dist/inputmask/inputmask.date.extensions.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/nouislider/distribute/nouislider.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/owl.carousel/dist/owl.carousel.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/autosize/dist/autosize.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/clipboard/dist/clipboard.min.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/dropzone/dist/dropzone.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/summernote/dist/summernote.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/markdown/lib/markdown.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/bootstrap-markdown/js/bootstrap-markdown.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/custom/js/vendors/bootstrap-markdown.init.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/bootstrap-notify/bootstrap-notify.min.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/custom/js/vendors/bootstrap-notify.init.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/jquery-validation/dist/jquery.validate.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/jquery-validation/dist/additional-methods.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/custom/js/vendors/jquery-validation.init.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/toastr/build/toastr.min.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/raphael/raphael.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/morris.js/morris.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/chart.js/dist/Chart.bundle.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/custom/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/custom/vendors/jquery-idletimer/idle-timer.min.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/waypoints/lib/jquery.waypoints.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/counterup/jquery.counterup.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/es6-promise-polyfill/promise.min.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/sweetalert2/dist/sweetalert2.min.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/custom/js/vendors/sweetalert2.init.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/jquery.repeater/src/lib.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/jquery.repeater/src/jquery.input.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/jquery.repeater/src/repeater.js");
            js.Include("~/_Content/metronic_v603/assets/vendors/general/dompurify/dist/purify.js");
            /// <!--end:: Global Optional Vendors -->

            bundles.Add(css);
            bundles.Add(js);

            if (QWebUtils.IsDevelopMode() == false)
                BundleTable.EnableOptimizations = true;
        }
    }

}