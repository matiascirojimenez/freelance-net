﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using QLibs.Common.Exceptions;
using QLibs.Logging;
using QLibs.Web;
using QLibs.Web.MVC.App_Start;
using QLibs.Web.MVC.Menues;
using RocaAPI.AdminEcomm.Web.Code;
using System;
using System.Configuration;
using System.Security.Claims;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

[assembly: Microsoft.Owin.OwinStartup(typeof(RocaAPI.AdminEcomm.Web.Startup))]

namespace RocaAPI.AdminEcomm.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
        //private double Redondear(double input, int places)
        //{
        //    double multiplier = Math.Pow(10, Convert.ToDouble(places));
        //    return Math.Ceiling(input * multiplier) / multiplier;
        //}

        protected void Application_Start()
        {
            //var d = Redondear(81.93 * 0.1, 2);

            MvcApplication.ValidateConfigurationToStart();
            WebBundleConfig.RegisterBundles(BundleTable.Bundles);
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes, "RocaAPI.AdminEcomm.Web");
            UnityConfig.RegisterComponents();
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;

            MenuHelper.FillMenu();
            SiteMapHelper.FillSiteMap();
        }

        private static void ValidateConfigurationToStart()
        {
            if (QWebUtils.IsTraceMode())
                QLibs.DataAccess.QDatabase.DoTrace = true;

            ///---------------------VALIDAION DECIMALES---------------------------------------------------
            var decimalValue = (decimal)1222.4448;
            var numberHtmlInput = QLibs.Web.MVC.Utils.FormatNumberForHtmlInput(decimalValue, 4);
            var decimalValue2 = QLibs.Web.MVC.Utils.GetNumberDecimalFromHtml(numberHtmlInput);
            if (decimalValue != decimalValue2)
                throw new Exception("No es posible iniciar la web. El server debe estar debidamente en su parametrizacion de numero y formato regional.");

            if (QWebUtils.IgnoreServerValidations())
                return;

            if (QWebUtils.IsDevelopMode())
                return;
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var ex = HttpContext.Current.Server.GetLastError();

            if (ex != null)
                if (ex is QUserUIException)
                {
                    /// Ignorada, son errores de pantalla para interaccion con usuario.
                }
                else if (ex is HttpAntiForgeryException)
                {
                    /// Ignorada
                }
                else
                {
                    string complementaryInfo = "";
                    if (HttpContext.Current != null)
                    {
                        if (HttpContext.Current.Request != null && HttpContext.Current.Request.Url != null)
                            complementaryInfo += "URL: " + HttpContext.Current.Request.Url.ToString() + Environment.NewLine;
                        if (HttpContext.Current.User != null)
                            complementaryInfo += "TENANT ID: " + HttpContext.Current.User.GetTenantID() + Environment.NewLine;
                        if (HttpContext.Current.User != null && HttpContext.Current.User.Identity != null)
                            complementaryInfo += "USER: " + HttpContext.Current.User.Identity.GetUserId() + " / " + HttpContext.Current.User.Identity.GetUserName() + Environment.NewLine;
                        if (HttpContext.Current.Session != null)
                            complementaryInfo += "SESSION ID: " + HttpContext.Current.Session.SessionID + Environment.NewLine;
                    }
                    LogHelper.Instance.LogException(ex, complementaryInfo);
                }
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started 
            string sessionId = Session.SessionID;
        }
    }

    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            Microsoft.Practices.EnterpriseLibrary.Data.DatabaseFactory.SetDatabaseProviderFactory(new Microsoft.Practices.EnterpriseLibrary.Data.DatabaseProviderFactory());

            var siteName = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["SiteName"]) ? ConfigurationManager.AppSettings["SiteName"] : "RocaAPI.AdminEcomm";

            SiteMapHelper.SiteName = siteName;
            MenuHelper.FillMenu();
            SiteMapHelper.FillSiteMap();

            var options = new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                CookieName = siteName + ".aspnet.auth"
            };
            app.UseCookieAuthentication(options);
        }

    }
}

