﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using System.Collections;
using RocaAPI.AdminEcomm.Web.Models.ErrorModels;
using QLibs.Logging;
using QLibs.Web.MVC;
using RocaAPI.AdminEcomm.Web.Code;
using RocaAPI.AdminEcomm.Web.Code.Controllers;
using QLibs.Common.Utils;

namespace RocaAPI.AdminEcomm.Web.Controllers
{
    public class ErrorsController : BaseController
    {
        #region -- Errors --

        [IgnoreAudit]
        [AuthorizeIgnoreValidation]
        public virtual ActionResult error(string m = "", string mid = "")
        {
            if (!string.IsNullOrEmpty(mid))
            {
                Exception ex = HttpContext.Cache[mid] as Exception;
                if (ex != null)
                    LogHelper.Instance.LogException(ex);
                if (ex is QLibs.Common.Exceptions.QBusinessException)
                {
                    var qbex = (ex as QLibs.Common.Exceptions.QBusinessException);
                    return View("error", new ErrorModel() { Message = qbex.Message });
                }
                else if (ex is QLibs.DataAccess.Exceptions.QDataAccessException)
                {
                    var qex = (ex as QLibs.DataAccess.Exceptions.QDataAccessException);
                    return View("error", new ErrorModel() { Message = qex.Message });
                }
                else if (ex is QLibs.Common.Exceptions.QException)
                {
                    var qex = (ex as QLibs.Common.Exceptions.QException);
                    return View("error", new ErrorModel() { Message = qex.Message });
                }
            }
            else if (!string.IsNullOrEmpty(m))
            {
                var message = Session[m].ToString();
                m = message;
            }
            return View("error", new ErrorModel() { Message = m });
        }

        [IgnoreAudit]
        [AuthorizeIgnoreValidation]
        public virtual ActionResult ER403()
        {
            return View("error", new Error403Model());
        }

        [IgnoreAudit]
        [AuthorizeIgnoreValidation]
        public virtual ActionResult ER404()
        {
            return View("error", new Error404Model());
        }

        [IgnoreAudit]
        [AuthorizeIgnoreValidation]
        public virtual ActionResult ER405()
        {
            return View("error", new Error405Model());
        }

        [IgnoreAudit]
        [AuthorizeIgnoreValidation]
        public virtual ActionResult E500()
        {
            return View("error", new Error500Model());
        }

        #endregion

        [AllowAnonymous]
        [IgnoreAudit]
        [AuthorizeIgnoreValidation]
        public virtual ActionResult IncompatibleEnvironment()
        {
            return View("error", new ErrorIncompatibleModel());
        }

        [AllowAnonymous]
        [IgnoreAudit]
        [AuthorizeIgnoreValidation]
        public virtual ActionResult OutOfService()
        {
            try
            {
                var message = "";
                if (HttpContext.Session["OutOfServiceMessage"] != null)
                    message = HttpContext.Session["OutOfServiceMessage"].ToString();
                DateTime dateto = DateTime.UtcNow.AddHours(1);
                if (HttpContext.Session["OutOfServiceDateTo"] != null)
                    dateto = (DateTime)HttpContext.Session["OutOfServiceDateTo"];
                return View("OutOfService", new OutOfServiceModel(dateto) { Message = message != null ? message.ToString() : "" });
            }
            catch (Exception ex)
            {
                LogHelper.Instance.LogException(ex);
                throw;
            }
        }
    }
}
