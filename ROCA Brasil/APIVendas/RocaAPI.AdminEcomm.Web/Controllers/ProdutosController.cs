﻿using QLibs.Web;
using RocaAPI.AdminEcomm.Common.Entities;
using RocaAPI.AdminEcomm.Common.Queries;
using RocaAPI.AdminEcomm.Domain.Managers;
using RocaAPI.AdminEcomm.Web.Code;
using RocaAPI.AdminEcomm.Web.Code.Controllers;
using RocaAPI.AdminEcomm.Web.Models.ProdutoModels;
using System;
using System.IO;
using System.Web.Mvc;

namespace RocaAPI.AdminEcomm.Web.Controllers
{
    public partial class ProdutosController : BaseController
    {
        #region -- Produto --

        [Authorize]
        [IgnoreAuditAttribute]
        public ActionResult Produto()
        {
            return View();
        }

        #region -- ProdutoSearch --

        [HttpPost]
        [Authorize]
        [IgnoreAuditAttribute]
        [AuthorizeChildAction("Produto")]
        public ActionResult ProdutoSearch()
        {
            using (var manager = new ProdutoManager())
            {
                var gridData = new GridDataTable<ProdutoQueryFilters>(Request);
                gridData.Filters.FindValue = Request["Filters[FindValue]"];
                gridData.Filters.Brand = Request["Filters[Brand]"];
                gridData.Filters.Code = Request["Filters[Code]"];
                gridData.Filters.Ean = Request["Filters[Ean]"];
                gridData.Filters.Description = Request["Filters[Description]"];
                gridData.Filters.Line = Request["Filters[Line]"];
                gridData.Filters.Category = Request["Filters[Category]"];
                gridData.Filters.Type = Request["Filters[Type]"];
                gridData.Filters.SubType = Request["Filters[SubType]"];
                var queryResult = manager.Query(gridData.Filters);
                return Json(GridDataTableData.JsonResult<Produto>(queryResult));
            }
        }

        #endregion

        #region -- ProdutoDelete --

        [HttpPost]
        [Authorize]
        public JsonResult ProdutoDelete(string BaseEntityID)
        {
            try
            {
                using (var manager = new ProdutoManager())
                {
                    var entity = new Produto();
                    entity.SetBaseEntityID(BaseEntityID);
                    if (manager.Delete(entity))
                        return base.GetJsonResultOk(null, "SuccessOperation", DoActionEnum.ExecuteMethod, "submitFormEditFormOKDefault");
                }

                return base.GetJsonResultErrorMessage();
            }
            catch (Exception ex)
            {
                return base.GetJsonResultErrorMessage(ex);
            }
        }

        #endregion

        #region -- ProdutoCreate --

        [Authorize]
        [IgnoreAuditAttribute]
        public ActionResult ProdutoCreate()
        {
            var model = new ProdutoModel("Produto");
            model.Produtos = new ProdutoManager().Query(new ProdutoQueryFilters() { }).CurrentPageResult;
            return PartialView("~/Views/Produtos/Partials/KitForm.cshtml", model);
        }

        [HttpPost]
        [Authorize]
        public JsonResult ProdutoCreate(ProdutoModel model)
        {
            try
            {
                using (var manager = new ProdutoManager())
                {
                    if (manager.GetById(model.Entity) == null)
                    {
                        manager.Create(model.Entity);
                    }
                    else
                        return base.GetJsonResultErrorMessage("This code is in use.");
                }
                return base.GetJsonResultOk(model, "SuccessOperation", DoActionEnum.ExecuteMethod, "submitFormEditFormOKDefault");
            }
            catch (Exception ex)
            {
                return base.GetJsonResultErrorMessage(ex);
            }
        }

        #endregion

        #region -- ProdutoUpdate --

        [Authorize]
        [IgnoreAuditAttribute]
        public ActionResult ProdutoUpdate(string BaseEntityId)
        {
            ProdutoModel model = null;
            using (var manager = new ProdutoManager())
            {
                Produto entity = new Produto();
                entity.SetBaseEntityID(BaseEntityId);
                entity = manager.GetById(entity);
                model = new ProdutoModel("Produto", entity);
                return PartialView("~/Views/Produtos/Partials/ProdutoForm.cshtml", model);
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult ProdutoUpdate(ProdutoModel model)
        {
            try
            {
                using (var manager = new ProdutoManager())
                {
                    if (manager.Update(model.Entity) != null)
                        return base.GetJsonResultOk(model, "SuccessOperation", DoActionEnum.ExecuteMethod, "submitFormEditFormOKDefault");
                }
                return base.GetJsonResultErrorMessage();
            }
            catch (Exception ex)
            {
                return base.GetJsonResultErrorMessage(ex);
            }
        }

        #endregion

        #region -- Ajax Methods --

        [HttpPost]
        [Authorize]
        public JsonResult SendApi(string BaseEntityID)
        {
            try
            {
                return base.GetJsonResultOk(null, "SuccessOperation", DoActionEnum.ExecuteMethod, "submitOkSendApi");
            }
            catch (Exception ex)
            {
                return base.GetJsonResultErrorMessage(ex);
                throw;
            }
        }

        #endregion

        #endregion
    }
}
