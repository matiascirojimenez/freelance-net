﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using QLibs.Logging;
using QLibs.Web;
using QLibs.Web.MVC.Extensions;
using RocaAPI.AdminEcomm.Common.Entities.CORE.Seguridad;
using RocaAPI.AdminEcomm.Domain.Managers.CORE.Seguridad;
using RocaAPI.AdminEcomm.Web.Code;
using RocaAPI.AdminEcomm.Web.Code.Controllers;
using RocaAPI.AdminEcomm.Web.Models.AccountModels;
using RocaAPI.AdminEcomm.Web.Properties;
using System;
using System.IO;
using System.Security.Claims;
using System.Web.Helpers;
using System.Web.Mvc;

namespace RocaAPI.AdminEcomm.Web.Controllers
{
    public class AccountController : BaseController
    {
        private const string LoginPartial = "~/Views/Account/Partials/LoginPartial.cshtml";

        #region -- Constructor --

        private readonly IAuthenticationManager _auth;

        public AccountController(IAuthenticationManager auth)
        {
            this._auth = auth;
        }

        #endregion

        #region -- Login --

        [AllowAnonymous]
        [AuthorizeIgnoreValidation]
        public ActionResult Login()
        {
            var model = new LoginModel() { Invalid = false };
            model.ReadInvalidIntents(HttpContext);
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [RecaptchaFilter]
        public JsonResult ExecuteLogin(LoginModel model, bool CaptchaValid = false, string returnUrl = "")
        {
            try
            {
                if (!ModelState.IsValid)
                    return Json(new { actionResult = false, message = Textos.INVALID_FORM_VALUES, partial = RenderView(this, LoginPartial, model) });

                model.ReadInvalidIntents(HttpContext);
                if (model.InvalidIntents > LoginModel.MAXREINTENTOS)
                    if (CaptchaValid == false)
                        return Json(new { actionResult = false, message = Textos.INVALID_CAPTCHA, partial = RenderView(this, LoginPartial, model) });

                var loginresult = SecurityManager.ValidateLogin(model.UserName, model.Password, true);

                if (loginresult != PasswordVerificationResult.Failed && ModelState.IsValid)
                {
                    using (UsuarioManager manager = new UsuarioManager())
                    {
                        model.InvalidIntents = 0;
                        model.SetInvalidIntents(HttpContext);
                        var user = manager.GetBy(model.UserName);
                        DoLogin(model.Remember, user);
                        if (user.CambiarClave)
                        {
                            return Json(new { actionResult = true, message = "", redirectURL = Url.Action("ChangePassword", "Account", new { UsuarioId = user.UsuarioId }) });
                        }
                        return Json(new { actionResult = true, message = "", redirectURL = GetHomeURL(returnUrl) });
                    }
                }
                else
                {
                    model.InvalidIntents++;
                    model.SetInvalidIntents(HttpContext);
                    return Json(new { actionResult = false, message = Textos.INVALID_LOGIN, partial = RenderView(this, LoginPartial, model) });
                }
            }
            catch (Exception ex)
            {
                LogHelper.Instance.LogException(ex);
                model.Invalid = true;
                model.InvalidMessage = "ERROR - " + ex.Message;
                return Json(new { actionResult = false, message = Textos.INVALID_LOGIN, partial = RenderView(this, LoginPartial, model) });
            }
        }

        private void DoLogin(bool rememberMe, Usuario user)
        {
            var claims = HttpContext.User.GetClaims(user.FormattedName, user.NombreUsuario, user.UsuarioId, 1, 1, "", "");
            this._auth.SignIn(new AuthenticationProperties { IsPersistent = rememberMe }, new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie));
        }

        #endregion

        #region -- LogOff --

        [HttpPost]
        [IgnoreAuditAttribute]
        [AuthorizeIgnoreValidation]
        public ActionResult LogOff()
        {
            var cache = new InMemoryCache();
            var user = HttpContext.User.GetUser();
            if (user != null)
                cache.Clear("SecurityManager.HasPermission." + user.BaseEntityID);
            this._auth.SignOut();
            return RedirectToAction("Login", "Account");
        }

        #endregion
    }
}