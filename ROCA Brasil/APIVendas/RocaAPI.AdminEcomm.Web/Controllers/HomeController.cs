﻿using RocaAPI.AdminEcomm.Web.Code.Controllers;
using RocaAPI.AdminEcomm.Web.Models.HomeModels;
using System.Web.Mvc;

namespace RocaAPI.AdminEcomm.Web.Controllers
{
    public class HomeController : BaseController
    {
        [Authorize]
        [IgnoreAuditAttribute]
        [AuthorizeIgnoreValidation]
        public ActionResult Index(string culture, bool testCulture = false)
        {
            if (!string.IsNullOrEmpty(culture))
                base.ChangeCulture(culture);

            IndexModel model = new IndexModel();
            if (testCulture)
            {
                var doubleValue = 1333999.123;
                string testCultureString = "";
                testCultureString += "TOSTRING: " + doubleValue.ToString() + "<br>";
                testCultureString += "N3: " + doubleValue.ToString("N3") + "<br>";
                model.TestCulture = testCultureString;
                model.TestCultureDouble = doubleValue;
            }

            return View(model);
        }
    }
}
