﻿using QLibs.Web;
using RocaAPI.AdminEcomm.Common.Entities;
using RocaAPI.AdminEcomm.Common.Queries;
using RocaAPI.AdminEcomm.Domain.Managers;
using RocaAPI.AdminEcomm.Web.Code;
using RocaAPI.AdminEcomm.Web.Code.Controllers;
using RocaAPI.AdminEcomm.Web.Models.PedidoModels;
using System;
using System.IO;
using System.Web.Mvc;

namespace RocaAPI.AdminEcomm.Web.Controllers
{
    public partial class PedidosInternoController : BaseController
    {
        #region -- PedidoInterno --

        [Authorize]
        [IgnoreAuditAttribute]
        public ActionResult PedidoInterno()
        {
            return View();
        }

        #region -- PedidoInternoSearch --

        [HttpPost]
        [Authorize]
        [IgnoreAuditAttribute]
        [AuthorizeChildAction("PedidoInterno")]
        public ActionResult PedidoInternoSearch()
        {
            using (var manager = new PedidoInternoManager())
            {
                var gridData = new GridDataTable<PedidoInternoQueryFilters>(Request);
                gridData.Filters.FindValue = Request["Filters[FindValue]"];
                gridData.Filters.CdCliente = !string.IsNullOrEmpty(Request["Filters[CdCliente]"]) ? Int32.Parse(Request["Filters[CdCliente]"]) : (int?)null;
                gridData.Filters.NrPedidoCliente = Request["Filters[NrPedidoCliente]"];
                gridData.Filters.NrPedidoSap = Request["Filters[NrPedidoSap]"];
                gridData.Filters.DtEntradaFrom = !string.IsNullOrEmpty(Request["Filters[DtEntradaFrom]"]) ? DateTime.ParseExact(Request["Filters[DtEntradaFrom]"], "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture) : (DateTime?)null;
                gridData.Filters.DtEntradaTo = !string.IsNullOrEmpty(Request["Filters[DtEntradaTo]"]) ? DateTime.ParseExact(Request["Filters[DtEntradaTo]"], "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture) : (DateTime?)null;
                var queryResult = manager.Query(gridData.Filters);

                return Json(GridDataTableData.JsonResult<PedidoInterno>(queryResult));
            }
        }

        #endregion

        #region -- PedidoInternoUpdate --

        [Authorize]
        [IgnoreAuditAttribute]
        public ActionResult PedidoInternoUpdate(string BaseEntityId)
        {
            PedidoInternoModel model = null;
            using (var manager = new PedidoInternoManager())
            {
                PedidoInterno entity = new PedidoInterno();
                entity.SetBaseEntityID(BaseEntityId);
                entity = manager.GetById(entity);
                model = new PedidoInternoModel("PedidoInterno", entity);
                return PartialView("~/Views/PedidosInterno/Partials/PedidoInternoForm.cshtml", model);
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult PedidoInternoUpdate(PedidoInternoModel model)
        {
            try
            {
                using (var manager = new PedidoInternoManager())
                {
                    var cliente = new Cliente()
                    {
                        CdCliente = model.Entity.CdCliente,
                        DsEndereco = model.Entity.DsEndereco,
                        DsNumero = model.Entity.DsNumero,
                        DsComplemento = model.Entity.DsComplemento,
                        DsBairro = model.Entity.DsBairro,
                        NmCidade = model.Entity.NmCidade,
                        NrCep = model.Entity.NrCep,
                        NrSuframa = model.Entity.NrSuframa,
                    };

                    if (manager.UpdateCliente(cliente))
                        return base.GetJsonResultOk(model, "SuccessOperation", DoActionEnum.ExecuteMethod, "submitFormEditFormOKDefault");
                }
                return base.GetJsonResultErrorMessage();
            }
            catch (Exception ex)
            {
                return base.GetJsonResultErrorMessage(ex);
            }
        }

        #endregion

        #region -- Ajax Methods --

        [HttpPost]
        [Authorize]
        public JsonResult SendApi(string BaseEntityID)
        {
            try
            {
                return base.GetJsonResultOk(null, "SuccessOperation", DoActionEnum.ExecuteMethod, "submitOkSendApi");
            }
            catch (Exception ex)
            {
                return base.GetJsonResultErrorMessage(ex);
                throw;
            }
        }

        #endregion

        #endregion
    }
}
