using QLibs.DataAccess;
using QLibs.Domain;
using RocaAPI.AdminEcomm.Common.Entities;
using RocaAPI.AdminEcomm.Common.Queries;
using RocaAPI.AdminEcomm.DataAccess;

namespace RocaAPI.AdminEcomm.Domain.Managers
{
    public partial class PedidoManager : BaseManager<PedidoDataAccess, Pedido, PedidoQueryFilters> 
    {
        #region -- Constructors --

        public PedidoManager()
        {
        }

        public PedidoManager(QDatabase dac)
            : base(dac)
        {
        }

        #endregion

        #region -- Public Methods --

        #endregion

        #region -- Private Methods --

        #endregion
    }
}
