using RocaAPI.AdminEcomm.Common.Entities.CORE.Seguridad;
using System;

namespace RocaAPI.AdminEcomm.Domain.Managers.CORE.Seguridad
{
    public partial class UsuarioManager : IDisposable
    {
        public void Dispose()
        {
        }

        public Usuario GetBy(string userName)
        {
            if (userName == "admin")
                return new Usuario()
                {
                    Nombre = "admin",
                    NombreUsuario = "admin",
                    Clave = "@123Aa321",
                    Apellido = "",
                    Bloqueado = false,
                    CambiarClave = false,
                    Habilitado = true,
                };
            return null;
        }
    }
}
