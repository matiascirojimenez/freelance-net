using QLibs.Common.DTO.Queries;
using QLibs.DataAccess;
using QLibs.Domain;
using RocaAPI.AdminEcomm.Common.Entities;
using RocaAPI.AdminEcomm.DataAccess;

namespace RocaAPI.AdminEcomm.Domain.Managers
{
    public partial class PedidoAgrupadorZNManager : BaseManager<PedidoAgrupadorZNDataAccess, PedidoAgrupadorZN, BaseQueryFilters> 
    {
        #region -- Constructors --

        public PedidoAgrupadorZNManager()
        {
        }

        public PedidoAgrupadorZNManager(QDatabase dac)
            : base(dac)
        {
        }

        #endregion

        #region -- Public Methods --

        #endregion

        #region -- Private Methods --

        #endregion
    }
}
