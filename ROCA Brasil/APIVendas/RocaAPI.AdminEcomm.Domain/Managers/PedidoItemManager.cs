using QLibs.DataAccess;
using QLibs.Domain;
using RocaAPI.AdminEcomm.Common.Entities;
using RocaAPI.AdminEcomm.Common.Queries;
using RocaAPI.AdminEcomm.DataAccess;

namespace RocaAPI.AdminEcomm.Domain.Managers
{
    public partial class PedidoItemManager : BaseManager<PedidoItemDataAccess, PedidoItem, PedidoItemQueryFilters> 
    {
        #region -- Constructors --

        public PedidoItemManager()
        {
        }

        public PedidoItemManager(QDatabase dac)
            : base(dac)
        {
        }

        #endregion

        #region -- Public Methods --

        #endregion

        #region -- Private Methods --

        #endregion
    }
}
