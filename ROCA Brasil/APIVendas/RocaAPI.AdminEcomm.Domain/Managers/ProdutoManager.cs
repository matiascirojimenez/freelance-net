using QLibs.DataAccess;
using QLibs.Domain;
using RocaAPI.AdminEcomm.Common.Entities;
using RocaAPI.AdminEcomm.Common.Queries;
using RocaAPI.AdminEcomm.DataAccess;
using System.Collections.Generic;

namespace RocaAPI.AdminEcomm.Domain.Managers
{
    public partial class ProdutoManager : BaseManager<ProdutoDataAccess, Produto, ProdutoQueryFilters> 
    {
        #region -- Constructors --

        public ProdutoManager()
        {
        }

        public ProdutoManager(QDatabase dac)
            : base(dac)
        {
        }

        #endregion

        #region -- Public Methods --

        public List<string> GetBrand()
        {
            return this.DAC.GetBrand();
        }

        public List<string> GetLines()
        {
            return this.DAC.GetLines();
        }

        public List<string> GetCategories()
        {
            return this.DAC.GetCategories();
        }

        public List<string> GetTypes()
        {
            return this.DAC.GetTypes();
        }

        public List<string> GetSubTypes()
        {
            return this.DAC.GetSubTypes();
        }

        #endregion

        #region -- Private Methods --

        #endregion
    }
}
