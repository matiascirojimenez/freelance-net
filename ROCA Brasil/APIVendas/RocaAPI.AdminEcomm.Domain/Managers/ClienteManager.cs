using System;
using System.Text;
using QLibs.Domain;
using QLibs.Common.DTO.Queries;
using QLibs.DataAccess;
using RocaAPI.AdminEcomm.Common.Entities;
using RocaAPI.AdminEcomm.Common.Queries;
using RocaAPI.AdminEcomm.DataAccess;


namespace RocaAPI.AdminEcomm.Domain.Managers
{
    public partial class ClienteManager : BaseManager<ClienteDataAccess, Cliente, ClienteQueryFilters> 
    {
        #region -- Constructors --

        public ClienteManager()
        {
        }

        public ClienteManager(QDatabase dac)
            : base(dac)
        {
        }

        #endregion

        #region -- Public Methods --

        #endregion

        #region -- Private Methods --

        #endregion
    }
}
