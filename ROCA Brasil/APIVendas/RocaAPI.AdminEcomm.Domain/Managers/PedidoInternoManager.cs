using System;
using System.Text;
using QLibs.Domain;
using QLibs.Common.DTO.Queries;
using QLibs.DataAccess;
using RocaAPI.AdminEcomm.Common.Entities;
using RocaAPI.AdminEcomm.Common.Queries;
using RocaAPI.AdminEcomm.DataAccess;


namespace RocaAPI.AdminEcomm.Domain.Managers
{
    public partial class PedidoInternoManager : BaseManager<PedidoInternoDataAccess, PedidoInterno, PedidoInternoQueryFilters>
    {
        #region -- Constructors --

        public PedidoInternoManager()
        {
        }

        public PedidoInternoManager(QDatabase dac)
            : base(dac)
        {
        }

        #endregion

        #region -- Public Methods --

        public bool UpdateCliente(Cliente cliente)
        {
            bool result = true;

            try
            {
                this.DAC.BeginTransaction(this);

                using (var dac = new ClienteDataAccess(this.DAC))
                    result &= dac.UpdateCliente(cliente);

                this.DAC.CommitTransaction(this);
            }
            catch (Exception ex)
            {
                this.DAC.RollbackTransaction(this);
                throw ex;
            }

            return result;
        }

        #endregion

        #region -- Private Methods --

        #endregion
    }
}
