using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using QLibs.Common.Entities;
using QLibs.Common.Attributes;
using RocaAPI.AdminEcomm.Common.Entities;

namespace RocaAPI.AdminEcomm.Common.Entities
{
    /// <summary>
    /// match with table: PEDIDO_INTERNO_ITEM
    /// </summary>
    [Serializable]
    public partial class PedidoInternoItem : BaseEntity
    {
        #region -- Private fields --

        #endregion

        #region -- Constructors --

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PedidoInternoItem()
        {
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PedidoInternoItem(Guid nrcontrole, Int32 sqitem)
        {
            this.NrControle = nrcontrole;
            this.SqItem = sqitem;

        }

        #endregion

        #region -- Public Properties --

        /// <summary>
        /// column: NR_CONTROLE
        ///   type: [UNIQUEIDENTIFIER]
        /// </summary>
        [Required]
        [SortExpression(SortName = "PE.NR_CONTROLE")]
        public Guid NrControle { get; set; }

        /// <summary>
        /// column: SQ_ITEM
        ///   type: [INT]
        /// </summary>
        [Required]
        [SortExpression(SortName = "PE.SQ_ITEM")]
        public Int32 SqItem { get; set; }

        /// <summary>
        /// column: CD_PRODUTO
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(18)]
        [SortExpression(SortName = "PE.CD_PRODUTO")]
        public String CdProduto { get; set; }

        /// <summary>
        /// column: CD_EMBALAGEM
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(2)]
        [SortExpression(SortName = "PE.CD_EMBALAGEM")]
        public String CdEmbalagem { get; set; }

        /// <summary>
        /// column: TX_EMBALAGEM
        ///   type: [DECIMAL]
        /// </summary>
        [SortExpression(SortName = "PE.TX_EMBALAGEM")]
        public Decimal? TxEmbalagem { get; set; }

        /// <summary>
        /// column: VL_UNITARIO
        ///   type: [DECIMAL]
        /// </summary>
        [Required]
        [SortExpression(SortName = "PE.VL_UNITARIO")]
        public Decimal VlUnitario { get; set; }

        /// <summary>
        /// column: QT_PEDIDO
        ///   type: [INT]
        /// </summary>
        [Required]
        [SortExpression(SortName = "PE.QT_PEDIDO")]
        public Int32 QtPedido { get; set; }

        /// <summary>
        /// column: QT_ATENDIDO
        ///   type: [INT]
        /// </summary>
        [SortExpression(SortName = "PE.QT_ATENDIDO")]
        public Int32? QtAtendido { get; set; }

        /// <summary>
        /// column: TP_SITUACAO
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(2)]
        [SortExpression(SortName = "PE.TP_SITUACAO")]
        public String TpSituacao { get; set; }

        /// <summary>
        /// column: CD_GRUPO_EXPEDICAO
        ///   type: [SMALLINT]
        /// </summary>
        [SortExpression(SortName = "PE.CD_GRUPO_EXPEDICAO")]
        public Int16? CdGrupoExpedicao { get; set; }

        /// <summary>
        /// column: DT_REMESSA
        ///   type: [DATETIME]
        /// </summary>
        [SortExpression(SortName = "PE.DT_REMESSA")]
        public DateTime? DtRemessa { get; set; }

        /// <summary>
        /// column: CD_CENTRO
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(4)]
        [SortExpression(SortName = "PE.CD_CENTRO")]
        public String CdCentro { get; set; }

        /// <summary>
        /// column: CD_TRANSPORTADORA
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(10)]
        [SortExpression(SortName = "PE.CD_TRANSPORTADORA")]
        public String CdTransportadora { get; set; }

        /// <summary>
        /// column: CD_TRANSPORTADORA_RED
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(10)]
        [SortExpression(SortName = "PE.CD_TRANSPORTADORA_RED")]
        public String CdTransportadoraRed { get; set; }

        /// <summary>
        /// column: VL_PESO_BRUTO
        ///   type: [DECIMAL]
        /// </summary>
        [SortExpression(SortName = "PE.VL_PESO_BRUTO")]
        public Decimal? VlPesoBruto { get; set; }

        /// <summary>
        /// column: QT_SALDO
        ///   type: [INT]
        /// </summary>
        [SortExpression(SortName = "PE.QT_SALDO")]
        public Int32? QtSaldo { get; set; }

        /// <summary>
        /// column: QT_FORNECIDA
        ///   type: [INT]
        /// </summary>
        [SortExpression(SortName = "PE.QT_FORNECIDA")]
        public Int32? QtFornecida { get; set; }

        /// <summary>
        /// column: CD_ROTA
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(10)]
        [SortExpression(SortName = "PE.CD_ROTA")]
        public String CdRota { get; set; }

        /// <summary>
        /// column: VL_IMPOSTO
        ///   type: [DECIMAL]
        /// </summary>
        [SortExpression(SortName = "PE.VL_IMPOSTO")]
        public Decimal? VlImposto { get; set; }

        /// <summary>
        /// column: VL_TOTAL
        ///   type: [DECIMAL]
        /// </summary>
        [SortExpression(SortName = "PE.VL_TOTAL")]
        public Decimal? VlTotal { get; set; }

        /// <summary>
        /// column: CD_IMPOSTO
        ///   type: [CHAR]
        /// </summary>
        [StringLength(2)]
        [SortExpression(SortName = "PE.CD_IMPOSTO")]
        public String CdImposto { get; set; }

        /// <summary>
        /// column: DT_PREVISAO
        ///   type: [DATETIME]
        /// </summary>
        [SortExpression(SortName = "PE.DT_PREVISAO")]
        public DateTime? DtPrevisao { get; set; }

        /// <summary>
        /// column: VL_FRETE
        ///   type: [DECIMAL]
        /// </summary>
        [SortExpression(SortName = "PE.VL_FRETE")]
        public Decimal? VlFrete { get; set; }

        /// <summary>
        /// column: CD_RASTREIO
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PE.CD_RASTREIO")]
        public String CdRastreio { get; set; }

        /// <summary>
        /// column: Company_ID
        ///   type: [INT]
        /// </summary>
        [Required]
        [SortExpression(SortName = "PE.Company_ID")]
        public Int32 CompanyId { get; set; }

        /// <summary>
        /// column: COD_RESERVA
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PE.COD_RESERVA")]
        public String CodReserva { get; set; }

        /// <summary>
        /// column: ANO
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(10)]
        [SortExpression(SortName = "PE.ANO")]
        public String Ano { get; set; }

        #endregion

        #region -- IDs Mappings --

        public override string BaseEntityID
        {
            get
            {
                return NrControle.ToString() + '|' + SqItem.ToString();
            }
        }

        public override void SetBaseEntityID(string id)
        {
            this.NrControle = Guid.Parse(id.Split('|')[0]);
            this.SqItem = Int32.Parse(id.Split('|')[1]);

        }

        public override bool HasID
        {
            get
            {
                return this.NrControle != null && this.SqItem > 0;
            }
        }

        #endregion

        #region -- Join Properties --

        #endregion
    }
}
