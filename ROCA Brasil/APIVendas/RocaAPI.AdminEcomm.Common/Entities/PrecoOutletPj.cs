using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using QLibs.Common.Entities;
using QLibs.Common.Attributes;
using RocaAPI.AdminEcomm.Common.Entities;

namespace RocaAPI.AdminEcomm.Common.Entities
{
    /// <summary>
    /// match with table: PRECO_OUTLET_PJ
    /// </summary>
    [Serializable]
    public partial class PrecoOutletPj : BaseEntity
    {
        #region -- Private fields --

        #endregion

        #region -- Constructors --

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PrecoOutletPj()
        {
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PrecoOutletPj(Decimal codtab, String werks, String regio, String matnr)
        {
            this.Codtab = codtab;
            this.Werks = werks;
            this.Regio = regio;
            this.Matnr = matnr;

        }

        #endregion

        #region -- Public Properties --

        /// <summary>
        /// column: CODTAB
        ///   type: [NUMERIC]
        /// </summary>
        [SortExpression(SortName = "PR.CODTAB")]
        public Decimal Codtab { get; set; }

        /// <summary>
        /// column: WERKS
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(20)]
        [SortExpression(SortName = "PR.WERKS")]
        public String Werks { get; set; }

        /// <summary>
        /// column: REGIO
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(20)]
        [SortExpression(SortName = "PR.REGIO")]
        public String Regio { get; set; }

        /// <summary>
        /// column: MATNR
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(20)]
        [SortExpression(SortName = "PR.MATNR")]
        public String Matnr { get; set; }

        /// <summary>
        /// column: PRECO
        ///   type: [NUMERIC]
        /// </summary>
        [SortExpression(SortName = "PR.PRECO")]
        public Double? Preco { get; set; }

        /// <summary>
        /// column: PRBAS
        ///   type: [NUMERIC]
        /// </summary>
        [SortExpression(SortName = "PR.PRBAS")]
        public Double? Prbas { get; set; }

        /// <summary>
        /// column: VLICMS
        ///   type: [NUMERIC]
        /// </summary>
        [SortExpression(SortName = "PR.VLICMS")]
        public Double? Vlicms { get; set; }

        /// <summary>
        /// column: VLIPI
        ///   type: [NUMERIC]
        /// </summary>
        [SortExpression(SortName = "PR.VLIPI")]
        public Double? Vlipi { get; set; }

        /// <summary>
        /// column: VLICST
        ///   type: [NUMERIC]
        /// </summary>
        [SortExpression(SortName = "PR.VLICST")]
        public Double? Vlicst { get; set; }

        /// <summary>
        /// column: VLFECPST
        ///   type: [NUMERIC]
        /// </summary>
        [SortExpression(SortName = "PR.VLFECPST")]
        public Double? Vlfecpst { get; set; }

        /// <summary>
        /// column: VLPIS
        ///   type: [NUMERIC]
        /// </summary>
        [SortExpression(SortName = "PR.VLPIS")]
        public Double? Vlpis { get; set; }

        /// <summary>
        /// column: VLCOF
        ///   type: [NUMERIC]
        /// </summary>
        [SortExpression(SortName = "PR.VLCOF")]
        public Double? Vlcof { get; set; }

        /// <summary>
        /// column: VLICMSPART
        ///   type: [NUMERIC]
        /// </summary>
        [SortExpression(SortName = "PR.VLICMSPART")]
        public Double? Vlicmspart { get; set; }

        /// <summary>
        /// column: VLFECPPART
        ///   type: [NUMERIC]
        /// </summary>
        [SortExpression(SortName = "PR.VLFECPPART")]
        public Double? Vlfecppart { get; set; }

        /// <summary>
        /// column: auditdate
        ///   type: [DATETIME]
        /// </summary>
        [SortExpression(SortName = "PR.auditdate")]
        public DateTime? Auditdate { get; set; }

        #endregion

        #region -- IDs Mappings --

        public override string BaseEntityID
        {
            get
            {
                return Codtab.ToString() + '|' + Werks.ToString() + '|' + Regio.ToString() + '|' + Matnr.ToString();
            }
        }

        public override void SetBaseEntityID(string id)
        {
            this.Codtab = decimal.Parse(id.Split('|')[0]);
            this.Werks = id.Split('|')[1];
            this.Regio = id.Split('|')[2];
            this.Matnr = id.Split('|')[3];

        }

        public override bool HasID
        {
            get
            {
                return this.Codtab != null && !string.IsNullOrEmpty(this.Werks) && !string.IsNullOrEmpty(this.Regio) && !string.IsNullOrEmpty(this.Matnr);
            }
        }

        #endregion

        #region -- Join Properties --

        #endregion
    }
}
