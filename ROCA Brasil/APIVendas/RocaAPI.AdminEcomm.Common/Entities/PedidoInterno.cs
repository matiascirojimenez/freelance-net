using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using QLibs.Common.Entities;
using QLibs.Common.Attributes;
using RocaAPI.AdminEcomm.Common.Entities;

namespace RocaAPI.AdminEcomm.Common.Entities
{
    /// <summary>
    /// match with table: PEDIDO_INTERNO
    /// </summary>
    [Serializable]
    public partial class PedidoInterno : BaseEntity
    {
        #region -- Private fields --

        #endregion

        #region -- Constructors --

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PedidoInterno()
        {
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PedidoInterno(Guid nrcontrole)
        {
            this.NrControle = nrcontrole;

        }

        #endregion

        #region -- Public Properties --

        /// <summary>
        /// column: NR_CONTROLE
        ///   type: [UNIQUEIDENTIFIER]
        /// </summary>
        [Required]
        [SortExpression(SortName = "PE.NR_CONTROLE")]
        public Guid NrControle { get; set; }

        /// <summary>
        /// column: CD_MARCA
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(4)]
        [SortExpression(SortName = "PE.CD_MARCA")]
        public String CdMarca { get; set; }

        /// <summary>
        /// column: CD_VENDEDOR
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(3)]
        [SortExpression(SortName = "PE.CD_VENDEDOR")]
        public String CdVendedor { get; set; }

        /// <summary>
        /// column: CD_CLIENTE_EMISSOR
        ///   type: [BIGINT]
        /// </summary>
        [SortExpression(SortName = "PE.CD_CLIENTE_EMISSOR")]
        public Int64? CdClienteEmissor { get; set; }

        /// <summary>
        /// column: CD_CLIENTE_RECEBEDOR
        ///   type: [BIGINT]
        /// </summary>
        [SortExpression(SortName = "PE.CD_CLIENTE_RECEBEDOR")]
        public Int64? CdClienteRecebedor { get; set; }

        /// <summary>
        /// column: NR_PEDIDO_CLIENTE
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(35)]
        [SortExpression(SortName = "PE.NR_PEDIDO_CLIENTE")]
        public String NrPedidoCliente { get; set; }

        /// <summary>
        /// column: NR_PEDIDO_SAP
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PE.NR_PEDIDO_SAP")]
        public String NrPedidoSap { get; set; }

        /// <summary>
        /// column: CD_TIPO_PEDIDO
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(4)]
        [SortExpression(SortName = "PE.CD_TIPO_PEDIDO")]
        public String CdTipoPedido { get; set; }

        /// <summary>
        /// column: DT_ENTRADA
        ///   type: [DATETIME]
        /// </summary>
        [Required]
        [SortExpression(SortName = "PE.DT_ENTRADA")]
        public DateTime DtEntrada { get; set; }

        /// <summary>
        /// column: DT_PROGRAMACAO
        ///   type: [DATETIME]
        /// </summary>
        [Required]
        [SortExpression(SortName = "PE.DT_PROGRAMACAO")]
        public DateTime DtProgramacao { get; set; }

        /// <summary>
        /// column: CD_FRETE
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(3)]
        [SortExpression(SortName = "PE.CD_FRETE")]
        public String CdFrete { get; set; }

        /// <summary>
        /// column: CD_PRAZO
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(4)]
        [SortExpression(SortName = "PE.CD_PRAZO")]
        public String CdPrazo { get; set; }

        /// <summary>
        /// column: CD_TABELA_PRECO
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(3)]
        [SortExpression(SortName = "PE.CD_TABELA_PRECO")]
        public String CdTabelaPreco { get; set; }

        /// <summary>
        /// column: AA_TABELA_PRECO
        ///   type: [SMALLINT]
        /// </summary>
        [SortExpression(SortName = "PE.AA_TABELA_PRECO")]
        public Int16? AaTabelaPreco { get; set; }

        /// <summary>
        /// column: CD_BLOQUEIO
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(2)]
        [SortExpression(SortName = "PE.CD_BLOQUEIO")]
        public String CdBloqueio { get; set; }

        /// <summary>
        /// column: DS_OBSERVACAO1
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(1333)]
        [SortExpression(SortName = "PE.DS_OBSERVACAO1")]
        public String DsObservacao1 { get; set; }

        /// <summary>
        /// column: DS_OBSERVACAO2
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(80)]
        [SortExpression(SortName = "PE.DS_OBSERVACAO2")]
        public String DsObservacao2 { get; set; }

        /// <summary>
        /// column: STATUS
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(15)]
        [SortExpression(SortName = "PE.STATUS")]
        public String Status { get; set; }

        /// <summary>
        /// column: VL_PEDIDO
        ///   type: [DECIMAL]
        /// </summary>
        [Required]
        [SortExpression(SortName = "PE.VL_PEDIDO")]
        public Decimal VlPedido { get; set; }

        /// <summary>
        /// column: VL_FRETE
        ///   type: [DECIMAL]
        /// </summary>
        [Required]
        [SortExpression(SortName = "PE.VL_FRETE")]
        public Decimal VlFrete { get; set; }

        /// <summary>
        /// column: VL_DESCONTO
        ///   type: [DECIMAL]
        /// </summary>
        [SortExpression(SortName = "PE.VL_DESCONTO")]
        public Decimal? VlDesconto { get; set; }

        /// <summary>
        /// column: VL_DESPESAS_ACESSORIAS
        ///   type: [DECIMAL]
        /// </summary>
        [Required]
        [SortExpression(SortName = "PE.VL_DESPESAS_ACESSORIAS")]
        public Decimal VlDespesasAcessorias { get; set; }

        /// <summary>
        /// column: VL_MERCADORIA
        ///   type: [DECIMAL]
        /// </summary>
        [SortExpression(SortName = "PE.VL_MERCADORIA")]
        public Decimal? VlMercadoria { get; set; }

        /// <summary>
        /// column: NR_BOLETO
        ///   type: [NCHAR]
        /// </summary>
        [StringLength(128)]
        [SortExpression(SortName = "PE.NR_BOLETO")]
        public String NrBoleto { get; set; }

        /// <summary>
        /// column: LG_CANCELADO
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [SortExpression(SortName = "PE.LG_CANCELADO")]
        public string LgCancelado { get; set; }

        /// <summary>
        /// column: LG_PROCESSADO
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [SortExpression(SortName = "PE.LG_PROCESSADO")]
        public string LgProcessado { get; set; }

        /// <summary>
        /// column: QT_PECAS
        ///   type: [INT]
        /// </summary>
        [SortExpression(SortName = "PE.QT_PECAS")]
        public Int32? QtPecas { get; set; }

        /// <summary>
        /// column: DT_CARTEIRA_BOA
        ///   type: [DATETIME]
        /// </summary>
        [SortExpression(SortName = "PE.DT_CARTEIRA_BOA")]
        public DateTime? DtCarteiraBoa { get; set; }

        /// <summary>
        /// column: DT_COMPROMETIMENTO
        ///   type: [DATETIME]
        /// </summary>
        [SortExpression(SortName = "PE.DT_COMPROMETIMENTO")]
        public DateTime? DtComprometimento { get; set; }

        /// <summary>
        /// column: LG_ATRASO
        ///   type: [VARCHAR]
        /// </summary>
        [SortExpression(SortName = "PE.LG_ATRASO")]
        public Char? LgAtraso { get; set; }

        /// <summary>
        /// column: DD_ATRASO
        ///   type: [SMALLINT]
        /// </summary>
        [SortExpression(SortName = "PE.DD_ATRASO")]
        public Int16? DdAtraso { get; set; }

        /// <summary>
        /// column: DT_PREVISAO
        ///   type: [DATETIME]
        /// </summary>
        [SortExpression(SortName = "PE.DT_PREVISAO")]
        public DateTime? DtPrevisao { get; set; }

        /// <summary>
        /// column: VL_IPI
        ///   type: [DECIMAL]
        /// </summary>
        [SortExpression(SortName = "PE.VL_IPI")]
        public Decimal? VlIpi { get; set; }

        /// <summary>
        /// column: CD_ORGANIZACAO
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(4)]
        [SortExpression(SortName = "PE.CD_ORGANIZACAO")]
        public String CdOrganizacao { get; set; }

        /// <summary>
        /// column: DT_INICIAL
        ///   type: [DATETIME]
        /// </summary>
        [SortExpression(SortName = "PE.DT_INICIAL")]
        public DateTime? DtInicial { get; set; }

        /// <summary>
        /// column: CD_RASTREIO
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PE.CD_RASTREIO")]
        public String CdRastreio { get; set; }

        /// <summary>
        /// column: Company_ID
        ///   type: [INT]
        /// </summary>
        [Required]
        [SortExpression(SortName = "PE.Company_ID")]
        public Int32 CompanyId { get; set; }

        /// <summary>
        /// column: CD_CLIENTE
        ///   type: [INT]
        /// </summary>
        [Required]
        [SortExpression(SortName = "PE.CD_CLIENTE")]
        public Int32 CdCliente { get; set; }

        /// <summary>
        /// column: auditdate
        ///   type: [DATETIME]
        /// </summary>
        [SortExpression(SortName = "PE.auditdate")]
        public DateTime? Auditdate { get; set; }

        /// <summary>
        /// column: NR_PEDIDO_SAP_SERVICO
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PE.NR_PEDIDO_SAP_SERVICO")]
        public String NrPedidoSapServico { get; set; }

        #endregion

        #region -- IDs Mappings --

        public override string BaseEntityID
        {
            get
            {
                return NrControle.ToString();
            }
        }

        public override void SetBaseEntityID(string id)
        {
            this.NrControle = Guid.Parse(id.Split('|')[0]);

        }

        public override bool HasID
        {
            get
            {
                return this.NrControle != null;
            }
        }

        #endregion

        #region -- Join Properties --

        [SortExpression(SortName = "CLI.NR_CNPJ")]
        public String ClienteNrCnpj { get; set; }

        [SortExpression(SortName = "CLI.NR_CPF")]
        public String ClienteNrCpf { get; set; }

        [SortExpression(SortName = "CLI.NM_ESTABELECIMENTO")]
        public String ClienteNmEstabelecimento { get; set; }

        /// <summary>
        /// CLIENTE.NR_CNPJ
        ///   funcional: EmissorNrCnpj
        /// </summary>
        [DisplayName("EmissorNrCnpj")]
        [SortExpression(SortName = "EMI.NR_CNPJ")]
        public String EmissorNrCnpj { get; set; }

        /// <summary>
        /// CLIENTE.NR_CPF
        ///   funcional: EmissorNrCpf
        /// </summary>
        [DisplayName("EmissorNrCpf")]
        [SortExpression(SortName = "EMI.NR_CPF")]
        public String EmissorNrCpf { get; set; }

        /// <summary>
        /// CLIENTE.NM_ESTABELECIMENTO
        ///   funcional: EmissorNmEstabelecimento
        /// </summary>
        [DisplayName("EmissorNmEstabelecimento")]
        [SortExpression(SortName = "EMI.NM_ESTABELECIMENTO")]
        public String EmissorNmEstabelecimento { get; set; }

        /// <summary>
        /// CLIENTE.NR_CNPJ
        ///   funcional: RecebedorNrCnpj
        /// </summary>
        [DisplayName("RecebedorNrCnpj")]
        [SortExpression(SortName = "RECE.NR_CNPJ")]
        public String RecebedorNrCnpj { get; set; }

        /// <summary>
        /// CLIENTE.NR_CPF
        ///   funcional: RecebedorNrCpf
        /// </summary>
        [DisplayName("RecebedorNrCpf")]
        [SortExpression(SortName = "RECE.NR_CPF")]
        public String RecebedorNrCpf { get; set; }

        /// <summary>
        /// CLIENTE.NM_ESTABELECIMENTO
        ///   funcional: RecebedorNmEstabelecimento
        /// </summary>
        [DisplayName("RecebedorNmEstabelecimento")]
        [SortExpression(SortName = "RECE.NM_ESTABELECIMENTO")]
        public String RecebedorNmEstabelecimento { get; set; }

        #region -- Cliente --
        
        [SortExpression(SortName = "CLI.DS_ENDERECO")]
        public String DsEndereco { get; set; }

        [SortExpression(SortName = "CLI.DS_NUMERO")]
        public String DsNumero { get; set; }

        [SortExpression(SortName = "CLI.DS_COMPLEMENTO")]
        public String DsComplemento { get; set; }

        [SortExpression(SortName = "CLI.DS_BAIRRO")]
        public String DsBairro { get; set; }

        [SortExpression(SortName = "CLI.NM_CIDADE")]
        public String NmCidade { get; set; }

        [SortExpression(SortName = "CLI.NR_CEP")]
        public String NrCep { get; set; }

        [SortExpression(SortName = "CLI.NR_SUFRAMA")]
        public String NrSuframa { get; set; }

        #endregion

        [SortExpression(SortName = "PE.DT_ENTRADA")]
        public String DtEntradaString { get { return DtEntrada.ToString("dd/MM/yyyy"); } }

        [SortExpression(SortName = "PE.DT_PROGRAMACAO")]
        public String DtProgramacaoString { get { return DtProgramacao.ToString("dd/MM/yyyy"); } }

        public List<PedidoInternoItem> Items { get; set; }

        #endregion
    }
}
