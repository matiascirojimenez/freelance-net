using QLibs.Common.Attributes;
using QLibs.Common.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace RocaAPI.AdminEcomm.Common.Entities.CORE.Seguridad
{
    /// <summary>
    /// match with table: segUsuario
    /// </summary>
    [Serializable]
    [System.ComponentModel.DisplayName("Usuario")]
    public partial class Usuario : BaseEntity, IJSONComboOptionEntity, IUniqueCodeEntity
    {
        #region -- Private fields --

        #endregion

        #region -- Constructors --

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Usuario()
        {
            this.Habilitado = true;
        }

        public Usuario(int usuarioId)
        {
            this.UsuarioId = usuarioId;
        }

        #endregion

        #region -- Public Properties --

        /// <summary>
        /// column: UsuarioId
        ///   type: [INT]
        /// </summary>
        [DisplayName("Usuario Id")]
        [Required]
        [SortExpression(SortName = "SE.UsuarioId")]
        public Int32 UsuarioId { get; set; }

        /// <summary>
        /// column: NombreUsuario
        ///   type: [VARCHAR]
        /// </summary>
        [DisplayName("Nombre de Usuario")]
        [Required]
        [StringLength(50)]
        [SortExpression(SortName = "SE.NombreUsuario")]
        public String NombreUsuario { get; set; }

        /// <summary>
        /// column: Nombre
        ///   type: [VARCHAR]
        /// </summary>
        [DisplayName("Nombre")]
        [StringLength(100)]
        [SortExpression(SortName = "SE.Nombre")]
        public String Nombre { get; set; }

        /// <summary>
        /// column: Apellido
        ///   type: [VARCHAR]
        /// </summary>
        [DisplayName("Apellido")]
        [Required]
        [StringLength(50)]
        [SortExpression(SortName = "SE.Apellido")]
        public String Apellido { get; set; }

        /// <summary>
        /// column: Email
        ///   type: [VARCHAR]
        /// </summary>
        [DisplayName("Email")]
        [Required]
        [StringLength(50)]
        [EmailAddress]
        [SortExpression(SortName = "SE.Email")]
        public String Email { get; set; }

        /// <summary>
        /// column: Clave
        ///   type: [VARCHAR]
        /// </summary>
        [DisplayName("Clave")]
        [StringLength(500)]
        [SortExpression(SortName = "SE.Clave")]
        public String Clave { get; set; }

        /// <summary>
        /// column: Bloqueado
        ///   type: [BIT]
        /// </summary>
        [DisplayName("Bloqueado")]
        [Required]
        [SortExpression(SortName = "SE.Bloqueado")]
        public Boolean Bloqueado { get; set; }

        /// <summary>
        /// column: FechaBaja
        ///   type: [DATETIME]
        /// </summary>
        [DisplayName("Fecha de baja")]
        [SortExpression(SortName = "SE.FechaBaja")]
        public DateTime? FechaBaja { get; set; }

        /// <summary>
        /// column: Habilitado
        ///   type: [BIT]
        /// </summary>
        [DisplayName("Habilitado")]
        [Required]
        [SortExpression(SortName = "SE.Habilitado")]
        public Boolean Habilitado { get; set; }

        /// <summary>
        /// column: CambiarClave
        ///   type: [BIT]
        /// </summary>
        [DisplayName("Cambiar clave")]
        [Required]
        [SortExpression(SortName = "SE.CambiarClave")]
        public bool CambiarClave { get; set; }

        /// <summary>
        /// column: Guid 
        ///   type: [GUID]
        /// </summary>
        [DisplayName("Guid")]
        [Required]
        [SortExpression(SortName = "SE.Guid")]
        public Guid Guid { get; set; }

        /// <summary>
        /// column: CodigoVendedor 
        ///   type: [VARCHAR2]
        /// </summary>
        [DisplayName("C�digo Vendedor")]
        [SortExpression(SortName = "SE.CodigoVendedor")]
        public String CodigoVendedor { get; set; }

        /// <summary>
        /// column: UsuarioAprobadorId 
        ///   type: [VARCHAR2]
        /// </summary>
        [DisplayName("Aprobador")]
        [SortExpression(SortName = "SE.UsuarioAprobadorId")]
        public int? UsuarioAprobadorId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DisplayName("Vendedores a cargo")]
        [SortExpression(SortName = "SE.VendedoresACargo")]
        public string VendedoresACargo { get; set; }

        [DisplayName("Notificar")]
        [SortExpression(SortName = "SE.NotificarPropuestas")]
        public bool NotificarPropuestas { get; set; }

        [DisplayName("Aprobar")]
        [SortExpression(SortName = "SE.ApruebaPropuestaCliente")]
        public bool ApruebaPropuestaCliente { get; set; }
        
        #endregion

        #region -- IDs Mappings --

        public override void SetBaseIdentity(object id)
        {
            this.UsuarioId = int.Parse(id.ToString());
        }

        [IgnoredForApi]
        public override string BaseEntityID
        {
            get
            {
                return UsuarioId.ToString();
            }
        }

        public override void SetBaseEntityID(string id)
        {

            this.UsuarioId = Int32.Parse(id.Split('|')[0]);
        }

        [IgnoredForApi]
        public override bool HasID
        {
            get
            {
                return this.UsuarioId > 0;
            }
        }

        public string GetTenatColumnName()
        {
            return "SE.TenantID";
        }

        #endregion

        #region -- Join Properties --

        public string FormattedName { get { return Nombre + " " + Apellido; } }

        #endregion

        #region --- IJSONComboOptionEntity  ---

        public string GetDescription()
        {
            return this.FormattedName;
        }

        public string GetValue()
        {
            return this.UsuarioId.ToString();
        }

        public string GetCode()
        {
            return this.CodigoVendedor;
        }

        #endregion

        #region -- IUniqueCodeEntity --

        public string UniqueCode => this.NombreUsuario;

        public string[] ObtenerVendedoresACargo()
        {
            if (!string.IsNullOrEmpty(this.VendedoresACargo))
                return VendedoresACargo.Contains(";") ? this.VendedoresACargo.Split(';') : this.VendedoresACargo.Split(',');
            return new string[0];
        }

        #endregion
    }
}
