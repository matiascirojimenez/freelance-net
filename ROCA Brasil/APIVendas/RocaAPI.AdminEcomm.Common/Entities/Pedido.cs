using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using QLibs.Common.Entities;
using QLibs.Common.Attributes;
using RocaAPI.AdminEcomm.Common.Entities;

namespace RocaAPI.AdminEcomm.Common.Entities
{
    /// <summary>
    /// match with table: PEDIDO
    /// </summary>
    [Serializable]
    public partial class Pedido : BaseEntity
    {
        #region -- Private fields --

        #endregion

        #region -- Constructors --

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Pedido()
        {
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Pedido(Int64 nrpedido)
        {
            this.NrPedido = nrpedido;

        }

        #endregion

        #region -- Public Properties --

        /// <summary>
        /// column: NR_PEDIDO
        ///   type: [BIGINT]
        /// </summary>
        [Required]
        [SortExpression(SortName = "PE.NR_PEDIDO")]
        public Int64 NrPedido { get; set; }

        /// <summary>
        /// column: NR_CONTROLE
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(50)]
        [SortExpression(SortName = "PE.NR_CONTROLE")]
        public String NrControle { get; set; }

        /// <summary>
        /// column: CD_MARCA
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(4)]
        [SortExpression(SortName = "PE.CD_MARCA")]
        public String CdMarca { get; set; }

        /// <summary>
        /// column: CD_VENDEDOR
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(3)]
        [SortExpression(SortName = "PE.CD_VENDEDOR")]
        public String CdVendedor { get; set; }

        /// <summary>
        /// column: CD_CLIENTE_EMISSOR
        ///   type: [BIGINT]
        /// </summary>
        [Required]
        [SortExpression(SortName = "PE.CD_CLIENTE_EMISSOR")]
        public Int64 CdClienteEmissor { get; set; }

        /// <summary>
        /// column: CD_CLIENTE_RECEBEDOR
        ///   type: [BIGINT]
        /// </summary>
        [SortExpression(SortName = "PE.CD_CLIENTE_RECEBEDOR")]
        public Int64? CdClienteRecebedor { get; set; }

        /// <summary>
        /// column: NR_PEDIDO_CLIENTE
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(35)]
        [SortExpression(SortName = "PE.NR_PEDIDO_CLIENTE")]
        public String NrPedidoCliente { get; set; }

        /// <summary>
        /// column: CD_TIPO_PEDIDO
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(4)]
        [SortExpression(SortName = "PE.CD_TIPO_PEDIDO")]
        public String CdTipoPedido { get; set; }

        /// <summary>
        /// column: DT_ENTRADA
        ///   type: [DATETIME]
        /// </summary>
        [SortExpression(SortName = "PE.DT_ENTRADA")]
        public DateTime? DtEntrada { get; set; }

        /// <summary>
        /// column: DT_PROGRAMACAO
        ///   type: [DATETIME]
        /// </summary>
        [SortExpression(SortName = "PE.DT_PROGRAMACAO")]
        public DateTime? DtProgramacao { get; set; }

        /// <summary>
        /// column: CD_FRETE
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(3)]
        [SortExpression(SortName = "PE.CD_FRETE")]
        public String CdFrete { get; set; }

        /// <summary>
        /// column: CD_PRAZO
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(4)]
        [SortExpression(SortName = "PE.CD_PRAZO")]
        public String CdPrazo { get; set; }

        /// <summary>
        /// column: CD_TABELA_PRECO
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(3)]
        [SortExpression(SortName = "PE.CD_TABELA_PRECO")]
        public String CdTabelaPreco { get; set; }

        /// <summary>
        /// column: AA_TABELA_PRECO
        ///   type: [SMALLINT]
        /// </summary>
        [SortExpression(SortName = "PE.AA_TABELA_PRECO")]
        public Int16? AaTabelaPreco { get; set; }

        /// <summary>
        /// column: CD_BLOQUEIO
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(2)]
        [SortExpression(SortName = "PE.CD_BLOQUEIO")]
        public String CdBloqueio { get; set; }

        /// <summary>
        /// column: DS_OBSERVACAO1
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(1333)]
        [SortExpression(SortName = "PE.DS_OBSERVACAO1")]
        public String DsObservacao1 { get; set; }

        /// <summary>
        /// column: DS_OBSERVACAO2
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(80)]
        [SortExpression(SortName = "PE.DS_OBSERVACAO2")]
        public String DsObservacao2 { get; set; }

        /// <summary>
        /// column: STATUS
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(80)]
        [SortExpression(SortName = "PE.STATUS")]
        public String Status { get; set; }

        /// <summary>
        /// column: VL_PEDIDO
        ///   type: [DECIMAL]
        /// </summary>
        [SortExpression(SortName = "PE.VL_PEDIDO")]
        public Decimal? VlPedido { get; set; }

        /// <summary>
        /// column: VL_FRETE
        ///   type: [DECIMAL]
        /// </summary>
        [SortExpression(SortName = "PE.VL_FRETE")]
        public Decimal? VlFrete { get; set; }

        /// <summary>
        /// column: VL_DESPESAS_ACESSORIAS
        ///   type: [DECIMAL]
        /// </summary>
        [SortExpression(SortName = "PE.VL_DESPESAS_ACESSORIAS")]
        public Decimal? VlDespesasAcessorias { get; set; }

        /// <summary>
        /// column: VL_MERCADORIA
        ///   type: [DECIMAL]
        /// </summary>
        [SortExpression(SortName = "PE.VL_MERCADORIA")]
        public Decimal? VlMercadoria { get; set; }

        /// <summary>
        /// column: LG_CANCELADO
        ///   type: [VARCHAR]
        /// </summary>
        [SortExpression(SortName = "PE.LG_CANCELADO")]
        public Char? LgCancelado { get; set; }

        /// <summary>
        /// column: LG_PROCESSADO
        ///   type: [VARCHAR]
        /// </summary>
        [SortExpression(SortName = "PE.LG_PROCESSADO")]
        public Char? LgProcessado { get; set; }

        /// <summary>
        /// column: QT_PECAS
        ///   type: [INT]
        /// </summary>
        [SortExpression(SortName = "PE.QT_PECAS")]
        public Int32? QtPecas { get; set; }

        /// <summary>
        /// column: DT_CARTEIRA_BOA
        ///   type: [DATETIME]
        /// </summary>
        [SortExpression(SortName = "PE.DT_CARTEIRA_BOA")]
        public DateTime? DtCarteiraBoa { get; set; }

        /// <summary>
        /// column: DT_COMPROMETIMENTO
        ///   type: [DATETIME]
        /// </summary>
        [SortExpression(SortName = "PE.DT_COMPROMETIMENTO")]
        public DateTime? DtComprometimento { get; set; }

        /// <summary>
        /// column: LG_ATRASO
        ///   type: [VARCHAR]
        /// </summary>
        [SortExpression(SortName = "PE.LG_ATRASO")]
        public Char? LgAtraso { get; set; }

        /// <summary>
        /// column: DD_ATRASO
        ///   type: [SMALLINT]
        /// </summary>
        [SortExpression(SortName = "PE.DD_ATRASO")]
        public Int16? DdAtraso { get; set; }

        /// <summary>
        /// column: DT_PREVISAO
        ///   type: [DATETIME]
        /// </summary>
        [SortExpression(SortName = "PE.DT_PREVISAO")]
        public DateTime? DtPrevisao { get; set; }

        /// <summary>
        /// column: VL_IPI
        ///   type: [DECIMAL]
        /// </summary>
        [SortExpression(SortName = "PE.VL_IPI")]
        public Decimal? VlIpi { get; set; }

        /// <summary>
        /// column: CD_ORGANIZACAO
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(4)]
        [SortExpression(SortName = "PE.CD_ORGANIZACAO")]
        public String CdOrganizacao { get; set; }

        /// <summary>
        /// column: DT_INICIAL
        ///   type: [DATETIME]
        /// </summary>
        [SortExpression(SortName = "PE.DT_INICIAL")]
        public DateTime? DtInicial { get; set; }

        #endregion

        #region -- IDs Mappings --

        public override string BaseEntityID
        {
            get
            {
                return NrPedido.ToString();
            }
        }

        public override void SetBaseEntityID(string id)
        {
            this.NrPedido = Int64.Parse(id.Split('|')[0]);
        }

        public override bool HasID
        {
            get
            {
                return this.NrPedido > 0;
            }
        }

        #endregion

        #region -- Join Properties --

        #endregion
    }
}
