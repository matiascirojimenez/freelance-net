using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using QLibs.Common.Entities;
using QLibs.Common.Attributes;
using RocaAPI.AdminEcomm.Common.Entities;

namespace RocaAPI.AdminEcomm.Common.Entities
{
    /// <summary>
    /// match with table: products
    /// </summary>
    [Serializable]
    public partial class Produto : BaseEntity
    {
        #region -- Private fields --

        #endregion

        #region -- Constructors --

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Produto()
        {
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Produto(String brand, String code)
        {
            this.Brand = brand;
            this.Code = code;

        }

        #endregion

        #region -- Public Properties --

        /// <summary>
        /// column: brand
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(10)]
        [SortExpression(SortName = "PR.brand")]
        public String Brand { get; set; }

        /// <summary>
        /// column: code
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(50)]
        [SortExpression(SortName = "PR.code")]
        public String Code { get; set; }

        /// <summary>
        /// column: ean
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PR.ean")]
        public String Ean { get; set; }

        /// <summary>
        /// column: description
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(255)]
        [SortExpression(SortName = "PR.description")]
        public String Description { get; set; }

        /// <summary>
        /// column: line
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PR.line")]
        public String Line { get; set; }

        /// <summary>
        /// column: category
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PR.category")]
        public String Category { get; set; }

        /// <summary>
        /// column: type
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PR.type")]
        public String Type { get; set; }

        /// <summary>
        /// column: subtype
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PR.subtype")]
        public String Subtype { get; set; }

        /// <summary>
        /// column: images
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(-1)]
        [SortExpression(SortName = "PR.images")]
        public String Images { get; set; }

        /// <summary>
        /// column: package
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PR.package")]
        public String Package { get; set; }

        /// <summary>
        /// column: instalation_stuff
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(255)]
        [SortExpression(SortName = "PR.instalation_stuff")]
        public String InstalationStuff { get; set; }

        /// <summary>
        /// column: warranty
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(100)]
        [SortExpression(SortName = "PR.warranty")]
        public String Warranty { get; set; }

        /// <summary>
        /// column: addinfo_aplication
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PR.addinfo_aplication")]
        public String AddinfoAplication { get; set; }

        /// <summary>
        /// column: addinfo_format
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PR.addinfo_format")]
        public String AddinfoFormat { get; set; }

        /// <summary>
        /// column: addinfo_functionmode
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PR.addinfo_functionmode")]
        public String AddinfoFunctionmode { get; set; }

        /// <summary>
        /// column: addinfo_instalationtype
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PR.addinfo_instalationtype")]
        public String AddinfoInstalationtype { get; set; }

        /// <summary>
        /// column: addinfo_ambience
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PR.addinfo_ambience")]
        public String AddinfoAmbience { get; set; }

        /// <summary>
        /// column: addinfo_waterpoint
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(100)]
        [SortExpression(SortName = "PR.addinfo_waterpoint")]
        public String AddinfoWaterpoint { get; set; }

        /// <summary>
        /// column: addinfo_motion
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PR.addinfo_motion")]
        public String AddinfoMotion { get; set; }

        /// <summary>
        /// column: addinfo_aerator
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PR.addinfo_aerator")]
        public String AddinfoAerator { get; set; }

        /// <summary>
        /// column: addinfo_tap
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PR.addinfo_tap")]
        public String AddinfoTap { get; set; }

        /// <summary>
        /// column: addinfo_waterpression
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(500)]
        [SortExpression(SortName = "PR.addinfo_waterpression")]
        public String AddinfoWaterpression { get; set; }

        /// <summary>
        /// column: addinfo_finish
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PR.addinfo_finish")]
        public String AddinfoFinish { get; set; }

        /// <summary>
        /// column: addinfo_material
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PR.addinfo_material")]
        public String AddinfoMaterial { get; set; }

        /// <summary>
        /// column: addinfo_drilling
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PR.addinfo_drilling")]
        public String AddinfoDrilling { get; set; }

        /// <summary>
        /// column: addinfo_sewer
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PR.addinfo_sewer")]
        public String AddinfoSewer { get; set; }

        /// <summary>
        /// column: addinfo_flush
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PR.addinfo_flush")]
        public String AddinfoFlush { get; set; }

        /// <summary>
        /// column: addinfo_others
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PR.addinfo_others")]
        public String AddinfoOthers { get; set; }

        /// <summary>
        /// column: addinfo_color
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(500)]
        [SortExpression(SortName = "PR.addinfo_color")]
        public String AddinfoColor { get; set; }

        /// <summary>
        /// column: price
        ///   type: [double]
        /// </summary>
        [SortExpression(SortName = "PR.price")]
        public double? Price { get; set; }

        /// <summary>
        /// column: package_weight
        ///   type: [double]
        /// </summary>
        [SortExpression(SortName = "PR.package_weight")]
        public double? PackageWeight { get; set; }

        /// <summary>
        /// column: package_height
        ///   type: [double]
        /// </summary>
        [SortExpression(SortName = "PR.package_height")]
        public double? PackageHeight { get; set; }

        /// <summary>
        /// column: package_width
        ///   type: [double]
        /// </summary>
        [SortExpression(SortName = "PR.package_width")]
        public double? PackageWidth { get; set; }

        /// <summary>
        /// column: package_depth
        ///   type: [double]
        /// </summary>
        [SortExpression(SortName = "PR.package_depth")]
        public double? PackageDepth { get; set; }

        /// <summary>
        /// column: NCM
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PR.NCM")]
        public String Ncm { get; set; }

        /// <summary>
        /// column: price_full
        ///   type: [double]
        /// </summary>
        [SortExpression(SortName = "PR.price_full")]
        public double? PriceFull { get; set; }

        /// <summary>
        /// column: product_height
        ///   type: [double]
        /// </summary>
        [SortExpression(SortName = "PR.product_height")]
        public double? ProductHeight { get; set; }

        /// <summary>
        /// column: product_width
        ///   type: [double]
        /// </summary>
        [SortExpression(SortName = "PR.product_width")]
        public double? ProductWidth { get; set; }

        /// <summary>
        /// column: product_depth
        ///   type: [double]
        /// </summary>
        [SortExpression(SortName = "PR.product_depth")]
        public double? ProductDepth { get; set; }

        /// <summary>
        /// column: update_date
        ///   type: [DATETIME]
        /// </summary>
        [SortExpression(SortName = "PR.update_date")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// column: zzoutlet
        ///   type: [CHAR]
        /// </summary>
        [SortExpression(SortName = "PR.zzoutlet")]
        public Char? Zzoutlet { get; set; }

        /// <summary>
        /// column: novo_outlet
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(5)]
        [SortExpression(SortName = "PR.novo_outlet")]
        public String NovoOutlet { get; set; }

        /// <summary>
        /// column: mkt_text
        ///   type: [VARCHAR]
        /// </summary>
        [SortExpression(SortName = "PR.mkt_text")]
        public String MktText { get; set; }

        /// <summary>
        /// column: destaque
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(5)]
        [SortExpression(SortName = "PR.destaque")]
        public String Destaque { get; set; }

        /// <summary>
        /// column: URL_VIDEO_TEC
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(255)]
        [SortExpression(SortName = "PR.URL_VIDEO_TEC")]
        public String UrlVideoTec { get; set; }

        /// <summary>
        /// column: URL_VIDEO_INST
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(255)]
        [SortExpression(SortName = "PR.URL_VIDEO_INST")]
        public String UrlVideoInst { get; set; }

        /// <summary>
        /// column: URL_VIDEO_PUB
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(255)]
        [SortExpression(SortName = "PR.URL_VIDEO_PUB")]
        public String UrlVideoPub { get; set; }

        /// <summary>
        /// column: cd_linha
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(10)]
        [SortExpression(SortName = "PR.cd_linha")]
        public String CdLinha { get; set; }

        /// <summary>
        /// column: cd_segmento
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(10)]
        [SortExpression(SortName = "PR.cd_segmento")]
        public String CdSegmento { get; set; }

        /// <summary>
        /// column: cd_classe
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(30)]
        [SortExpression(SortName = "PR.cd_classe")]
        public String CdClasse { get; set; }

        /// <summary>
        /// column: carrossel02
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(5)]
        [SortExpression(SortName = "PR.carrossel02")]
        public String Carrossel02 { get; set; }

        #endregion

        #region -- IDs Mappings --

        public override string BaseEntityID
        {
            get
            {
                return Brand.ToString() + '|' + Code.ToString();
            }
        }

        public override void SetBaseEntityID(string id)
        {
            this.Brand = id.Split('|')[0];
            this.Code = id.Split('|')[1];

        }

        public override bool HasID
        {
            get
            {
                return !string.IsNullOrEmpty(this.Brand) && !string.IsNullOrEmpty(this.Code);
            }
        }

        #endregion

        #region -- Join Properties --

        #endregion
    }
}
