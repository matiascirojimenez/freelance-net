﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RocaAPI.AdminEcomm.Common.Entities
{
    public interface _ISincronizableEntity
    {
        string RowStatus { get; set; }
    }
}
