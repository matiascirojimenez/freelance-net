using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using QLibs.Common.Entities;
using QLibs.Common.Attributes;
using RocaAPI.AdminEcomm.Common.Entities;

namespace RocaAPI.AdminEcomm.Common.Entities
{
    /// <summary>
    /// match with table: PEDIDO_AGRUPADORZN
    /// </summary>
    [Serializable]
    public partial class PedidoAgrupadorZN : BaseEntity
    {
        #region -- Private fields --

        #endregion

        #region -- Constructors --

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PedidoAgrupadorZN()
        {
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PedidoAgrupadorZN(String nrpedido, String kunnrzn)
        {
            this.NrPedido = nrpedido;
            this.KunnrZn = kunnrzn;

        }

        #endregion

        #region -- Public Properties --

        /// <summary>
        /// column: nr_pedido
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PE.nr_pedido")]
        public String NrPedido { get; set; }

        /// <summary>
        /// column: kunnr_zn
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "PE.kunnr_zn")]
        public String KunnrZn { get; set; }

        #endregion

        #region -- IDs Mappings --

        public override string BaseEntityID
        {
            get
            {
                return NrPedido.ToString() + '|' + KunnrZn.ToString();
            }
        }

        public override void SetBaseEntityID(string id)
        {
            this.NrPedido = id.Split('|')[0];
            this.KunnrZn = id.Split('|')[1];

        }

        public override bool HasID
        {
            get
            {
                return !string.IsNullOrEmpty(this.NrPedido) && !string.IsNullOrEmpty(this.KunnrZn);
            }
        }

        #endregion

        #region -- Join Properties --

        #endregion
    }
}
