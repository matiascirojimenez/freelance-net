using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using QLibs.Common.Entities;
using QLibs.Common.Attributes;
using RocaAPI.AdminEcomm.Common.Entities;

namespace RocaAPI.AdminEcomm.Common.Entities
{
    /// <summary>
    /// match with table: CLIENTE
    /// </summary>
    [Serializable]
    public partial class Cliente : BaseEntity, IJSONComboOptionEntity
    {
        #region -- Private fields --

        #endregion

        #region -- Constructors --

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Cliente()
        {
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Cliente(Int32 cdcliente)
        {
            this.CdCliente = cdcliente;

        }

        #endregion

        #region -- Public Properties --

        /// <summary>
        /// column: CD_CLIENTE
        ///   type: [INT]
        /// </summary>
        [Required]
        [SortExpression(SortName = "CL.CD_CLIENTE")]
        public Int32 CdCliente { get; set; }

        /// <summary>
        /// column: NR_CNPJ
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(14)]
        [SortExpression(SortName = "CL.NR_CNPJ")]
        public String NrCnpj { get; set; }

        /// <summary>
        /// column: NR_CPF
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(11)]
        [SortExpression(SortName = "CL.NR_CPF")]
        public String NrCpf { get; set; }

        /// <summary>
        /// column: NM_ESTABELECIMENTO
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(255)]
        [SortExpression(SortName = "CL.NM_ESTABELECIMENTO")]
        public String NmEstabelecimento { get; set; }

        /// <summary>
        /// column: NM_REDUZIDO
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(50)]
        [SortExpression(SortName = "CL.NM_REDUZIDO")]
        public String NmReduzido { get; set; }

        /// <summary>
        /// column: CD_RAMO
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(2)]
        [SortExpression(SortName = "CL.CD_RAMO")]
        public String CdRamo { get; set; }

        /// <summary>
        /// column: NR_IE
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(18)]
        [SortExpression(SortName = "CL.NR_IE")]
        public String NrIe { get; set; }

        /// <summary>
        /// column: DS_ENDERECO
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(255)]
        [SortExpression(SortName = "CL.DS_ENDERECO")]
        public String DsEndereco { get; set; }

        /// <summary>
        /// column: DS_NUMERO
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [SortExpression(SortName = "CL.DS_NUMERO")]
        public String DsNumero { get; set; }

        /// <summary>
        /// column: DS_COMPLEMENTO
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(255)]
        [SortExpression(SortName = "CL.DS_COMPLEMENTO")]
        public String DsComplemento { get; set; }

        /// <summary>
        /// column: DS_BAIRRO
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(255)]
        [SortExpression(SortName = "CL.DS_BAIRRO")]
        public String DsBairro { get; set; }

        /// <summary>
        /// column: NM_CIDADE
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(255)]
        [SortExpression(SortName = "CL.NM_CIDADE")]
        public String NmCidade { get; set; }

        /// <summary>
        /// column: SG_UF
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(3)]
        [SortExpression(SortName = "CL.SG_UF")]
        public String SgUf { get; set; }

        /// <summary>
        /// column: NR_CEP
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(10)]
        [SortExpression(SortName = "CL.NR_CEP")]
        public String NrCep { get; set; }

        /// <summary>
        /// column: NR_FONE
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(16)]
        [SortExpression(SortName = "CL.NR_FONE")]
        public String NrFone { get; set; }

        /// <summary>
        /// column: NR_FAX
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(16)]
        [SortExpression(SortName = "CL.NR_FAX")]
        public String NrFax { get; set; }

        /// <summary>
        /// column: DS_EMAIL
        ///   type: [VARCHAR]
        /// </summary>
        [Required]
        [StringLength(255)]
        [EmailAddress]
        [SortExpression(SortName = "CL.DS_EMAIL")]
        public String DsEmail { get; set; }

        /// <summary>
        /// column: CD_TIPO_CLIENTE
        ///   type: [INT]
        /// </summary>
        [Required]
        [SortExpression(SortName = "CL.CD_TIPO_CLIENTE")]
        public Int32 CdTipoCliente { get; set; }

        /// <summary>
        /// column: CD_SITUACAO
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(2)]
        [SortExpression(SortName = "CL.CD_SITUACAO")]
        public String CdSituacao { get; set; }

        /// <summary>
        /// column: CD_CLIENTE_PAGADOR
        ///   type: [INT]
        /// </summary>
        [SortExpression(SortName = "CL.CD_CLIENTE_PAGADOR")]
        public Int32? CdClientePagador { get; set; }

        /// <summary>
        /// column: NR_SUFRAMA
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(18)]
        [SortExpression(SortName = "CL.NR_SUFRAMA")]
        public String NrSuframa { get; set; }

        /// <summary>
        /// column: LG_CONTRIBUINTE
        ///   type: [VARCHAR]
        /// </summary>
        [SortExpression(SortName = "CL.LG_CONTRIBUINTE")]
        public Char? LgContribuinte { get; set; }

        /// <summary>
        /// column: CD_ERV
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(4)]
        [SortExpression(SortName = "CL.CD_ERV")]
        public String CdErv { get; set; }

        /// <summary>
        /// column: CD_CLASSIFICACAO
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(2)]
        [SortExpression(SortName = "CL.CD_CLASSIFICACAO")]
        public String CdClassificacao { get; set; }

        /// <summary>
        /// column: CD_CLASSIFICACAO_CONTABIL
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(2)]
        [SortExpression(SortName = "CL.CD_CLASSIFICACAO_CONTABIL")]
        public String CdClassificacaoContabil { get; set; }

        /// <summary>
        /// column: CD_CLASSIFICACAO_FISCAL
        ///   type: [VARCHAR]
        /// </summary>
        [SortExpression(SortName = "CL.CD_CLASSIFICACAO_FISCAL")]
        public Char? CdClassificacaoFiscal { get; set; }

        /// <summary>
        /// column: DT_CADASTRO
        ///   type: [DATETIME]
        /// </summary>
        [Required]
        [SortExpression(SortName = "CL.DT_CADASTRO")]
        public DateTime DtCadastro { get; set; }

        /// <summary>
        /// column: NR_RAPPEL
        ///   type: [INT]
        /// </summary>
        [SortExpression(SortName = "CL.NR_RAPPEL")]
        public Int32? NrRappel { get; set; }

        /// <summary>
        /// column: DS_ENDERECO_COBRANCA
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(35)]
        [SortExpression(SortName = "CL.DS_ENDERECO_COBRANCA")]
        public String DsEnderecoCobranca { get; set; }

        /// <summary>
        /// column: DS_BAIRRO_COBRANCA
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(25)]
        [SortExpression(SortName = "CL.DS_BAIRRO_COBRANCA")]
        public String DsBairroCobranca { get; set; }

        /// <summary>
        /// column: NM_CIDADE_COBRANCA
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(25)]
        [SortExpression(SortName = "CL.NM_CIDADE_COBRANCA")]
        public String NmCidadeCobranca { get; set; }

        /// <summary>
        /// column: SG_UF_COBRANCA
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(3)]
        [SortExpression(SortName = "CL.SG_UF_COBRANCA")]
        public String SgUfCobranca { get; set; }

        /// <summary>
        /// column: NR_CEP_COBRANCA
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(10)]
        [SortExpression(SortName = "CL.NR_CEP_COBRANCA")]
        public String NrCepCobranca { get; set; }

        /// <summary>
        /// column: NR_FONE_COBRANCA
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(16)]
        [SortExpression(SortName = "CL.NR_FONE_COBRANCA")]
        public String NrFoneCobranca { get; set; }

        /// <summary>
        /// column: DS_EMAIL_NFE
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(50)]
        [EmailAddress]
        [SortExpression(SortName = "CL.DS_EMAIL_NFE")]
        public String DsEmailNfe { get; set; }

        /// <summary>
        /// column: CD_CATEGORIA_CFOP
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(2)]
        [SortExpression(SortName = "CL.CD_CATEGORIA_CFOP")]
        public String CdCategoriaCfop { get; set; }

        /// <summary>
        /// column: DS_EMAIL_DANFE
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(255)]
        [EmailAddress]
        [SortExpression(SortName = "CL.DS_EMAIL_DANFE")]
        public String DsEmailDanfe { get; set; }

        /// <summary>
        /// column: MARGEM_CLI
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(12)]
        [SortExpression(SortName = "CL.MARGEM_CLI")]
        public String MargemCli { get; set; }

        /// <summary>
        /// column: MARGEM_CANAL
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(12)]
        [SortExpression(SortName = "CL.MARGEM_CANAL")]
        public String MargemCanal { get; set; }

        /// <summary>
        /// column: CD_SEGMENTACAO
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(2)]
        [SortExpression(SortName = "CL.CD_SEGMENTACAO")]
        public String CdSegmentacao { get; set; }

        /// <summary>
        /// column: REDE
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(15)]
        [SortExpression(SortName = "CL.REDE")]
        public String Rede { get; set; }

        /// <summary>
        /// column: FL_ISNEW
        ///   type: [BIT]
        /// </summary>
        [Required]
        [SortExpression(SortName = "CL.FL_ISNEW")]
        public Boolean FlIsnew { get; set; }

        /// <summary>
        /// column: FL_INTEGRATED
        ///   type: [BIT]
        /// </summary>
        [Required]
        [SortExpression(SortName = "CL.FL_INTEGRATED")]
        public Boolean FlIntegrated { get; set; }

        /// <summary>
        /// column: IE
        ///   type: [VARCHAR]
        /// </summary>
        [StringLength(12)]
        [SortExpression(SortName = "CL.IE")]
        public String Ie { get; set; }

        #endregion

        #region -- IDs Mappings --

        public override string BaseEntityID
        {
            get
            {
                return CdCliente.ToString();
            }
        }

        public override void SetBaseEntityID(string id)
        {
            this.CdCliente = Int32.Parse(id.Split('|')[0]);

        }

        public override bool HasID
        {
            get
            {
                return this.CdCliente > 0;
            }
        }

        public bool Habilitado => true;

        #endregion

        #region -- IJSONComboOptionEntity --

        public string GetDescription()
        {
            return this.NmEstabelecimento;
        }

        public string GetValue()
        {
            return this.CdCliente.ToString();
        }

        #endregion

        #region -- Join Properties --

        #endregion
    }
}
