﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RocaAPI.AdminEcomm.Common.DTO.Sincronizacion
{
    [Serializable]
    public enum ResultadoCargaLoteEnum
    {
        Ok = 0,
        OkConErrores = 1,
        Error = 2
    }

    [Serializable]
    public class ResultadoSincronizacionERP
    {
        #region -- Constructors --

        public ResultadoSincronizacionERP()
        {
            this.Resultado = ResultadoCargaLoteEnum.Ok;
            this.Detalles = new List<ResultadoSincronizacionItemERP>();
        }

        public ResultadoSincronizacionERP(ResultadoCargaLoteEnum r, string m)
        {
            this.Detalles = new List<ResultadoSincronizacionItemERP>();
            this.Resultado = r;
            this.Mensaje = m;
        }


        #endregion

        #region -- Public Properties --

        public ResultadoCargaLoteEnum Resultado { get; set; }

        public List<ResultadoSincronizacionItemERP> Detalles { get; set; }

        public string Mensaje { get; set; }

        public bool CodigoConError(string identificador)
        {
            if (Detalles != null && Detalles.Count > 0 && Detalles.Count(d => d.Codigo == identificador) > 0)
                return true;
            return false;
        }

        #endregion

        public void AgregarDetalle(ResultadoSincronizacionItemERP detalle)
        {
            this.Detalles.Add(detalle);

            if (this.Resultado == ResultadoCargaLoteEnum.Ok)
                if (detalle.Resultado == false)
                    this.Resultado = ResultadoCargaLoteEnum.OkConErrores;
        }

        public bool CodigoConError(object codigo)
        {
            throw new NotImplementedException();
        }

        public bool IdentificadorConError(string Codigo)
        {
            if (Detalles != null && Detalles.Count > 0 && Detalles.Count(d => d.Codigo == Codigo) > 0)
                return true;
            return false;
        }
    }

    [Serializable]
    public class ResultadoSincronizacionItemERP
    {
        public ResultadoSincronizacionItemERP() { }
        public ResultadoSincronizacionItemERP(bool r, string m, string i = null, string isb = null)
        {
            this.Codigo = i;
            this.CodigoSaleBinds = isb;
            this.Mensajes = new string[] { m };
            this.Resultado = r;
        }
        public string Codigo { get; set; }
        public string CodigoSaleBinds { get; set; }
        public bool Resultado { get; set; }
        public string[] Mensajes { get; set; }
    }
}
