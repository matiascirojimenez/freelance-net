﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RocaAPI.AdminEcomm.Common.DTO.Sincronizacion
{
    public interface IERPDataItem
    {
        String Codigo { get; set; }
    }

    public class IERPDataItemUtils
    {
        public static bool ValidarIdentificador(IERPDataItem item)
        {
            if (string.IsNullOrEmpty(item.Codigo) || item.Codigo.Trim().Length == 0)
                return false;
            item.Codigo = item.Codigo.Trim().ToUpper();
            return true;
        }

        public static bool ValidarUnico(IERPDataItem[] items)
        {
            return ValidarUnico(items.Select(d => d.Codigo));
        }

        public static bool ValidarUnico(IEnumerable<string> items)
        {
            var count = items.Where(d => !string.IsNullOrEmpty(d)).Select(d => d.Trim().ToUpper()).Distinct().Count();
            if (count != items.Count())
                return false;
            return true;
        }

        public static bool ValidarNombre(string p)
        {
            if (string.IsNullOrEmpty(p) || p.Trim().Length == 0)
                return false;
            return true;
        }

        public static bool ValidarLogitud(string valor, int lenght)
        {
            if (valor.Length > lenght)
                return false;
            return true;
        }
    }
}
