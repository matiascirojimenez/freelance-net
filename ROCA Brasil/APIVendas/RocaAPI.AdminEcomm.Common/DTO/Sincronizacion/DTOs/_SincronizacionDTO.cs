﻿using QLibs.Excel.Attributes;
using System.ComponentModel.DataAnnotations;

namespace RocaAPI.AdminEcomm.Common.DTO.Sincronizacion
{
    public class SincronizacionDTO
    {
        [StringLength(100)]
        [ExcelImportExportProperty(0, "Codigo", true)]
        public string Codigo { get; set; }
    }
}