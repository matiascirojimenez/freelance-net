﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RocaAPI.AdminEcomm.Common.Utils
{
    public class HtmlHelper
    {
        public enum HtmlHelperLabelType
        {
            brand,
            warning,
            danger,
            success,
            primary,
        }

        public static string BoxLabelColored(string text, HtmlHelperLabelType type = HtmlHelperLabelType.brand, bool bold = true, string classData = "")
        {
            return @"<span class='btn btn-sm btn-label-" + type + (bold ? " btn-bold " : " ") + classData + "'>" + text + "</span>";
        }

        public static string LabelColored(string text, HtmlHelperLabelType type = HtmlHelperLabelType.brand, bool bold = true, string classData = "")
        {
            return @"<span class='kt-font-" + type + (bold ? " kt-font-bold " : " ") + classData + "'>" + text + "</span>";
        }
    }
}
