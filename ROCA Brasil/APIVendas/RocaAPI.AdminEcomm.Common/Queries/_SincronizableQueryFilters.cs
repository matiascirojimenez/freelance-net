﻿using QLibs.Common.DTO.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RocaAPI.AdminEcomm.Common.Queries
{
    public class _SincronizableQueryFilters : BaseQueryFilters
    {
        public _SincronizableQueryFilters()
            : base()
        {
        }

        public _SincronizableQueryFilters(int currentPage, int pageSize, params OrderByColumn[] obs)
            : base(currentPage, pageSize, obs)
        {
        }

        public _SincronizableQueryFilters(int currentPage, int pageSize, string sortText)
            : base(currentPage, pageSize, sortText)
        {
        }

        public DateTime? FechaUltimaModificacion { get; set; }
        public bool ExcluirEliminados { get; set; } = true;
    }
}
