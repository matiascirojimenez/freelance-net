using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QLibs.Common.DTO.Queries;

namespace RocaAPI.AdminEcomm.Common.Queries
{
    [Serializable]
    public class PedidoInternoQueryFilters : BaseQueryFilters
    {
        #region -- Constructors --

        public PedidoInternoQueryFilters()
            : base()
        {
        }

        public PedidoInternoQueryFilters(int currentPage, int pageSize, params OrderByColumn[] obs)
            : base(currentPage, pageSize, obs)
        {
        }

        public PedidoInternoQueryFilters(int currentPage, int pageSize, string sortText)
            : base(currentPage, pageSize, sortText)
        {
        }

        #endregion

        public Int64? CdClienteEmissor { get; set; }

public Int64? CdClienteRecebedor { get; set; }

        public Int32? CdCliente { get; set; }

        public String NrPedidoCliente { get; set; }

        public String NrPedidoSap { get; set; }

        public DateTime? DtEntradaFrom { get; set; }

        public DateTime? DtEntradaTo { get; set; }

    }
}
