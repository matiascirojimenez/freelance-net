using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QLibs.Common.DTO.Queries;

namespace RocaAPI.AdminEcomm.Common.Queries
{
    [Serializable]
    public class ProdutoQueryFilters : BaseQueryFilters
    {
        #region -- Constructors --

        public ProdutoQueryFilters()
            : base()
        {
        }

        public ProdutoQueryFilters(int currentPage, int pageSize, params OrderByColumn[] obs)
            : base(currentPage, pageSize, obs)
        {
        }

        public ProdutoQueryFilters(int currentPage, int pageSize, string sortText)
            : base(currentPage, pageSize, sortText)
        {
        }

        #endregion

        public string Brand { get; set; }
        public string Code { get; set; }
        public string Ean { get; set; }
        public string Description { get; set; }
        public string Line { get; set; }
        public string Category { get; set; }
        public string Type { get; set; }
        public string SubType { get; set; }
    }
}
