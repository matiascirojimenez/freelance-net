using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QLibs.Common.DTO.Queries;

namespace RocaAPI.AdminEcomm.Common.Queries
{
    [Serializable]
    public class PedidoItemQueryFilters : BaseQueryFilters
    {
        #region -- Constructors --

        public PedidoItemQueryFilters()
            : base()
        {
        }

        public PedidoItemQueryFilters(int currentPage, int pageSize, params OrderByColumn[] obs)
            : base(currentPage, pageSize, obs)
        {
        }

        public PedidoItemQueryFilters(int currentPage, int pageSize, string sortText)
            : base(currentPage, pageSize, sortText)
        {
        }

        #endregion

        public string NrPedido { get; set; }

public string NrControle { get; set; }


    }
}
