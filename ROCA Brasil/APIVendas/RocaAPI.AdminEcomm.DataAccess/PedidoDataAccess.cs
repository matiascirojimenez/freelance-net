using QLibs.DataAccess;
using RocaAPI.AdminEcomm.Common.Entities;
using RocaAPI.AdminEcomm.Common.Queries;
using RocaAPI.AdminEcomm.DataAccess.DBMappers;
using System;

namespace RocaAPI.AdminEcomm.DataAccess
{
    public partial class PedidoDataAccess : BaseEntityDataAccess<PedidoDBMapper, Pedido, PedidoQueryFilters, Int64>
    {
        #region -- Constructors --

        public PedidoDataAccess() { }

        public PedidoDataAccess(string connName) : base(connName) { }

        public PedidoDataAccess(QDatabase database) : base(database) { }

        #endregion
        
        #region -- Public Methods --

        #endregion

        #region -- Private Methods --

        #endregion
    }
}
