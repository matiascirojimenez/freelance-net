using QLibs.DataAccess;
using QLibs.DataAccess.Utils;
using RocaAPI.AdminEcomm.Common.Entities;
using RocaAPI.AdminEcomm.Common.Queries;
using RocaAPI.AdminEcomm.DataAccess.DBMappers;
using System;
using System.Collections.Generic;
using System.Data;

namespace RocaAPI.AdminEcomm.DataAccess
{
    public partial class ProdutoDataAccess : BaseEntityDataAccess<ProdutoDBMapper, Produto, ProdutoQueryFilters, String>
    {
        #region -- Constructors --

        public ProdutoDataAccess() { }

        public ProdutoDataAccess(string connName) : base(connName) { }

        public ProdutoDataAccess(QDatabase database) : base(database) { }

        #endregion

        #region -- Public Methods --

        public List<string> GetBrand()
        {
            return GetValuesColumn("GetBrand");
        }

        public List<string> GetLines()
        {
            return GetValuesColumn("GetLine");
        }

        public List<string> GetCategories()
        {
            return GetValuesColumn("GetCategory");
        }

        public List<string> GetTypes()
        {
            return GetValuesColumn("GetType");
        }

        public List<string> GetSubTypes()
        {
            return GetValuesColumn("GetSubType");
        }

        #endregion

        #region -- Private Methods --

        private List<string> GetValuesColumn(string query)
        {
            var result = new List<string>();
            var sqlText = DataAccessHelper.GetQuery(string.Format("{0}.{1}", GetType().Name, query));
            using (var command = GetSqlStringCommand(sqlText))
            using (IDataReader reader = this.ExecuteReader(command))
                while (reader.Read())
                    result.Add(reader.GetString(0));
            return result;
        }

        #endregion
    }
}
