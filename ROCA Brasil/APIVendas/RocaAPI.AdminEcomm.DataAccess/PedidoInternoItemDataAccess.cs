using QLibs.DataAccess;
using RocaAPI.AdminEcomm.Common.Entities;
using RocaAPI.AdminEcomm.Common.Queries;
using RocaAPI.AdminEcomm.DataAccess.DBMappers;
using System;

namespace RocaAPI.AdminEcomm.DataAccess
{
    public partial class PedidoInternoItemDataAccess : BaseEntityDataAccess<PedidoInternoItemDBMapper, PedidoInternoItem, PedidoInternoItemQueryFilters, Guid>
    {
        #region -- Constructors --

        public PedidoInternoItemDataAccess() { }

        public PedidoInternoItemDataAccess(string connName) : base(connName) { }

        public PedidoInternoItemDataAccess(QDatabase database) : base(database) { }

        #endregion
        
        #region -- Public Methods --

        #endregion

        #region -- Private Methods --

        #endregion
    }
}
