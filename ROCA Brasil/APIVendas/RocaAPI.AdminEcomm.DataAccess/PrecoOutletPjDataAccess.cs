using QLibs.Common.DTO.Queries;
using QLibs.DataAccess;
using RocaAPI.AdminEcomm.Common.Entities;
using RocaAPI.AdminEcomm.DataAccess.DBMappers;
using System;

namespace RocaAPI.AdminEcomm.DataAccess
{
    public partial class PrecoOutletPjDataAccess : BaseEntityDataAccess<PrecoOutletPjDBMapper, PrecoOutletPj, BaseQueryFilters, Decimal?>
    {
        #region -- Constructors --

        public PrecoOutletPjDataAccess() { }

        public PrecoOutletPjDataAccess(string connName) : base(connName) { }

        public PrecoOutletPjDataAccess(QDatabase database) : base(database) { }

        #endregion
        
        #region -- Public Methods --

        #endregion

        #region -- Private Methods --

        #endregion
    }
}
