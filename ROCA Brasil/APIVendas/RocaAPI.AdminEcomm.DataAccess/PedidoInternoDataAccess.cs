using QLibs.DataAccess;
using QLibs.DataAccess.Utils;
using RocaAPI.AdminEcomm.Common.Entities;
using RocaAPI.AdminEcomm.Common.Queries;
using RocaAPI.AdminEcomm.DataAccess.DBMappers;
using System;
using System.Linq;

namespace RocaAPI.AdminEcomm.DataAccess
{
    [QLibs.DataAccess.Attributes.EntityRelations(LoadRelationsOnGetById = true)]
    public partial class PedidoInternoDataAccess : BaseEntityDataAccess<PedidoInternoDBMapper, PedidoInterno, PedidoInternoQueryFilters, Guid>
    {
        #region -- Constructors --

        public PedidoInternoDataAccess() { }

        public PedidoInternoDataAccess(string connName) : base(connName) { }

        public PedidoInternoDataAccess(QDatabase database) : base(database) { }

        #endregion

        #region -- Override Methods --

        protected override void FillRelations(PedidoInterno entity)
        {
            using (var dac = new PedidoInternoItemDataAccess(this))
            {
                entity.Items = dac.Query(new PedidoInternoItemQueryFilters() { NrControle = entity.NrControle.ToString() }).ToList();
            }
        }

        #endregion

        #region -- Public Methods --

        #endregion

        #region -- Private Methods --

        #endregion
    }
}
