using QLibs.DataAccess;
using QLibs.DataAccess.Utils;
using RocaAPI.AdminEcomm.Common.Entities;
using RocaAPI.AdminEcomm.Common.Queries;
using RocaAPI.AdminEcomm.DataAccess.DBMappers;
using System;

namespace RocaAPI.AdminEcomm.DataAccess
{
    public partial class ClienteDataAccess : BaseEntityDataAccess<ClienteDBMapper, Cliente, ClienteQueryFilters, Int32>
    {
        #region -- Constructors --

        public ClienteDataAccess() { }

        public ClienteDataAccess(string connName) : base(connName) { }

        public ClienteDataAccess(QDatabase database) : base(database) { }

        #endregion
                
        #region -- Override Methods --
        
        protected override bool DoCreate(Cliente entity, bool fromImport = false)
        {
            entity.CdCliente = base.DoCreateIdentity(entity);
            return entity.CdCliente > 0;
        }

        #endregion

        #region -- Public Methods --

        public bool UpdateCliente(Cliente cliente)
        {
            var sqlText = DataAccessHelper.GetQuery(string.Format("{0}.{1}", GetType().Name, "UpdateCliente"));

            using (var command = GetSqlStringCommand(sqlText))
            {
                DBMapperHelper.AddInParameter<int>(this, command, "CdCliente", cliente.CdCliente);
                DBMapperHelper.AddInParameter<string>(this, command, "DsEndereco", cliente.DsEndereco);
                DBMapperHelper.AddInParameter<string>(this, command, "DsNumero", cliente.DsNumero);
                DBMapperHelper.AddInParameter<string>(this, command, "DsComplemento", cliente.DsComplemento);
                DBMapperHelper.AddInParameter<string>(this, command, "DsBairro", cliente.DsBairro);
                DBMapperHelper.AddInParameter<string>(this, command, "NmCidade", cliente.NmCidade);
                DBMapperHelper.AddInParameter<string>(this, command, "NrCep", cliente.NrCep);
                DBMapperHelper.AddInParameter<string>(this, command, "NrSuframa", cliente.NrSuframa);

                return this.ExecuteNonQuery(command) > 0;
            }
        }

        #endregion

        #region -- Private Methods --

        #endregion
    }
}
