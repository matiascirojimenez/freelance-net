using QLibs.DataAccess;
using RocaAPI.AdminEcomm.Common.Entities;
using RocaAPI.AdminEcomm.Common.Queries;
using RocaAPI.AdminEcomm.DataAccess.DBMappers;
using System;

namespace RocaAPI.AdminEcomm.DataAccess
{
    public partial class PedidoItemDataAccess : BaseEntityDataAccess<PedidoItemDBMapper, PedidoItem, PedidoItemQueryFilters, Int64>
    {
        #region -- Constructors --

        public PedidoItemDataAccess() { }

        public PedidoItemDataAccess(string connName) : base(connName) { }

        public PedidoItemDataAccess(QDatabase database) : base(database) { }

        #endregion
        
        #region -- Public Methods --

        #endregion

        #region -- Private Methods --

        #endregion
    }
}
