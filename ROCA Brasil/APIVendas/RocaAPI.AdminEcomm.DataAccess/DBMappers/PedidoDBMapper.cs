using QLibs.DataAccess;
using QLibs.DataAccess.DBMappers;
using QLibs.DataAccess.Utils;
using RocaAPI.AdminEcomm.Common.Entities;
using RocaAPI.AdminEcomm.Common.Queries;
using System;
using System.Data;
using System.Data.Common;

namespace RocaAPI.AdminEcomm.DataAccess.DBMappers
{
    /// <summary>
    /// Represents a mapper of the class <see cref="Pedido"/>.
    /// </summary>
    public class PedidoDBMapper : BaseDBMapper<Pedido, PedidoQueryFilters>
    {
        #region -- Build Methods --

        public override Pedido BuildEntity(IDataReader reader)
        {
            Pedido entity = new Pedido();

            entity.NrPedido = DBMapperHelper.GetColumnInt64Value(reader, "NR_PEDIDO");
            entity.NrControle = DBMapperHelper.GetColumnStringValue(reader, "NR_CONTROLE");
            entity.CdMarca = DBMapperHelper.GetColumnStringValue(reader, "CD_MARCA");
            entity.CdVendedor = DBMapperHelper.GetColumnStringValue(reader, "CD_VENDEDOR");
            entity.CdClienteEmissor = DBMapperHelper.GetColumnInt64Value(reader, "CD_CLIENTE_EMISSOR");
            entity.CdClienteRecebedor = DBMapperHelper.GetColumnNulleableInt64Value(reader, "CD_CLIENTE_RECEBEDOR");
            entity.NrPedidoCliente = DBMapperHelper.GetColumnStringValue(reader, "NR_PEDIDO_CLIENTE");
            entity.CdTipoPedido = DBMapperHelper.GetColumnStringValue(reader, "CD_TIPO_PEDIDO");
            entity.DtEntrada = DBMapperHelper.GetColumnNulleableDateTimeValue(reader, "DT_ENTRADA");
            entity.DtProgramacao = DBMapperHelper.GetColumnNulleableDateTimeValue(reader, "DT_PROGRAMACAO");
            entity.CdFrete = DBMapperHelper.GetColumnStringValue(reader, "CD_FRETE");
            entity.CdPrazo = DBMapperHelper.GetColumnStringValue(reader, "CD_PRAZO");
            entity.CdTabelaPreco = DBMapperHelper.GetColumnStringValue(reader, "CD_TABELA_PRECO");
            entity.AaTabelaPreco = DBMapperHelper.GetColumnNulleableInt16Value(reader, "AA_TABELA_PRECO");
            entity.CdBloqueio = DBMapperHelper.GetColumnStringValue(reader, "CD_BLOQUEIO");
            entity.DsObservacao1 = DBMapperHelper.GetColumnStringValue(reader, "DS_OBSERVACAO1");
            entity.DsObservacao2 = DBMapperHelper.GetColumnStringValue(reader, "DS_OBSERVACAO2");
            entity.Status = DBMapperHelper.GetColumnStringValue(reader, "STATUS");
            entity.VlPedido = DBMapperHelper.GetColumnNulleableDecimalValue(reader, "VL_PEDIDO");
            entity.VlFrete = DBMapperHelper.GetColumnNulleableDecimalValue(reader, "VL_FRETE");
            entity.VlDespesasAcessorias = DBMapperHelper.GetColumnNulleableDecimalValue(reader, "VL_DESPESAS_ACESSORIAS");
            entity.VlMercadoria = DBMapperHelper.GetColumnNulleableDecimalValue(reader, "VL_MERCADORIA");
            entity.LgCancelado = DBMapperHelper.GetColumnNulleableCharValueFromString(reader, "LG_CANCELADO");
            entity.LgProcessado = DBMapperHelper.GetColumnNulleableCharValueFromString(reader, "LG_PROCESSADO");
            entity.QtPecas = DBMapperHelper.GetColumnNulleableInt32Value(reader, "QT_PECAS");
            entity.DtCarteiraBoa = DBMapperHelper.GetColumnNulleableDateTimeValue(reader, "DT_CARTEIRA_BOA");
            entity.DtComprometimento = DBMapperHelper.GetColumnNulleableDateTimeValue(reader, "DT_COMPROMETIMENTO");
            entity.LgAtraso = DBMapperHelper.GetColumnNulleableCharValueFromString(reader, "LG_ATRASO");
            entity.DdAtraso = DBMapperHelper.GetColumnNulleableInt16Value(reader, "DD_ATRASO");
            entity.DtPrevisao = DBMapperHelper.GetColumnNulleableDateTimeValue(reader, "DT_PREVISAO");
            entity.VlIpi = DBMapperHelper.GetColumnNulleableDecimalValue(reader, "VL_IPI");
            entity.CdOrganizacao = DBMapperHelper.GetColumnStringValue(reader, "CD_ORGANIZACAO");
            entity.DtInicial = DBMapperHelper.GetColumnNulleableDateTimeValue(reader, "DT_INICIAL");


            return entity;
        }

        #endregion

        #region -- QueryFilters --

        public override QueryBuilder GetWhereClause(QDatabase database, DbCommand command, bool hasWhere, PedidoQueryFilters qFilters)
        {
            QueryBuilder builder = QueryBuilder.CreateInstance(hasWhere);
            builder.AddConditionWithAnd<string>(database, command, "PE.NR_PEDIDO", QueryBuilder.QueryBuilderOperator.Equals, qFilters.NrPedido, true, true);

            return builder;
        }

        #endregion

        #region -- Parameter Methods --

        public override void AddCreateParameters(QDatabase database, DbCommand command, Pedido entity)
        {
            AddAllParameters(database, command, entity);
        }

        public override void AddUpdateParameters(QDatabase database, DbCommand command, Pedido entity)
        {
            AddAllParameters(database, command, entity);
        }

        private void AddAllParameters(QDatabase database, DbCommand command, Pedido entity)
        {
            DBMapperHelper.AddInParameter<Int64>(database, command, "NrPedido", entity.NrPedido);
            DBMapperHelper.AddInParameter<String>(database, command, "NrControle", entity.NrControle);
            DBMapperHelper.AddInParameter<String>(database, command, "CdMarca", entity.CdMarca);
            DBMapperHelper.AddInParameter<String>(database, command, "CdVendedor", entity.CdVendedor);
            DBMapperHelper.AddInParameter<Int64>(database, command, "CdClienteEmissor", entity.CdClienteEmissor);
            DBMapperHelper.AddInParameter<Int64?>(database, command, "CdClienteRecebedor", entity.CdClienteRecebedor);
            DBMapperHelper.AddInParameter<String>(database, command, "NrPedidoCliente", entity.NrPedidoCliente);
            DBMapperHelper.AddInParameter<String>(database, command, "CdTipoPedido", entity.CdTipoPedido);
            DBMapperHelper.AddInParameter<DateTime?>(database, command, "DtEntrada", entity.DtEntrada);
            DBMapperHelper.AddInParameter<DateTime?>(database, command, "DtProgramacao", entity.DtProgramacao);
            DBMapperHelper.AddInParameter<String>(database, command, "CdFrete", entity.CdFrete);
            DBMapperHelper.AddInParameter<String>(database, command, "CdPrazo", entity.CdPrazo);
            DBMapperHelper.AddInParameter<String>(database, command, "CdTabelaPreco", entity.CdTabelaPreco);
            DBMapperHelper.AddInParameter<Int16?>(database, command, "AaTabelaPreco", entity.AaTabelaPreco);
            DBMapperHelper.AddInParameter<String>(database, command, "CdBloqueio", entity.CdBloqueio);
            DBMapperHelper.AddInParameter<String>(database, command, "DsObservacao1", entity.DsObservacao1);
            DBMapperHelper.AddInParameter<String>(database, command, "DsObservacao2", entity.DsObservacao2);
            DBMapperHelper.AddInParameter<String>(database, command, "Status", entity.Status);
            DBMapperHelper.AddInParameter<Decimal?>(database, command, "VlPedido", entity.VlPedido);
            DBMapperHelper.AddInParameter<Decimal?>(database, command, "VlFrete", entity.VlFrete);
            DBMapperHelper.AddInParameter<Decimal?>(database, command, "VlDespesasAcessorias", entity.VlDespesasAcessorias);
            DBMapperHelper.AddInParameter<Decimal?>(database, command, "VlMercadoria", entity.VlMercadoria);
            DBMapperHelper.AddInParameter<Char?>(database, command, "LgCancelado", entity.LgCancelado);
            DBMapperHelper.AddInParameter<Char?>(database, command, "LgProcessado", entity.LgProcessado);
            DBMapperHelper.AddInParameter<Int32?>(database, command, "QtPecas", entity.QtPecas);
            DBMapperHelper.AddInParameter<DateTime?>(database, command, "DtCarteiraBoa", entity.DtCarteiraBoa);
            DBMapperHelper.AddInParameter<DateTime?>(database, command, "DtComprometimento", entity.DtComprometimento);
            DBMapperHelper.AddInParameter<Char?>(database, command, "LgAtraso", entity.LgAtraso);
            DBMapperHelper.AddInParameter<Int16?>(database, command, "DdAtraso", entity.DdAtraso);
            DBMapperHelper.AddInParameter<DateTime?>(database, command, "DtPrevisao", entity.DtPrevisao);
            DBMapperHelper.AddInParameter<Decimal?>(database, command, "VlIpi", entity.VlIpi);
            DBMapperHelper.AddInParameter<String>(database, command, "CdOrganizacao", entity.CdOrganizacao);
            DBMapperHelper.AddInParameter<DateTime?>(database, command, "DtInicial", entity.DtInicial);

        }

        public override void AddDeleteParameters(QDatabase database, DbCommand command, Pedido entity)
        {
            DBMapperHelper.AddInParameter<Int64>(database, command, "NrPedido", entity.NrPedido);

        }

        public override void AddGetByIdParameters(QDatabase database, DbCommand command, Pedido entity)
        {
            DBMapperHelper.AddInParameter<Int64>(database, command, "NrPedido", entity.NrPedido);

        }

        #endregion
    }
}
