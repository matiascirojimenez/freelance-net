using QLibs.Common.DTO.Queries;
using QLibs.DataAccess;
using QLibs.DataAccess.DBMappers;
using QLibs.DataAccess.Utils;
using RocaAPI.AdminEcomm.Common.Entities;
using System;
using System.Data;
using System.Data.Common;

namespace RocaAPI.AdminEcomm.DataAccess.DBMappers
{
    /// <summary>
    /// Represents a mapper of the class <see cref="PedidoAgrupadorZN"/>.
    /// </summary>
    public class PedidoAgrupadorZNDBMapper : BaseDBMapper<PedidoAgrupadorZN, BaseQueryFilters>
    {
        #region -- Build Methods --

        public override PedidoAgrupadorZN BuildEntity(IDataReader reader)
        {
            PedidoAgrupadorZN entity = new PedidoAgrupadorZN();

            entity.NrPedido = DBMapperHelper.GetColumnStringValue(reader, "nr_pedido");
            entity.KunnrZn = DBMapperHelper.GetColumnStringValue(reader, "kunnr_zn");


            return entity;
        }

        #endregion

        #region -- QueryFilters --

        public override QueryBuilder GetWhereClause(QDatabase database, DbCommand command, bool hasWhere, BaseQueryFilters qFilters)
        {
            QueryBuilder builder = QueryBuilder.CreateInstance(hasWhere);

            return builder;
        }

        #endregion

        #region -- Parameter Methods --

        public override void AddCreateParameters(QDatabase database, DbCommand command, PedidoAgrupadorZN entity)
        {
            AddAllParameters(database, command, entity);
        }

        public override void AddUpdateParameters(QDatabase database, DbCommand command, PedidoAgrupadorZN entity)
        {
            AddAllParameters(database, command, entity);
        }

        private void AddAllParameters(QDatabase database, DbCommand command, PedidoAgrupadorZN entity)
        {
            DBMapperHelper.AddInParameter<String>(database, command, "NrPedido", entity.NrPedido);
            DBMapperHelper.AddInParameter<String>(database, command, "KunnrZn", entity.KunnrZn);

        }

        public override void AddDeleteParameters(QDatabase database, DbCommand command, PedidoAgrupadorZN entity)
        {
            DBMapperHelper.AddInParameter<String>(database, command, "NrPedido", entity.NrPedido);
            DBMapperHelper.AddInParameter<String>(database, command, "KunnrZn", entity.KunnrZn);

        }

        public override void AddGetByIdParameters(QDatabase database, DbCommand command, PedidoAgrupadorZN entity)
        {
            DBMapperHelper.AddInParameter<String>(database, command, "NrPedido", entity.NrPedido);
            DBMapperHelper.AddInParameter<String>(database, command, "KunnrZn", entity.KunnrZn);

        }

        #endregion
    }
}
