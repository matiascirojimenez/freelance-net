using QLibs.Common.DTO.Queries;
using QLibs.DataAccess;
using QLibs.DataAccess.DBMappers;
using QLibs.DataAccess.Utils;
using RocaAPI.AdminEcomm.Common.Entities;
using System;
using System.Data;
using System.Data.Common;

namespace RocaAPI.AdminEcomm.DataAccess.DBMappers
{
    /// <summary>
    /// Represents a mapper of the class <see cref="PrecoOutletPj"/>.
    /// </summary>
    public class PrecoOutletPjDBMapper : BaseDBMapper<PrecoOutletPj, BaseQueryFilters>
    {
        #region -- Build Methods --

        public override PrecoOutletPj BuildEntity(IDataReader reader)
        {
            PrecoOutletPj entity = new PrecoOutletPj();

            entity.Codtab = DBMapperHelper.GetColumnDecimalValue(reader, "CODTAB");
            entity.Werks = DBMapperHelper.GetColumnStringValue(reader, "WERKS");
            entity.Regio = DBMapperHelper.GetColumnStringValue(reader, "REGIO");
            entity.Matnr = DBMapperHelper.GetColumnStringValue(reader, "MATNR");
            entity.Preco = DBMapperHelper.GetColumnNulleableDoubleValue(reader, "PRECO");
            entity.Prbas = DBMapperHelper.GetColumnNulleableDoubleValue(reader, "PRBAS");
            entity.Vlicms = DBMapperHelper.GetColumnNulleableDoubleValue(reader, "VLICMS");
            entity.Vlipi = DBMapperHelper.GetColumnNulleableDoubleValue(reader, "VLIPI");
            entity.Vlicst = DBMapperHelper.GetColumnNulleableDoubleValue(reader, "VLICST");
            entity.Vlfecpst = DBMapperHelper.GetColumnNulleableDoubleValue(reader, "VLFECPST");
            entity.Vlpis = DBMapperHelper.GetColumnNulleableDoubleValue(reader, "VLPIS");
            entity.Vlcof = DBMapperHelper.GetColumnNulleableDoubleValue(reader, "VLCOF");
            entity.Vlicmspart = DBMapperHelper.GetColumnNulleableDoubleValue(reader, "VLICMSPART");
            entity.Vlfecppart = DBMapperHelper.GetColumnNulleableDoubleValue(reader, "VLFECPPART");
            entity.Auditdate = DBMapperHelper.GetColumnNulleableDateTimeValue(reader, "auditdate");


            return entity;
        }

        #endregion

        #region -- QueryFilters --

        public override QueryBuilder GetWhereClause(QDatabase database, DbCommand command, bool hasWhere, BaseQueryFilters qFilters)
        {
            QueryBuilder builder = QueryBuilder.CreateInstance(hasWhere);

            return builder;
        }

        #endregion

        #region -- Parameter Methods --

        public override void AddCreateParameters(QDatabase database, DbCommand command, PrecoOutletPj entity)
        {
            AddAllParameters(database, command, entity);
        }

        public override void AddUpdateParameters(QDatabase database, DbCommand command, PrecoOutletPj entity)
        {
            AddAllParameters(database, command, entity);
        }

        private void AddAllParameters(QDatabase database, DbCommand command, PrecoOutletPj entity)
        {
            DBMapperHelper.AddInParameter<Decimal?>(database, command, "Codtab", entity.Codtab);
            DBMapperHelper.AddInParameter<String>(database, command, "Werks", entity.Werks);
            DBMapperHelper.AddInParameter<String>(database, command, "Regio", entity.Regio);
            DBMapperHelper.AddInParameter<String>(database, command, "Matnr", entity.Matnr);
            DBMapperHelper.AddInParameter<Double?>(database, command, "Preco", entity.Preco);
            DBMapperHelper.AddInParameter<Double?>(database, command, "Prbas", entity.Prbas);
            DBMapperHelper.AddInParameter<Double?>(database, command, "Vlicms", entity.Vlicms);
            DBMapperHelper.AddInParameter<Double?>(database, command, "Vlipi", entity.Vlipi);
            DBMapperHelper.AddInParameter<Double?>(database, command, "Vlicst", entity.Vlicst);
            DBMapperHelper.AddInParameter<Double?>(database, command, "Vlfecpst", entity.Vlfecpst);
            DBMapperHelper.AddInParameter<Double?>(database, command, "Vlpis", entity.Vlpis);
            DBMapperHelper.AddInParameter<Double?>(database, command, "Vlcof", entity.Vlcof);
            DBMapperHelper.AddInParameter<Double?>(database, command, "Vlicmspart", entity.Vlicmspart);
            DBMapperHelper.AddInParameter<Double?>(database, command, "Vlfecppart", entity.Vlfecppart);
            DBMapperHelper.AddInParameter<DateTime?>(database, command, "Auditdate", entity.Auditdate);

        }

        public override void AddDeleteParameters(QDatabase database, DbCommand command, PrecoOutletPj entity)
        {
            DBMapperHelper.AddInParameter<Decimal>(database, command, "Codtab", entity.Codtab);
            DBMapperHelper.AddInParameter<String>(database, command, "Werks", entity.Werks);
            DBMapperHelper.AddInParameter<String>(database, command, "Regio", entity.Regio);
            DBMapperHelper.AddInParameter<String>(database, command, "Matnr", entity.Matnr);

        }

        public override void AddGetByIdParameters(QDatabase database, DbCommand command, PrecoOutletPj entity)
        {
            DBMapperHelper.AddInParameter<Decimal>(database, command, "Codtab", entity.Codtab);
            DBMapperHelper.AddInParameter<String>(database, command, "Werks", entity.Werks);
            DBMapperHelper.AddInParameter<String>(database, command, "Regio", entity.Regio);
            DBMapperHelper.AddInParameter<String>(database, command, "Matnr", entity.Matnr);

        }

        #endregion
    }
}
