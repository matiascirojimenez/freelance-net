using QLibs.DataAccess;
using QLibs.DataAccess.DBMappers;
using QLibs.DataAccess.Utils;
using RocaAPI.AdminEcomm.Common.Entities;
using RocaAPI.AdminEcomm.Common.Queries;
using System;
using System.Data;
using System.Data.Common;

namespace RocaAPI.AdminEcomm.DataAccess.DBMappers
{
    /// <summary>
    /// Represents a mapper of the class <see cref="Cliente"/>.
    /// </summary>
    public class ClienteDBMapper : BaseDBMapper<Cliente, ClienteQueryFilters>
    {
        #region -- Build Methods --

        public override Cliente BuildEntity(IDataReader reader)
        {
            Cliente entity = new Cliente();
            
            entity.CdCliente = DBMapperHelper.GetColumnInt32Value(reader, "CD_CLIENTE");
            entity.NrCnpj = DBMapperHelper.GetColumnStringValue(reader, "NR_CNPJ");
            entity.NrCpf = DBMapperHelper.GetColumnStringValue(reader, "NR_CPF");
            entity.NmEstabelecimento = DBMapperHelper.GetColumnStringValue(reader, "NM_ESTABELECIMENTO");
            entity.NmReduzido = DBMapperHelper.GetColumnStringValue(reader, "NM_REDUZIDO");
            entity.CdRamo = DBMapperHelper.GetColumnStringValue(reader, "CD_RAMO");
            entity.NrIe = DBMapperHelper.GetColumnStringValue(reader, "NR_IE");
            entity.DsEndereco = DBMapperHelper.GetColumnStringValue(reader, "DS_ENDERECO");
            entity.DsNumero = DBMapperHelper.GetColumnStringValue(reader, "DS_NUMERO");
            entity.DsComplemento = DBMapperHelper.GetColumnStringValue(reader, "DS_COMPLEMENTO");
            entity.DsBairro = DBMapperHelper.GetColumnStringValue(reader, "DS_BAIRRO");
            entity.NmCidade = DBMapperHelper.GetColumnStringValue(reader, "NM_CIDADE");
            entity.SgUf = DBMapperHelper.GetColumnStringValue(reader, "SG_UF");
            entity.NrCep = DBMapperHelper.GetColumnStringValue(reader, "NR_CEP");
            entity.NrFone = DBMapperHelper.GetColumnStringValue(reader, "NR_FONE");
            entity.NrFax = DBMapperHelper.GetColumnStringValue(reader, "NR_FAX");
            entity.DsEmail = DBMapperHelper.GetColumnStringValue(reader, "DS_EMAIL");
            entity.CdTipoCliente = DBMapperHelper.GetColumnInt32Value(reader, "CD_TIPO_CLIENTE");
            entity.CdSituacao = DBMapperHelper.GetColumnStringValue(reader, "CD_SITUACAO");
            entity.CdClientePagador = DBMapperHelper.GetColumnNulleableInt32Value(reader, "CD_CLIENTE_PAGADOR");
            entity.NrSuframa = DBMapperHelper.GetColumnStringValue(reader, "NR_SUFRAMA");
            entity.LgContribuinte = DBMapperHelper.GetColumnNulleableCharValue(reader, "LG_CONTRIBUINTE");
            entity.CdErv = DBMapperHelper.GetColumnStringValue(reader, "CD_ERV");
            entity.CdClassificacao = DBMapperHelper.GetColumnStringValue(reader, "CD_CLASSIFICACAO");
            entity.CdClassificacaoContabil = DBMapperHelper.GetColumnStringValue(reader, "CD_CLASSIFICACAO_CONTABIL");
            entity.CdClassificacaoFiscal = DBMapperHelper.GetColumnNulleableCharValue(reader, "CD_CLASSIFICACAO_FISCAL");
            entity.DtCadastro = DBMapperHelper.GetColumnDateTimeValue(reader, "DT_CADASTRO");
            entity.NrRappel = DBMapperHelper.GetColumnNulleableInt32Value(reader, "NR_RAPPEL");
            entity.DsEnderecoCobranca = DBMapperHelper.GetColumnStringValue(reader, "DS_ENDERECO_COBRANCA");
            entity.DsBairroCobranca = DBMapperHelper.GetColumnStringValue(reader, "DS_BAIRRO_COBRANCA");
            entity.NmCidadeCobranca = DBMapperHelper.GetColumnStringValue(reader, "NM_CIDADE_COBRANCA");
            entity.SgUfCobranca = DBMapperHelper.GetColumnStringValue(reader, "SG_UF_COBRANCA");
            entity.NrCepCobranca = DBMapperHelper.GetColumnStringValue(reader, "NR_CEP_COBRANCA");
            entity.NrFoneCobranca = DBMapperHelper.GetColumnStringValue(reader, "NR_FONE_COBRANCA");
            entity.DsEmailNfe = DBMapperHelper.GetColumnStringValue(reader, "DS_EMAIL_NFE");
            entity.CdCategoriaCfop = DBMapperHelper.GetColumnStringValue(reader, "CD_CATEGORIA_CFOP");
            entity.DsEmailDanfe = DBMapperHelper.GetColumnStringValue(reader, "DS_EMAIL_DANFE");
            entity.MargemCli = DBMapperHelper.GetColumnStringValue(reader, "MARGEM_CLI");
            entity.MargemCanal = DBMapperHelper.GetColumnStringValue(reader, "MARGEM_CANAL");
            entity.CdSegmentacao = DBMapperHelper.GetColumnStringValue(reader, "CD_SEGMENTACAO");
            entity.Rede = DBMapperHelper.GetColumnStringValue(reader, "REDE");
            entity.FlIsnew = DBMapperHelper.GetColumnBooleanValue(reader, "FL_ISNEW");
            entity.FlIntegrated = DBMapperHelper.GetColumnBooleanValue(reader, "FL_INTEGRATED");
            entity.Ie = DBMapperHelper.GetColumnStringValue(reader, "IE");


            return entity;
        }

        #endregion

        #region -- QueryFilters --

        public override QueryBuilder GetWhereClause(QDatabase database, DbCommand command, bool hasWhere, ClienteQueryFilters qFilters)
        {
            QueryBuilder builder = QueryBuilder.CreateInstance(hasWhere);
			builder.AddConditionWithAnd<string>(database, command, "CL.CD_CLIENTE", QueryBuilder.QueryBuilderOperator.Equals, qFilters.CdCliente, true, true);

            return builder;
        }

        #endregion

        #region -- Parameter Methods --

        public override void AddCreateParameters(QDatabase database, DbCommand command, Cliente entity)
        {
            AddAllParameters(database, command, entity); 
        }

        public override void AddUpdateParameters(QDatabase database, DbCommand command, Cliente entity)
        {
            AddAllParameters(database, command, entity); 
        }

	    private void AddAllParameters(QDatabase database, DbCommand command, Cliente entity)
	    {
            DBMapperHelper.AddInParameter<Int32>(database, command, "CdCliente", entity.CdCliente);
            DBMapperHelper.AddInParameter<String>(database, command, "NrCnpj", entity.NrCnpj);
            DBMapperHelper.AddInParameter<String>(database, command, "NrCpf", entity.NrCpf);
            DBMapperHelper.AddInParameter<String>(database, command, "NmEstabelecimento", entity.NmEstabelecimento);
            DBMapperHelper.AddInParameter<String>(database, command, "NmReduzido", entity.NmReduzido);
            DBMapperHelper.AddInParameter<String>(database, command, "CdRamo", entity.CdRamo);
            DBMapperHelper.AddInParameter<String>(database, command, "NrIe", entity.NrIe);
            DBMapperHelper.AddInParameter<String>(database, command, "DsEndereco", entity.DsEndereco);
            DBMapperHelper.AddInParameter<String>(database, command, "DsNumero", entity.DsNumero);
            DBMapperHelper.AddInParameter<String>(database, command, "DsComplemento", entity.DsComplemento);
            DBMapperHelper.AddInParameter<String>(database, command, "DsBairro", entity.DsBairro);
            DBMapperHelper.AddInParameter<String>(database, command, "NmCidade", entity.NmCidade);
            DBMapperHelper.AddInParameter<String>(database, command, "SgUf", entity.SgUf);
            DBMapperHelper.AddInParameter<String>(database, command, "NrCep", entity.NrCep);
            DBMapperHelper.AddInParameter<String>(database, command, "NrFone", entity.NrFone);
            DBMapperHelper.AddInParameter<String>(database, command, "NrFax", entity.NrFax);
            DBMapperHelper.AddInParameter<String>(database, command, "DsEmail", entity.DsEmail);
            DBMapperHelper.AddInParameter<Int32>(database, command, "CdTipoCliente", entity.CdTipoCliente);
            DBMapperHelper.AddInParameter<String>(database, command, "CdSituacao", entity.CdSituacao);
            DBMapperHelper.AddInParameter<Int32?>(database, command, "CdClientePagador", entity.CdClientePagador);
            DBMapperHelper.AddInParameter<String>(database, command, "NrSuframa", entity.NrSuframa);
            DBMapperHelper.AddInParameter<Char?>(database, command, "LgContribuinte", entity.LgContribuinte);
            DBMapperHelper.AddInParameter<String>(database, command, "CdErv", entity.CdErv);
            DBMapperHelper.AddInParameter<String>(database, command, "CdClassificacao", entity.CdClassificacao);
            DBMapperHelper.AddInParameter<String>(database, command, "CdClassificacaoContabil", entity.CdClassificacaoContabil);
            DBMapperHelper.AddInParameter<Char?>(database, command, "CdClassificacaoFiscal", entity.CdClassificacaoFiscal);
            DBMapperHelper.AddInParameter<DateTime>(database, command, "DtCadastro", entity.DtCadastro);
            DBMapperHelper.AddInParameter<Int32?>(database, command, "NrRappel", entity.NrRappel);
            DBMapperHelper.AddInParameter<String>(database, command, "DsEnderecoCobranca", entity.DsEnderecoCobranca);
            DBMapperHelper.AddInParameter<String>(database, command, "DsBairroCobranca", entity.DsBairroCobranca);
            DBMapperHelper.AddInParameter<String>(database, command, "NmCidadeCobranca", entity.NmCidadeCobranca);
            DBMapperHelper.AddInParameter<String>(database, command, "SgUfCobranca", entity.SgUfCobranca);
            DBMapperHelper.AddInParameter<String>(database, command, "NrCepCobranca", entity.NrCepCobranca);
            DBMapperHelper.AddInParameter<String>(database, command, "NrFoneCobranca", entity.NrFoneCobranca);
            DBMapperHelper.AddInParameter<String>(database, command, "DsEmailNfe", entity.DsEmailNfe);
            DBMapperHelper.AddInParameter<String>(database, command, "CdCategoriaCfop", entity.CdCategoriaCfop);
            DBMapperHelper.AddInParameter<String>(database, command, "DsEmailDanfe", entity.DsEmailDanfe);
            DBMapperHelper.AddInParameter<String>(database, command, "MargemCli", entity.MargemCli);
            DBMapperHelper.AddInParameter<String>(database, command, "MargemCanal", entity.MargemCanal);
            DBMapperHelper.AddInParameter<String>(database, command, "CdSegmentacao", entity.CdSegmentacao);
            DBMapperHelper.AddInParameter<String>(database, command, "Rede", entity.Rede);
            DBMapperHelper.AddInParameter<Boolean>(database, command, "FlIsnew", entity.FlIsnew);
            DBMapperHelper.AddInParameter<Boolean>(database, command, "FlIntegrated", entity.FlIntegrated);
            DBMapperHelper.AddInParameter<String>(database, command, "Ie", entity.Ie);

		}

        public override void AddDeleteParameters(QDatabase database, DbCommand command, Cliente entity)
        {
            DBMapperHelper.AddInParameter<Int32>(database, command, "CdCliente", entity.CdCliente);

		}

        public override void AddGetByIdParameters(QDatabase database, DbCommand command, Cliente entity)
        {
            DBMapperHelper.AddInParameter<Int32>(database, command, "CdCliente", entity.CdCliente);
        
		}

        #endregion
    }
}
