using QLibs.DataAccess;
using QLibs.DataAccess.DBMappers;
using QLibs.DataAccess.Utils;
using RocaAPI.AdminEcomm.Common.Entities;
using RocaAPI.AdminEcomm.Common.Queries;
using System;
using System.Data;
using System.Data.Common;

namespace RocaAPI.AdminEcomm.DataAccess.DBMappers
{
    /// <summary>
    /// Represents a mapper of the class <see cref="PedidoInterno"/>.
    /// </summary>
    public class PedidoInternoDBMapper : BaseDBMapper<PedidoInterno, PedidoInternoQueryFilters>
    {
        #region -- Build Methods --

        public override PedidoInterno BuildEntity(IDataReader reader)
        {
            PedidoInterno entity = new PedidoInterno();
            
            entity.NrControle = DBMapperHelper.GetColumnGuidValue(reader, "NR_CONTROLE");
            entity.CdMarca = DBMapperHelper.GetColumnStringValue(reader, "CD_MARCA");
            entity.CdVendedor = DBMapperHelper.GetColumnStringValue(reader, "CD_VENDEDOR");
            entity.CdClienteEmissor = DBMapperHelper.GetColumnNulleableInt64Value(reader, "CD_CLIENTE_EMISSOR");
            entity.CdClienteRecebedor = DBMapperHelper.GetColumnNulleableInt64Value(reader, "CD_CLIENTE_RECEBEDOR");
            entity.NrPedidoCliente = DBMapperHelper.GetColumnStringValue(reader, "NR_PEDIDO_CLIENTE");
            entity.NrPedidoSap = DBMapperHelper.GetColumnStringValue(reader, "NR_PEDIDO_SAP");
            entity.CdTipoPedido = DBMapperHelper.GetColumnStringValue(reader, "CD_TIPO_PEDIDO");
            entity.DtEntrada = DBMapperHelper.GetColumnDateTimeValue(reader, "DT_ENTRADA");
            entity.DtProgramacao = DBMapperHelper.GetColumnDateTimeValue(reader, "DT_PROGRAMACAO");
            entity.CdFrete = DBMapperHelper.GetColumnStringValue(reader, "CD_FRETE");
            entity.CdPrazo = DBMapperHelper.GetColumnStringValue(reader, "CD_PRAZO");
            entity.CdTabelaPreco = DBMapperHelper.GetColumnStringValue(reader, "CD_TABELA_PRECO");
            entity.AaTabelaPreco = DBMapperHelper.GetColumnNulleableInt16Value(reader, "AA_TABELA_PRECO");
            entity.CdBloqueio = DBMapperHelper.GetColumnStringValue(reader, "CD_BLOQUEIO");
            entity.DsObservacao1 = DBMapperHelper.GetColumnStringValue(reader, "DS_OBSERVACAO1");
            entity.DsObservacao2 = DBMapperHelper.GetColumnStringValue(reader, "DS_OBSERVACAO2");
            entity.Status = DBMapperHelper.GetColumnStringValue(reader, "STATUS");
            entity.VlPedido = DBMapperHelper.GetColumnDecimalValue(reader, "VL_PEDIDO");
            entity.VlFrete = DBMapperHelper.GetColumnDecimalValue(reader, "VL_FRETE");
            entity.VlDesconto = DBMapperHelper.GetColumnNulleableDecimalValue(reader, "VL_DESCONTO");
            entity.VlDespesasAcessorias = DBMapperHelper.GetColumnDecimalValue(reader, "VL_DESPESAS_ACESSORIAS");
            entity.VlMercadoria = DBMapperHelper.GetColumnNulleableDecimalValue(reader, "VL_MERCADORIA");
            entity.NrBoleto = DBMapperHelper.GetColumnStringValue(reader, "NR_BOLETO");
            entity.LgCancelado = DBMapperHelper.GetColumnStringValue(reader, "LG_CANCELADO");
            entity.LgProcessado = DBMapperHelper.GetColumnStringValue(reader, "LG_PROCESSADO");
            entity.QtPecas = DBMapperHelper.GetColumnNulleableInt32Value(reader, "QT_PECAS");
            entity.DtCarteiraBoa = DBMapperHelper.GetColumnNulleableDateTimeValue(reader, "DT_CARTEIRA_BOA");
            entity.DtComprometimento = DBMapperHelper.GetColumnNulleableDateTimeValue(reader, "DT_COMPROMETIMENTO");
            entity.LgAtraso = DBMapperHelper.GetColumnNulleableCharValue(reader, "LG_ATRASO");
            entity.DdAtraso = DBMapperHelper.GetColumnNulleableInt16Value(reader, "DD_ATRASO");
            entity.DtPrevisao = DBMapperHelper.GetColumnNulleableDateTimeValue(reader, "DT_PREVISAO");
            entity.VlIpi = DBMapperHelper.GetColumnNulleableDecimalValue(reader, "VL_IPI");
            entity.CdOrganizacao = DBMapperHelper.GetColumnStringValue(reader, "CD_ORGANIZACAO");
            entity.DtInicial = DBMapperHelper.GetColumnNulleableDateTimeValue(reader, "DT_INICIAL");
            entity.CdRastreio = DBMapperHelper.GetColumnStringValue(reader, "CD_RASTREIO");
            entity.CompanyId = DBMapperHelper.GetColumnInt32Value(reader, "Company_ID");
            entity.CdCliente = DBMapperHelper.GetColumnInt32Value(reader, "CD_CLIENTE");
            entity.Auditdate = DBMapperHelper.GetColumnNulleableDateTimeValue(reader, "auditdate");
            entity.NrPedidoSapServico = DBMapperHelper.GetColumnStringValue(reader, "NR_PEDIDO_SAP_SERVICO");

            /// JOIN PROPERTIES
            entity.ClienteNrCnpj = DBMapperHelper.GetColumnStringValue(reader, "ClienteNrCnpj");
            entity.ClienteNrCpf = DBMapperHelper.GetColumnStringValue(reader, "ClienteNrCpf");
            entity.ClienteNmEstabelecimento = DBMapperHelper.GetColumnStringValue(reader, "ClienteNmEstabelecimento");

            entity.EmissorNrCnpj = DBMapperHelper.GetColumnStringValue(reader, "EmissorNrCnpj");
            entity.EmissorNrCpf = DBMapperHelper.GetColumnStringValue(reader, "EmissorNrCpf");
            entity.EmissorNmEstabelecimento = DBMapperHelper.GetColumnStringValue(reader, "EmissorNmEstabelecimento");

            entity.RecebedorNrCnpj = DBMapperHelper.GetColumnStringValue(reader, "RecebedorNrCnpj");
            entity.RecebedorNrCpf = DBMapperHelper.GetColumnStringValue(reader, "RecebedorNrCpf");
            entity.RecebedorNmEstabelecimento = DBMapperHelper.GetColumnStringValue(reader, "RecebedorNmEstabelecimento");

            entity.DsEndereco = DBMapperHelper.GetColumnStringValue(reader, "DS_ENDERECO");
            entity.DsNumero = DBMapperHelper.GetColumnStringValue(reader, "DS_NUMERO");
            entity.DsComplemento = DBMapperHelper.GetColumnStringValue(reader, "DS_COMPLEMENTO");
            entity.DsBairro = DBMapperHelper.GetColumnStringValue(reader, "DS_BAIRRO");
            entity.NmCidade = DBMapperHelper.GetColumnStringValue(reader, "NM_CIDADE");
            entity.NrCep = DBMapperHelper.GetColumnStringValue(reader, "NR_CEP");
            entity.NrSuframa = DBMapperHelper.GetColumnStringValue(reader, "NR_SUFRAMA");

            return entity;
        }

        #endregion

        #region -- QueryFilters --

        public override QueryBuilder GetWhereClause(QDatabase database, DbCommand command, bool hasWhere, PedidoInternoQueryFilters qFilters)
        {
            QueryBuilder builder = QueryBuilder.CreateInstance(hasWhere);

            var subQuery = QueryBuilder.CreateSubQueryInstance(builder);
            subQuery.AddConditionWithOr<String>(database, command, "PE.NR_PEDIDO_CLIENTE", QueryBuilder.QueryBuilderOperator.LikeFull, qFilters.FindValue);
            subQuery.AddConditionWithOr<String>(database, command, "PE.NR_PEDIDO_SAP", QueryBuilder.QueryBuilderOperator.LikeFull, qFilters.FindValue);
            subQuery.AddConditionWithOr<String>(database, command, "PE.CD_TIPO_PEDIDO", QueryBuilder.QueryBuilderOperator.LikeFull, qFilters.FindValue);
            subQuery.AddConditionWithOr<String>(database, command, "PE.DS_OBSERVACAO1", QueryBuilder.QueryBuilderOperator.LikeFull, qFilters.FindValue);
            subQuery.AddConditionWithOr<String>(database, command, "PE.DS_OBSERVACAO2", QueryBuilder.QueryBuilderOperator.LikeFull, qFilters.FindValue);
            builder.AddSubQuery(QueryBuilder.QueryBuilderCondition.And, subQuery);

			builder.AddConditionWithAnd<Int64?>(database, command, "PE.CD_CLIENTE_EMISSOR", QueryBuilder.QueryBuilderOperator.Equals, qFilters.CdClienteEmissor, true, true);
builder.AddConditionWithAnd<Int64?>(database, command, "PE.CD_CLIENTE_RECEBEDOR", QueryBuilder.QueryBuilderOperator.Equals, qFilters.CdClienteRecebedor, true, true);
builder.AddConditionWithAnd<Int32?>(database, command, "PE.CD_CLIENTE", QueryBuilder.QueryBuilderOperator.Equals, qFilters.CdCliente, true, true);
            builder.AddConditionWithAnd<String>(database, command, "PE.NR_PEDIDO_CLIENTE", QueryBuilder.QueryBuilderOperator.Equals, qFilters.NrPedidoCliente, true, true);
            builder.AddConditionWithAnd<String>(database, command, "PE.NR_PEDIDO_SAP", QueryBuilder.QueryBuilderOperator.Equals, qFilters.NrPedidoSap, true, true);
            builder.AddConditionWithAnd<DateTime?>(database, command, "PE.DT_ENTRADA", QueryBuilder.QueryBuilderOperator.GreaterOrEqualsThan, qFilters.DtEntradaFrom, true, true);
            builder.AddConditionWithAnd<DateTime?>(database, command, "PE.DT_ENTRADA", QueryBuilder.QueryBuilderOperator.LowerOrEqualsThan, qFilters.DtEntradaTo, true, true);

            return builder;
        }

        #endregion

        #region -- Parameter Methods --

        public override void AddCreateParameters(QDatabase database, DbCommand command, PedidoInterno entity)
        {
            AddAllParameters(database, command, entity); 
        }

        public override void AddUpdateParameters(QDatabase database, DbCommand command, PedidoInterno entity)
        {
            AddAllParameters(database, command, entity); 
        }

	    private void AddAllParameters(QDatabase database, DbCommand command, PedidoInterno entity)
	    {
            DBMapperHelper.AddInParameter<Guid>(database, command, "NrControle", entity.NrControle);
            DBMapperHelper.AddInParameter<String>(database, command, "CdMarca", entity.CdMarca);
            DBMapperHelper.AddInParameter<String>(database, command, "CdVendedor", entity.CdVendedor);
            DBMapperHelper.AddInParameter<Int64?>(database, command, "CdClienteEmissor", entity.CdClienteEmissor);
            DBMapperHelper.AddInParameter<Int64?>(database, command, "CdClienteRecebedor", entity.CdClienteRecebedor);
            DBMapperHelper.AddInParameter<String>(database, command, "NrPedidoCliente", entity.NrPedidoCliente);
            DBMapperHelper.AddInParameter<String>(database, command, "NrPedidoSap", entity.NrPedidoSap);
            DBMapperHelper.AddInParameter<String>(database, command, "CdTipoPedido", entity.CdTipoPedido);
            DBMapperHelper.AddInParameter<DateTime>(database, command, "DtEntrada", entity.DtEntrada);
            DBMapperHelper.AddInParameter<DateTime>(database, command, "DtProgramacao", entity.DtProgramacao);
            DBMapperHelper.AddInParameter<String>(database, command, "CdFrete", entity.CdFrete);
            DBMapperHelper.AddInParameter<String>(database, command, "CdPrazo", entity.CdPrazo);
            DBMapperHelper.AddInParameter<String>(database, command, "CdTabelaPreco", entity.CdTabelaPreco);
            DBMapperHelper.AddInParameter<Int16?>(database, command, "AaTabelaPreco", entity.AaTabelaPreco);
            DBMapperHelper.AddInParameter<String>(database, command, "CdBloqueio", entity.CdBloqueio);
            DBMapperHelper.AddInParameter<String>(database, command, "DsObservacao1", entity.DsObservacao1);
            DBMapperHelper.AddInParameter<String>(database, command, "DsObservacao2", entity.DsObservacao2);
            DBMapperHelper.AddInParameter<String>(database, command, "Status", entity.Status);
            DBMapperHelper.AddInParameter<Decimal>(database, command, "VlPedido", entity.VlPedido);
            DBMapperHelper.AddInParameter<Decimal>(database, command, "VlFrete", entity.VlFrete);
            DBMapperHelper.AddInParameter<Decimal?>(database, command, "VlDesconto", entity.VlDesconto);
            DBMapperHelper.AddInParameter<Decimal>(database, command, "VlDespesasAcessorias", entity.VlDespesasAcessorias);
            DBMapperHelper.AddInParameter<Decimal?>(database, command, "VlMercadoria", entity.VlMercadoria);
            DBMapperHelper.AddInParameter<String>(database, command, "NrBoleto", entity.NrBoleto);
            DBMapperHelper.AddInParameter<String>(database, command, "LgCancelado", entity.LgCancelado);
            DBMapperHelper.AddInParameter<String>(database, command, "LgProcessado", entity.LgProcessado);
            DBMapperHelper.AddInParameter<Int32?>(database, command, "QtPecas", entity.QtPecas);
            DBMapperHelper.AddInParameter<DateTime?>(database, command, "DtCarteiraBoa", entity.DtCarteiraBoa);
            DBMapperHelper.AddInParameter<DateTime?>(database, command, "DtComprometimento", entity.DtComprometimento);
            DBMapperHelper.AddInParameter<Char?>(database, command, "LgAtraso", entity.LgAtraso);
            DBMapperHelper.AddInParameter<Int16?>(database, command, "DdAtraso", entity.DdAtraso);
            DBMapperHelper.AddInParameter<DateTime?>(database, command, "DtPrevisao", entity.DtPrevisao);
            DBMapperHelper.AddInParameter<Decimal?>(database, command, "VlIpi", entity.VlIpi);
            DBMapperHelper.AddInParameter<String>(database, command, "CdOrganizacao", entity.CdOrganizacao);
            DBMapperHelper.AddInParameter<DateTime?>(database, command, "DtInicial", entity.DtInicial);
            DBMapperHelper.AddInParameter<String>(database, command, "CdRastreio", entity.CdRastreio);
            DBMapperHelper.AddInParameter<Int32>(database, command, "CompanyId", entity.CompanyId);
            DBMapperHelper.AddInParameter<Int32>(database, command, "CdCliente", entity.CdCliente);
            DBMapperHelper.AddInParameter<DateTime?>(database, command, "Auditdate", entity.Auditdate);
            DBMapperHelper.AddInParameter<String>(database, command, "NrPedidoSapServico", entity.NrPedidoSapServico);

		}

        public override void AddDeleteParameters(QDatabase database, DbCommand command, PedidoInterno entity)
        {
            DBMapperHelper.AddInParameter<Guid>(database, command, "NrControle", entity.NrControle);

		}

        public override void AddGetByIdParameters(QDatabase database, DbCommand command, PedidoInterno entity)
        {
            DBMapperHelper.AddInParameter<Guid>(database, command, "NrControle", entity.NrControle);
        
		}

        #endregion
    }
}
