using QLibs.DataAccess;
using QLibs.DataAccess.DBMappers;
using QLibs.DataAccess.Utils;
using RocaAPI.AdminEcomm.Common.Entities;
using RocaAPI.AdminEcomm.Common.Queries;
using System;
using System.Data;
using System.Data.Common;

namespace RocaAPI.AdminEcomm.DataAccess.DBMappers
{
    /// <summary>
    /// Represents a mapper of the class <see cref="PedidoItem"/>.
    /// </summary>
    public class PedidoItemDBMapper : BaseDBMapper<PedidoItem, PedidoItemQueryFilters>
    {
        #region -- Build Methods --

        public override PedidoItem BuildEntity(IDataReader reader)
        {
            PedidoItem entity = new PedidoItem();

            entity.NrPedido = DBMapperHelper.GetColumnInt64Value(reader, "NR_PEDIDO");
            entity.NrControle = DBMapperHelper.GetColumnStringValue(reader, "NR_CONTROLE");
            entity.SqItem = DBMapperHelper.GetColumnInt32Value(reader, "SQ_ITEM");
            entity.CdProduto = DBMapperHelper.GetColumnStringValue(reader, "CD_PRODUTO");
            entity.CdEmbalagem = DBMapperHelper.GetColumnStringValue(reader, "CD_EMBALAGEM");
            entity.TxEmbalagem = DBMapperHelper.GetColumnNulleableDecimalValue(reader, "TX_EMBALAGEM");
            entity.VlUnitario = DBMapperHelper.GetColumnDecimalValue(reader, "VL_UNITARIO");
            entity.QtPedido = DBMapperHelper.GetColumnInt32Value(reader, "QT_PEDIDO");
            entity.QtAtendido = DBMapperHelper.GetColumnNulleableInt32Value(reader, "QT_ATENDIDO");
            entity.TpSituacao = DBMapperHelper.GetColumnStringValue(reader, "TP_SITUACAO");
            entity.CdGrupoExpedicao = DBMapperHelper.GetColumnNulleableInt16Value(reader, "CD_GRUPO_EXPEDICAO");
            entity.DtRemessa = DBMapperHelper.GetColumnNulleableDateTimeValue(reader, "DT_REMESSA");
            entity.CdCentro = DBMapperHelper.GetColumnStringValue(reader, "CD_CENTRO");
            entity.CdTransportadora = DBMapperHelper.GetColumnStringValue(reader, "CD_TRANSPORTADORA");
            entity.CdTransportadoraRed = DBMapperHelper.GetColumnStringValue(reader, "CD_TRANSPORTADORA_RED");
            entity.VlPesoBruto = DBMapperHelper.GetColumnNulleableDecimalValue(reader, "VL_PESO_BRUTO");
            entity.QtSaldo = DBMapperHelper.GetColumnNulleableInt32Value(reader, "QT_SALDO");
            entity.QtFornecida = DBMapperHelper.GetColumnNulleableInt32Value(reader, "QT_FORNECIDA");
            entity.CdRota = DBMapperHelper.GetColumnStringValue(reader, "CD_ROTA");
            entity.VlImposto = DBMapperHelper.GetColumnNulleableDecimalValue(reader, "VL_IMPOSTO");
            entity.VlTotal = DBMapperHelper.GetColumnNulleableDecimalValue(reader, "VL_TOTAL");
            entity.CdImposto = DBMapperHelper.GetColumnStringValue(reader, "CD_IMPOSTO");
            entity.DtPrevisao = DBMapperHelper.GetColumnNulleableDateTimeValue(reader, "DT_PREVISAO");
            entity.VlFrete = DBMapperHelper.GetColumnNulleableDecimalValue(reader, "VL_FRETE");
            entity.CdRastreio = DBMapperHelper.GetColumnStringValue(reader, "CD_RASTREIO");


            return entity;
        }

        #endregion

        #region -- QueryFilters --

        public override QueryBuilder GetWhereClause(QDatabase database, DbCommand command, bool hasWhere, PedidoItemQueryFilters qFilters)
        {
            QueryBuilder builder = QueryBuilder.CreateInstance(hasWhere);
            builder.AddConditionWithAnd<string>(database, command, "PE.NR_PEDIDO", QueryBuilder.QueryBuilderOperator.Equals, qFilters.NrPedido, true, true);
            builder.AddConditionWithAnd<string>(database, command, "PE.NR_CONTROLE", QueryBuilder.QueryBuilderOperator.Equals, qFilters.NrControle, true, true);

            return builder;
        }

        #endregion

        #region -- Parameter Methods --

        public override void AddCreateParameters(QDatabase database, DbCommand command, PedidoItem entity)
        {
            AddAllParameters(database, command, entity);
        }

        public override void AddUpdateParameters(QDatabase database, DbCommand command, PedidoItem entity)
        {
            AddAllParameters(database, command, entity);
        }

        private void AddAllParameters(QDatabase database, DbCommand command, PedidoItem entity)
        {
            DBMapperHelper.AddInParameter<Int64>(database, command, "NrPedido", entity.NrPedido);
            DBMapperHelper.AddInParameter<String>(database, command, "NrControle", entity.NrControle);
            DBMapperHelper.AddInParameter<Int32>(database, command, "SqItem", entity.SqItem);
            DBMapperHelper.AddInParameter<String>(database, command, "CdProduto", entity.CdProduto);
            DBMapperHelper.AddInParameter<String>(database, command, "CdEmbalagem", entity.CdEmbalagem);
            DBMapperHelper.AddInParameter<Decimal?>(database, command, "TxEmbalagem", entity.TxEmbalagem);
            DBMapperHelper.AddInParameter<Decimal>(database, command, "VlUnitario", entity.VlUnitario);
            DBMapperHelper.AddInParameter<Int32>(database, command, "QtPedido", entity.QtPedido);
            DBMapperHelper.AddInParameter<Int32?>(database, command, "QtAtendido", entity.QtAtendido);
            DBMapperHelper.AddInParameter<String>(database, command, "TpSituacao", entity.TpSituacao);
            DBMapperHelper.AddInParameter<Int16?>(database, command, "CdGrupoExpedicao", entity.CdGrupoExpedicao);
            DBMapperHelper.AddInParameter<DateTime?>(database, command, "DtRemessa", entity.DtRemessa);
            DBMapperHelper.AddInParameter<String>(database, command, "CdCentro", entity.CdCentro);
            DBMapperHelper.AddInParameter<String>(database, command, "CdTransportadora", entity.CdTransportadora);
            DBMapperHelper.AddInParameter<String>(database, command, "CdTransportadoraRed", entity.CdTransportadoraRed);
            DBMapperHelper.AddInParameter<Decimal?>(database, command, "VlPesoBruto", entity.VlPesoBruto);
            DBMapperHelper.AddInParameter<Int32?>(database, command, "QtSaldo", entity.QtSaldo);
            DBMapperHelper.AddInParameter<Int32?>(database, command, "QtFornecida", entity.QtFornecida);
            DBMapperHelper.AddInParameter<String>(database, command, "CdRota", entity.CdRota);
            DBMapperHelper.AddInParameter<Decimal?>(database, command, "VlImposto", entity.VlImposto);
            DBMapperHelper.AddInParameter<Decimal?>(database, command, "VlTotal", entity.VlTotal);
            DBMapperHelper.AddInParameter<String>(database, command, "CdImposto", entity.CdImposto);
            DBMapperHelper.AddInParameter<DateTime?>(database, command, "DtPrevisao", entity.DtPrevisao);
            DBMapperHelper.AddInParameter<Decimal?>(database, command, "VlFrete", entity.VlFrete);
            DBMapperHelper.AddInParameter<String>(database, command, "CdRastreio", entity.CdRastreio);

        }

        public override void AddDeleteParameters(QDatabase database, DbCommand command, PedidoItem entity)
        {
            DBMapperHelper.AddInParameter<Int64>(database, command, "NrPedido", entity.NrPedido);
            DBMapperHelper.AddInParameter<String>(database, command, "NrControle", entity.NrControle);
            DBMapperHelper.AddInParameter<Int32>(database, command, "SqItem", entity.SqItem);

        }

        public override void AddGetByIdParameters(QDatabase database, DbCommand command, PedidoItem entity)
        {
            DBMapperHelper.AddInParameter<Int64>(database, command, "NrPedido", entity.NrPedido);
            DBMapperHelper.AddInParameter<String>(database, command, "NrControle", entity.NrControle);
            DBMapperHelper.AddInParameter<Int32>(database, command, "SqItem", entity.SqItem);

        }

        #endregion
    }
}
