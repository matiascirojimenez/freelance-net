using QLibs.DataAccess;
using QLibs.DataAccess.DBMappers;
using QLibs.DataAccess.Utils;
using RocaAPI.AdminEcomm.Common.Entities;
using RocaAPI.AdminEcomm.Common.Queries;
using System;
using System.Data;
using System.Data.Common;

namespace RocaAPI.AdminEcomm.DataAccess.DBMappers
{
    /// <summary>
    /// Represents a mapper of the class <see cref="Produto"/>.
    /// </summary>
    public class ProdutoDBMapper : BaseDBMapper<Produto, ProdutoQueryFilters>
    {
        #region -- Build Methods --

        public override Produto BuildEntity(IDataReader reader)
        {
            Produto entity = new Produto();

            entity.Brand = DBMapperHelper.GetColumnStringValue(reader, "brand");
            entity.Code = DBMapperHelper.GetColumnStringValue(reader, "code");
            entity.Ean = DBMapperHelper.GetColumnStringValue(reader, "ean");
            entity.Description = DBMapperHelper.GetColumnStringValue(reader, "description");
            entity.Line = DBMapperHelper.GetColumnStringValue(reader, "line");
            entity.Category = DBMapperHelper.GetColumnStringValue(reader, "category");
            entity.Type = DBMapperHelper.GetColumnStringValue(reader, "type");
            entity.Subtype = DBMapperHelper.GetColumnStringValue(reader, "subtype");
            entity.Images = DBMapperHelper.GetColumnStringValue(reader, "images");
            entity.Package = DBMapperHelper.GetColumnStringValue(reader, "package");
            entity.InstalationStuff = DBMapperHelper.GetColumnStringValue(reader, "instalation_stuff");
            entity.Warranty = DBMapperHelper.GetColumnStringValue(reader, "warranty");
            entity.AddinfoAplication = DBMapperHelper.GetColumnStringValue(reader, "addinfo_aplication");
            entity.AddinfoFormat = DBMapperHelper.GetColumnStringValue(reader, "addinfo_format");
            entity.AddinfoFunctionmode = DBMapperHelper.GetColumnStringValue(reader, "addinfo_functionmode");
            entity.AddinfoInstalationtype = DBMapperHelper.GetColumnStringValue(reader, "addinfo_instalationtype");
            entity.AddinfoAmbience = DBMapperHelper.GetColumnStringValue(reader, "addinfo_ambience");
            entity.AddinfoWaterpoint = DBMapperHelper.GetColumnStringValue(reader, "addinfo_waterpoint");
            entity.AddinfoMotion = DBMapperHelper.GetColumnStringValue(reader, "addinfo_motion");
            entity.AddinfoAerator = DBMapperHelper.GetColumnStringValue(reader, "addinfo_aerator");
            entity.AddinfoTap = DBMapperHelper.GetColumnStringValue(reader, "addinfo_tap");
            entity.AddinfoWaterpression = DBMapperHelper.GetColumnStringValue(reader, "addinfo_waterpression");
            entity.AddinfoFinish = DBMapperHelper.GetColumnStringValue(reader, "addinfo_finish");
            entity.AddinfoMaterial = DBMapperHelper.GetColumnStringValue(reader, "addinfo_material");
            entity.AddinfoDrilling = DBMapperHelper.GetColumnStringValue(reader, "addinfo_drilling");
            entity.AddinfoSewer = DBMapperHelper.GetColumnStringValue(reader, "addinfo_sewer");
            entity.AddinfoFlush = DBMapperHelper.GetColumnStringValue(reader, "addinfo_flush");
            entity.AddinfoOthers = DBMapperHelper.GetColumnStringValue(reader, "addinfo_others");
            entity.AddinfoColor = DBMapperHelper.GetColumnStringValue(reader, "addinfo_color");
            entity.Price = DBMapperHelper.GetColumnNulleableDoubleValue(reader, "price");
            entity.PackageWeight = DBMapperHelper.GetColumnNulleableDoubleValue(reader, "package_weight");
            entity.PackageHeight = DBMapperHelper.GetColumnNulleableDoubleValue(reader, "package_height");
            entity.PackageWidth = DBMapperHelper.GetColumnNulleableDoubleValue(reader, "package_width");
            entity.PackageDepth = DBMapperHelper.GetColumnNulleableDoubleValue(reader, "package_depth");
            entity.Ncm = DBMapperHelper.GetColumnStringValue(reader, "NCM");
            entity.PriceFull = DBMapperHelper.GetColumnNulleableDoubleValue(reader, "price_full");
            entity.ProductHeight = DBMapperHelper.GetColumnNulleableDoubleValue(reader, "product_height");
            entity.ProductWidth = DBMapperHelper.GetColumnNulleableDoubleValue(reader, "product_width");
            entity.ProductDepth = DBMapperHelper.GetColumnNulleableDoubleValue(reader, "product_depth");
            entity.UpdateDate = DBMapperHelper.GetColumnNulleableDateTimeValue(reader, "update_date");
            entity.Zzoutlet = DBMapperHelper.GetColumnNulleableCharValueFromString(reader, "zzoutlet");
            entity.NovoOutlet = DBMapperHelper.GetColumnStringValue(reader, "novo_outlet");
            entity.MktText = DBMapperHelper.GetColumnStringValue(reader, "mkt_text");
            entity.Destaque = DBMapperHelper.GetColumnStringValue(reader, "destaque");
            entity.UrlVideoTec = DBMapperHelper.GetColumnStringValue(reader, "URL_VIDEO_TEC");
            entity.UrlVideoInst = DBMapperHelper.GetColumnStringValue(reader, "URL_VIDEO_INST");
            entity.UrlVideoPub = DBMapperHelper.GetColumnStringValue(reader, "URL_VIDEO_PUB");
            entity.CdLinha = DBMapperHelper.GetColumnStringValue(reader, "cd_linha");
            entity.CdSegmento = DBMapperHelper.GetColumnStringValue(reader, "cd_segmento");
            entity.CdClasse = DBMapperHelper.GetColumnStringValue(reader, "cd_classe");
            entity.Carrossel02 = DBMapperHelper.GetColumnStringValue(reader, "carrossel02");

            return entity;
        }

        #endregion

        #region -- QueryFilters --

        public override QueryBuilder GetWhereClause(QDatabase database, DbCommand command, bool hasWhere, ProdutoQueryFilters qFilters)
        {
            QueryBuilder builder = QueryBuilder.CreateInstance(hasWhere);

            var subQuery = QueryBuilder.CreateSubQueryInstance(builder);
            subQuery.AddConditionWithOr<string>(database, command, "PR.code", QueryBuilder.QueryBuilderOperator.LikeFull, qFilters.FindValue);
            subQuery.AddConditionWithOr<string>(database, command, "PR.ean", QueryBuilder.QueryBuilderOperator.LikeFull, qFilters.FindValue);
            subQuery.AddConditionWithOr<string>(database, command, "PR.description", QueryBuilder.QueryBuilderOperator.LikeFull, qFilters.FindValue);
            builder.AddSubQuery(QueryBuilder.QueryBuilderCondition.And, subQuery);

            builder.AddConditionWithAnd<string>(database, command, "PR.brand", QueryBuilder.QueryBuilderOperator.Equals, qFilters.Brand, true, true);
            builder.AddConditionWithAnd<string>(database, command, "PR.code", QueryBuilder.QueryBuilderOperator.LikeFull, qFilters.Code, true, true);
            builder.AddConditionWithAnd<string>(database, command, "PR.ean", QueryBuilder.QueryBuilderOperator.LikeFull, qFilters.Ean, true, true);
            builder.AddConditionWithAnd<string>(database, command, "PR.description", QueryBuilder.QueryBuilderOperator.LikeFull, qFilters.Description, true, true);
            builder.AddConditionWithAnd<string>(database, command, "PR.line", QueryBuilder.QueryBuilderOperator.Equals, qFilters.Line, true, true);
            builder.AddConditionWithAnd<string>(database, command, "PR.category", QueryBuilder.QueryBuilderOperator.Equals, qFilters.Category, true, true);
            builder.AddConditionWithAnd<string>(database, command, "PR.type", QueryBuilder.QueryBuilderOperator.Equals, qFilters.Type, true, true);
            builder.AddConditionWithAnd<string>(database, command, "PR.subtype", QueryBuilder.QueryBuilderOperator.Equals, qFilters.SubType, true, true);

            return builder;
        }

        #endregion

        #region -- Parameter Methods --

        public override void AddCreateParameters(QDatabase database, DbCommand command, Produto entity)
        {
            AddAllParameters(database, command, entity);
        }

        public override void AddUpdateParameters(QDatabase database, DbCommand command, Produto entity)
        {
            AddAllParameters(database, command, entity);
        }

        private void AddAllParameters(QDatabase database, DbCommand command, Produto entity)
        {
            DBMapperHelper.AddInParameter<String>(database, command, "Brand", entity.Brand);
            DBMapperHelper.AddInParameter<String>(database, command, "Code", entity.Code);
            DBMapperHelper.AddInParameter<String>(database, command, "Ean", entity.Ean);
            DBMapperHelper.AddInParameter<String>(database, command, "Description", entity.Description);
            DBMapperHelper.AddInParameter<String>(database, command, "Line", entity.Line);
            DBMapperHelper.AddInParameter<String>(database, command, "Category", entity.Category);
            DBMapperHelper.AddInParameter<String>(database, command, "Type", entity.Type);
            DBMapperHelper.AddInParameter<String>(database, command, "Subtype", entity.Subtype);
            DBMapperHelper.AddInParameter<String>(database, command, "Images", entity.Images);
            DBMapperHelper.AddInParameter<String>(database, command, "Package", entity.Package);
            DBMapperHelper.AddInParameter<String>(database, command, "InstalationStuff", entity.InstalationStuff);
            DBMapperHelper.AddInParameter<String>(database, command, "Warranty", entity.Warranty);
            DBMapperHelper.AddInParameter<String>(database, command, "AddinfoAplication", entity.AddinfoAplication);
            DBMapperHelper.AddInParameter<String>(database, command, "AddinfoFormat", entity.AddinfoFormat);
            DBMapperHelper.AddInParameter<String>(database, command, "AddinfoFunctionmode", entity.AddinfoFunctionmode);
            DBMapperHelper.AddInParameter<String>(database, command, "AddinfoInstalationtype", entity.AddinfoInstalationtype);
            DBMapperHelper.AddInParameter<String>(database, command, "AddinfoAmbience", entity.AddinfoAmbience);
            DBMapperHelper.AddInParameter<String>(database, command, "AddinfoWaterpoint", entity.AddinfoWaterpoint);
            DBMapperHelper.AddInParameter<String>(database, command, "AddinfoMotion", entity.AddinfoMotion);
            DBMapperHelper.AddInParameter<String>(database, command, "AddinfoAerator", entity.AddinfoAerator);
            DBMapperHelper.AddInParameter<String>(database, command, "AddinfoTap", entity.AddinfoTap);
            DBMapperHelper.AddInParameter<String>(database, command, "AddinfoWaterpression", entity.AddinfoWaterpression);
            DBMapperHelper.AddInParameter<String>(database, command, "AddinfoFinish", entity.AddinfoFinish);
            DBMapperHelper.AddInParameter<String>(database, command, "AddinfoMaterial", entity.AddinfoMaterial);
            DBMapperHelper.AddInParameter<String>(database, command, "AddinfoDrilling", entity.AddinfoDrilling);
            DBMapperHelper.AddInParameter<String>(database, command, "AddinfoSewer", entity.AddinfoSewer);
            DBMapperHelper.AddInParameter<String>(database, command, "AddinfoFlush", entity.AddinfoFlush);
            DBMapperHelper.AddInParameter<String>(database, command, "AddinfoOthers", entity.AddinfoOthers);
            DBMapperHelper.AddInParameter<String>(database, command, "AddinfoColor", entity.AddinfoColor);
            DBMapperHelper.AddInParameter<double?>(database, command, "Price", entity.Price);
            DBMapperHelper.AddInParameter<double?>(database, command, "PackageWeight", entity.PackageWeight);
            DBMapperHelper.AddInParameter<double?>(database, command, "PackageHeight", entity.PackageHeight);
            DBMapperHelper.AddInParameter<double?>(database, command, "PackageWidth", entity.PackageWidth);
            DBMapperHelper.AddInParameter<double?>(database, command, "PackageDepth", entity.PackageDepth);
            DBMapperHelper.AddInParameter<String>(database, command, "Ncm", entity.Ncm);
            DBMapperHelper.AddInParameter<double?>(database, command, "PriceFull", entity.PriceFull);
            DBMapperHelper.AddInParameter<double?>(database, command, "ProductHeight", entity.ProductHeight);
            DBMapperHelper.AddInParameter<double?>(database, command, "ProductWidth", entity.ProductWidth);
            DBMapperHelper.AddInParameter<double?>(database, command, "ProductDepth", entity.ProductDepth);
            DBMapperHelper.AddInParameter<DateTime?>(database, command, "UpdateDate", entity.UpdateDate);
            DBMapperHelper.AddInParameter<Char?>(database, command, "Zzoutlet", entity.Zzoutlet);
            DBMapperHelper.AddInParameter<String>(database, command, "NovoOutlet", entity.NovoOutlet);
            DBMapperHelper.AddInParameter<String>(database, command, "MktText", entity.MktText);
            DBMapperHelper.AddInParameter<String>(database, command, "Destaque", entity.Destaque);
            DBMapperHelper.AddInParameter<String>(database, command, "UrlVideoTec", entity.UrlVideoTec);
            DBMapperHelper.AddInParameter<String>(database, command, "UrlVideoInst", entity.UrlVideoInst);
            DBMapperHelper.AddInParameter<String>(database, command, "UrlVideoPub", entity.UrlVideoPub);
            DBMapperHelper.AddInParameter<String>(database, command, "CdLinha", entity.CdLinha);
            DBMapperHelper.AddInParameter<String>(database, command, "CdSegmento", entity.CdSegmento);
            DBMapperHelper.AddInParameter<String>(database, command, "CdClasse", entity.CdClasse);
            DBMapperHelper.AddInParameter<String>(database, command, "Carrossel02", entity.Carrossel02);

        }

        public override void AddDeleteParameters(QDatabase database, DbCommand command, Produto entity)
        {
            DBMapperHelper.AddInParameter<String>(database, command, "Brand", entity.Brand);
            DBMapperHelper.AddInParameter<String>(database, command, "Code", entity.Code);

        }

        public override void AddGetByIdParameters(QDatabase database, DbCommand command, Produto entity)
        {
            DBMapperHelper.AddInParameter<String>(database, command, "Brand", entity.Brand);
            DBMapperHelper.AddInParameter<String>(database, command, "Code", entity.Code);

        }

        #endregion
    }
}
