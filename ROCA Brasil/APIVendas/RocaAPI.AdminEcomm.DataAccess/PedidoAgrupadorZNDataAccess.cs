using QLibs.Common.DTO.Queries;
using QLibs.DataAccess;
using RocaAPI.AdminEcomm.Common.Entities;
using RocaAPI.AdminEcomm.DataAccess.DBMappers;
using System;

namespace RocaAPI.AdminEcomm.DataAccess
{
    public partial class PedidoAgrupadorZNDataAccess : BaseEntityDataAccess<PedidoAgrupadorZNDBMapper, PedidoAgrupadorZN, BaseQueryFilters, String>
    {
        #region -- Constructors --

        public PedidoAgrupadorZNDataAccess() { }

        public PedidoAgrupadorZNDataAccess(string connName) : base(connName) { }

        public PedidoAgrupadorZNDataAccess(QDatabase database) : base(database) { }

        #endregion
        
        #region -- Public Methods --

        #endregion

        #region -- Private Methods --

        #endregion
    }
}
